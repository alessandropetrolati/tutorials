//
//  ViewController.h
//  03_bufferFrame
//
//  Created by Alessandro Petrolati on 29/05/15.
//  Copyright (c) 2015 apeSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

+ (UIViewController*) topMostController;
@end

