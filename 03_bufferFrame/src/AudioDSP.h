//
//  AudioDSP.h
//  03_bufferFrame
//
//  Created by Alessandro Petrolati on 29/05/15.
//  Copyright (c) 2015 apeSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioServices.h>
#import <AudioUnit/AudioUnit.h>
#import <AVFoundation/AVFoundation.h>

#import "csound.h"

#define SAMPLE_RATE_DEF 44100
#define KSMPS_DEF 256

#if! TARGET_OS_MACCATALYST
#define IAA
//#define AB
#endif

//#define ENABLE_BLUETOOTH
#define ENABLE_MIDI

#ifdef AB
#import "Audiobus.h"
#import "ABAudiobusController.h"
#endif

@interface AudioDSP : NSObject {
    
    /* Audio IO */
    AudioUnit csAUHAL;
    OSStatus err;
    ExtAudioFileRef recFile;
    
    /* Csound data */
    long bufframes;
    int ret;
    int nchnls;
    int counter;
    bool running;
    bool shouldRecord;
    
    /* User Interface */
    IBOutlet UISlider* inputGain;
    IBOutlet UISlider* sineGain;
    IBOutlet UISwitch* rec;
    IBOutlet UISwitch* dspToggle;
    
    IBOutlet UISegmentedControl* _sr;
    IBOutlet UISegmentedControl* _ksmps;
    IBOutlet UILabel* _settingsVerbose;
    
    Float64 _currentSamplingRate;
    Float64 _currentKsmps;
    Float64 tmpSR;
    Float64 tmpKSMPS;
}
@property(nonatomic, readwrite) CSOUND* cs;

#ifdef AB
//AUDIOBUS
@property (strong, nonatomic) ABAudiobusController* AB_Controller;
@property (strong, nonatomic) ABSenderPort* output;
@property (strong, nonatomic) ABFilterPort* filter;
#endif

#ifdef IAA
@property (nonatomic, readonly) BOOL connected;
#endif
@end
