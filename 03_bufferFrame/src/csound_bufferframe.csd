/*
cs4dev tutorial 03
by Alessandro Petrolati
www.apesoft.it
*/

<CsoundSynthesizer>
<CsOptions>

-o dac 
-+rtmidi=null 
-+rtaudio=null 
-d 
-+msg_color=0 
--expression-opt
-M0 
-m0
-i adc

</CsOptions>
<CsInstruments>

sr = 44100
ksmps = 64

;;;;SR;;;;		//strings replaced from Objective-C
;;;;KSMPS;;;;

nchnls = 2
0dbfs = 1

instr 1

ain1, ain2 ins

kampIn chnget "input_gain"
kampSin chnget "sine_amp"

kampIn portk kampIn, 0.2
kampSin portk kampSin, 0.2

ampIn interp kampIn
ampSin interp kampSin

ain1 *= ampIn
ain2 *= ampIn

asine oscili ampSin, 440

outs ain1+asine, ain2+asine

endin

</CsInstruments>
<CsScore>

i 1 0 10000

</CsScore>
</CsoundSynthesizer>
