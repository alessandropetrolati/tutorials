1) Download latest “Audiobus” http://audiob.us
2) Copy “Audiobus-SDK-2” folder in current folder
3) Download latest “The Amazing Audio Engine” http://theamazingaudioengine.com
4) Copy “TheAmazingAudioEngine-master” folder in current folder
5) open xCode “Csound iOS Examples” and compile.

Warning: You need to setup a valid Audiobus Key, follow the Audiobus documentations.

Welcome to Csound for iOS! This app show how to using Csound for iOS.
For those with knowledge of Csound, hopefully you will see that the value of your knowledge is only enhanced by offering a new platform on which to create musical software and works.
Users interested in diving in to see how the API is used may want to download the Csound for iOS Examples Project which contains a set of examples that cover different use cases of the API. The code for each example may be useful in getting you a jump-start into building your own application.

Csound for iOs
Copyright (C) 2011 Steven Yi, Victor Lazzarini
 
This version contains Alessandro Petrolati's features (apeSoft).
"The Amazing Audio Engine" is required
for Audiobus and Inter App Audio Integration.

http://www.csounds.com
http://theamazingaudioengine.com
http://audiob.us

Download xCode Project at:
http://www.densitygs.com

The Csound for iOS Library is free software; you can redistribute it
and/or modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.   

Csound is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Csound; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
02111-1307 USA