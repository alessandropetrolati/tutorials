<CsoundSynthesizer>
<CsOptions>
-o dac -+rtmidi=null -+rtaudio=null -d -+msg_color=0 -M0 -m0 -i adc
</CsOptions>
<CsInstruments>
sr        = 44100
ksmps     = 256
nchnls    = 2
0dbfs	  = 1

	instr 1
S_file strget p4
ilen filelen S_file
ich filenchnls S_file

S_score sprintfk {{ i 2 0 %f "%s" %f}}, ilen, S_file, ich
scoreline S_score, 1
turnoff
	endin

	instr 2
kpitch chnget "pitch"

if p5 == 2 then	
aL, aR diskin2 p4, kpitch
elseif p5 == 1 then
aL diskin2 p4, kpitch
aR = aL
endif

aL = aL
aR = aR
outs aL, aR
	endin


</CsInstruments>
<CsScore>

f0 100000

</CsScore>
</CsoundSynthesizer>
<bsbPanel>
 <label>Widgets</label>
 <objectName/>
 <x>100</x>
 <y>100</y>
 <width>320</width>
 <height>240</height>
 <visible>true</visible>
 <uuid/>
 <bgcolor mode="nobackground">
  <r>255</r>
  <g>255</g>
  <b>255</b>
 </bgcolor>
</bsbPanel>
<bsbPresets>
</bsbPresets>
