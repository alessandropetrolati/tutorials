/* 
 
 CsoundObj.h:
 
 This version contains Alessandro Petrolati's features (apeSoft)
 "The Amazing Audio Engine" is required
 for a deep integration with Audiobus and Inter App Audio.
 
 http://theamazingaudioengine.com
 http://audiob.us
 http://www.densitygs.com

 Copyright (C) 2011 Steven Yi, Victor Lazzarini
 
 This file is part of Csound for iOS.
 
 The Csound for iOS Library is free software; you can redistribute it
 and/or modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.   

 Csound is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with Csound; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
 02111-1307 USA

*/

#import <AudioToolbox/ExtendedAudioFile.h>
#import <AudioToolbox/AudioConverter.h>
#import <AudioToolbox/AudioServices.h>
#import <AudioUnit/AudioUnit.h>
#import <AVFoundation/AVFoundation.h>
#import <Foundation/Foundation.h>
#import "csound.h"
#import <CoreMotion/CoreMotion.h>
#import "AEAudioController.h"

@class AEBlockChannel;
@class AEAudioUnitFilter;
@class AEAudioController;
@class AEPlaythroughChannel;
@class AEBlockFilter;
@class AEBlockAudioReceiver;
@class AERecorder;
@class ABAudiobusController;
@class ABSenderPort;
@class ABFilterPort;

/* Enable Audio Input */
#define USE_AUDIO_INPUT YES

/* Sets Default SR and KSMPS */
#define SAMPLE_RATE_DEF 44100.0
#define BUFFER_SIZE_DEF 512

/* Setup here your Audiobus Key */
#define AUDIOBUS_KEY @"MTQxNzcwMzY3MSoqKmNzNGRldioqKmNzNGRldi5hdWRpb2J1czovLw==:FBLp8K0mqwkz6ZIVamoWaeyqNwUAehvEhZrQOsSO+Qu+Fg3iuFatcnn99rOjEQ255udi4eqh7CmvY3H15IjuuueFw4nXk2B2r4xEci5wAwTE89uPUrg467FRNHGv5aGd"

/* Enable Inter App Audio */
#define IAA

typedef struct csdata_ {
	CSOUND *cs;
	long bufframes;
	int ret;
	int nchnls;
    int nchnls_i;
    bool running;
	bool shouldMute;
     __unsafe_unretained NSMutableArray* valuesCache;
} csdata;

typedef struct {
	CSOUND *cs;
	int attr;
	const char *format;
	va_list valist;
} Message;

@class CsoundObj;
@protocol CsoundValueCacheable;

@protocol CsoundObjCompletionListener 

-(void)csoundObjDidStart:(CsoundObj*)csoundObj;
-(void)csoundObjComplete:(CsoundObj*)csoundObj;

@end

@interface CsoundObj : NSObject {
    NSMutableArray* valuesCache;
    NSMutableArray* completionListeners;
    csdata mCsData;
    BOOL mMidiInEnabled;
    CMMotionManager* mMotionManager;
	id  mMessageListener;
    
    AEPlaythroughChannel* audioInput;
    AEBlockFilter* audioEngine;
    AERecorder* recorder;
//Add othe AudioUnit Effects
//    AEAudioUnitFilter* auEffectUnit_1;
//    AEAudioUnitFilter* auEffectUnit_2;
//    AEAudioUnitFilter* auEffectUnit_3;
}


@property (assign) SEL mMessageCallback;
@property (assign) BOOL midiInEnabled;
@property (nonatomic, strong) CMMotionManager* motionManager;

/* The Amazing Audio Engine */
@property (retain, nonatomic) AEAudioController *TAAE_Controller;

/* Audiobus */
@property (strong, nonatomic) ABAudiobusController *AB_Controller;
@property (strong, nonatomic) ABSenderPort *output;
@property (strong, nonatomic) ABFilterPort *filter;


#pragma mark UI and Hardware Methods

-(id<CsoundValueCacheable>)addSwitch:(UISwitch*)uiSwitch forChannelName:(NSString*)channelName;
-(id<CsoundValueCacheable>)addSlider:(UISlider*)uiSlider forChannelName:(NSString*)channelName;
-(id<CsoundValueCacheable>)addButton:(UIButton*)uiButton forChannelName:(NSString*)channelName;

-(void)addValueCacheable:(id<CsoundValueCacheable>)valueCacheable;
-(void)removeValueCaheable:(id<CsoundValueCacheable>)valueCacheable;

-(id<CsoundValueCacheable>)enableAccelerometer;
-(id<CsoundValueCacheable>)enableGyroscope;
-(id<CsoundValueCacheable>)enableAttitude;

#pragma mark -

-(void)sendScore:(NSString*)score;

#pragma mark -

-(void)addCompletionListener:(id<CsoundObjCompletionListener>)listener;

#pragma mark -

+(CsoundObj*) sharedInstance;

#pragma mark -

-(void)startCsound:(NSString*)csdFilePath;
-(void)stopCsound;
-(void)muteCsound;
-(void)unmuteCsound;
-(void)recordToURL:(NSURL *)outputURL;
-(void)stopRecording;
-(void)startCsoundToDisk:(NSString*)csdFilePath outputFile:(NSString*)outputFile;
-(CSOUND*)getCsound;

/** get a float* output channel that maps to a channel name and type, where type is 
 CSOUND_AUDIO_CHANNEL, CSOUND_CONTROL_CHANNEL, etc. */
-(float*)getInputChannelPtr:(NSString*)channelName channelType:(controlChannelType)channelType;

/** get a float* output channel that maps to a channel name and type, where type is 
 CSOUND_AUDIO_CHANNEL, CSOUND_CONTROL_CHANNEL, etc. */
-(float*)getOutputChannelPtr:(NSString*)channelName channelType:(controlChannelType)channelType;

-(NSData*)getOutSamples;
-(int)getNumChannels;
-(int)getKsmps;

-(void)setMessageCallback:(SEL)method withListener:(id)listener;
-(void)performMessageCallback:(NSValue *)infoObj;

@end




