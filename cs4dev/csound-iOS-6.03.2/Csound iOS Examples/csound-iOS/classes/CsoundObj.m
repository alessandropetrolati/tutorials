/*
 
 CsoundObj.m:
 
 This version contains Alessandro Petrolati's features (apeSoft)
 "The Amazing Audio Engine" is required
 for a deep integration with Audiobus and Inter App Audio.
 
 http://theamazingaudioengine.com
 http://audiob.us
 http://www.densitygs.com
 
 Copyright (C) 2011 Steven Yi, Victor Lazzarini
 
 This file is part of Csound for iOS.
 
 The Csound for iOS Library is free software; you can redistribute it
 and/or modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 Csound is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with Csound; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
 02111-1307 USA
 
 */

#import "CsoundObj.h"
#import "CachedSlider.h"
#import "CachedButton.h"
#import "CachedSwitch.h"
#import "CachedAccelerometer.h"
#import "CachedGyroscope.h"
#import "CachedAttitude.h"
#import "CsoundValueCacheable.h"
#import "CsoundMIDI.h"

#import "TheAmazingAudioEngine.h"
#import "AEAudioController.h"
#import "AEPlaythroughChannel.h"
#import "AERecorder.h"
#import "Audiobus.h"

@interface CsoundObj()

-(void)runCsound:(NSString*)csdFilePath;
-(void)runCsoundToDisk:(NSArray*)paths;

@end

void MIDIEventProcCallBack(void *userData, UInt32 inStatus, UInt32 inData1, UInt32 inData2, UInt32 inOffsetSampleFrame);
void AudioUnitPropertyChangeDispatcher(void *inRefCon, AudioUnit inUnit, AudioUnitPropertyID inID, AudioUnitScope inScope, AudioUnitElement inElement);

@implementation CsoundObj

@synthesize midiInEnabled = mMidiInEnabled;
@synthesize motionManager = mMotionManager;
@synthesize TAAE_Controller = _TAAE_Controller;
@synthesize AB_Controller = _AB_Controller;
@synthesize output = _output;
@synthesize filter = _filter;


static CsoundObj* instance = nil;

+(CsoundObj*)sharedInstance {
    @synchronized(self)
    {
        if (instance == nil) {
            NSLog(@"CsoundObj Singleton Initializing");
            instance = [[self alloc] init];
        }
    }
    return(instance);
}

- (id)init
{
    self = [super init];
    if (self) {
        
		mCsData.shouldMute = false;
        mCsData.cs = nil;
        mCsData.running = false;
        mCsData.ret = 1;
        valuesCache = [[NSMutableArray alloc] init];
        completionListeners = [[NSMutableArray alloc] init];
        mMidiInEnabled = NO;
        self.motionManager = [[CMMotionManager alloc] init];
        
        [self INITIALIZE_TAAE_AB_IAA];
    }
    
    return self;
}

- (void)dealloc {
    
    _TAAE_Controller = nil;
    //    auEffectUnit_1 = nil;
    //    auEffectUnit_2 = nil;
    //    auEffectUnit_3 = nil;
    audioInput = nil;
    audioEngine = nil;
    recorder = nil;
}

-(id<CsoundValueCacheable>)addSwitch:(UISwitch*)uiSwitch forChannelName:(NSString*)channelName {
    CachedSwitch* cachedSwitch = [[CachedSwitch alloc] init:uiSwitch
                                                channelName:channelName];
    [valuesCache addObject:cachedSwitch];
	
    return cachedSwitch;
}

-(id<CsoundValueCacheable>)addSlider:(UISlider*)uiSlider forChannelName:(NSString*)channelName {
    
    CachedSlider* cachedSlider = [[CachedSlider alloc] init:uiSlider
                                                channelName:channelName];
    [valuesCache addObject:cachedSlider];
    
    return cachedSlider;
}

-(id<CsoundValueCacheable>)addButton:(UIButton*)uiButton forChannelName:(NSString*)channelName {
    CachedButton* cachedButton = [[CachedButton alloc] init:uiButton
                                                channelName:channelName];
    [valuesCache addObject:cachedButton];
    return cachedButton;
}

-(void)addValueCacheable:(id<CsoundValueCacheable>)valueCacheable {
    if (valueCacheable != nil) {
        [valuesCache addObject:valueCacheable];
    }
}

-(void)removeValueCaheable:(id<CsoundValueCacheable>)valueCacheable {
	if (valueCacheable != nil && [valuesCache containsObject:valueCacheable]) {
		[valuesCache removeObject:valueCacheable];
	}
}

-(id<CsoundValueCacheable>)enableAccelerometer {
    
    if (!mMotionManager.accelerometerAvailable) {
        NSLog(@"Accelerometer not available");
        return nil;
    }
    
    CachedAccelerometer* accelerometer = [[CachedAccelerometer alloc] init:mMotionManager];
    [valuesCache addObject:accelerometer];
    
    
    mMotionManager.accelerometerUpdateInterval = 1 / 100.0; // 100 hz
    
    [mMotionManager startAccelerometerUpdates];
	
	return accelerometer;
}

-(id<CsoundValueCacheable>)enableGyroscope {
    
    if (!mMotionManager.isGyroAvailable) {
        NSLog(@"Gyroscope not available");
        return nil;
    }
    
    CachedGyroscope* gyro = [[CachedGyroscope alloc] init:mMotionManager];
    [valuesCache addObject:gyro];
    
    mMotionManager.gyroUpdateInterval = 1 / 100.0; // 100 hz
    
    [mMotionManager startGyroUpdates];
	
	return gyro;
}

-(id<CsoundValueCacheable>)enableAttitude {
    if (!mMotionManager.isDeviceMotionAvailable) {
        NSLog(@"Attitude not available");
        return nil;
    }
    
    CachedAttitude* attitude = [[CachedAttitude alloc] init:mMotionManager];
    [valuesCache addObject:attitude];
    
    mMotionManager.deviceMotionUpdateInterval = 1 / 100.0; // 100hz
    
    [mMotionManager startDeviceMotionUpdates];
	
	return attitude;
}

#pragma mark -

static void messageCallback(CSOUND *cs, int attr, const char *format, va_list valist)
{
	@autoreleasepool {
		CsoundObj *obj = (__bridge CsoundObj *)(csoundGetHostData(cs));
		Message info;
		info.cs = cs;
		info.attr = attr;
		info.format = format;
        va_copy(info.valist,valist);
		NSValue *infoObj = [NSValue value:&info withObjCType:@encode(Message)];
		[obj performSelector:@selector(performMessageCallback:) withObject:infoObj];
	}
}

- (void)setMessageCallback:(SEL)method withListener:(id)listener
{
	self.mMessageCallback = method;
	mMessageListener = listener;
}

- (void)performMessageCallback:(NSValue *)infoObj
{
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
	[mMessageListener performSelector:_mMessageCallback withObject:infoObj];
#pragma clang diagnostic pop
}

#pragma mark -

-(void)sendScore:(NSString *)score {
    if (mCsData.cs != NULL) {
        csoundReadScore(mCsData.cs, (char*)[score cStringUsingEncoding:NSASCIIStringEncoding]);
    }
}

#pragma mark -

-(void)addCompletionListener:(id<CsoundObjCompletionListener>)listener {
    [completionListeners addObject:listener];
}


-(CSOUND*)getCsound {
    if (!mCsData.running) {
        return NULL;
    }
    return mCsData.cs;
}

-(float*)getInputChannelPtr:(NSString*)channelName channelType:(controlChannelType)channelType
{
    float *value;
    csoundGetChannelPtr(mCsData.cs, &value, [channelName cStringUsingEncoding:NSASCIIStringEncoding],
                        channelType | CSOUND_INPUT_CHANNEL);
    return value;
}

-(float*)getOutputChannelPtr:(NSString *)channelName channelType:(controlChannelType)channelType
{
	float *value;
	csoundGetChannelPtr(mCsData.cs, &value, [channelName cStringUsingEncoding:NSASCIIStringEncoding],
                        channelType | CSOUND_OUTPUT_CHANNEL);
	return value;
}

-(NSData*)getOutSamples {
    if (!mCsData.running) {
        return nil;
    }
    CSOUND* csound = [self getCsound];
    float* spout = csoundGetSpout(csound);
    int nchnls = csoundGetNchnls(csound);
    int ksmps = csoundGetKsmps(csound);
    NSData* data = [NSData dataWithBytes:spout length:(nchnls * ksmps * sizeof(MYFLT))];
    return data;
}

-(int)getNumChannels {
    if (!mCsData.running) {
        return -1;
    }
    return csoundGetNchnls(mCsData.cs);
}
-(int)getKsmps {
    if (!mCsData.running) {
        return -1;
    }
    return csoundGetKsmps(mCsData.cs);
}

#pragma mark Csound Code

//-(void)startCsound:(NSString*)csdFilePath {
//
//    if(!mCsData.cs && !mCsData.running)
//    {
//
//        CSOUND* cs = csoundCreate(NULL);
//        csoundSetHostImplementedAudioIO(cs, 1, 0);
//
//        csoundSetMessageCallback(cs, messageCallback);
//        csoundSetHostData(cs, (__bridge void *)(self));
//
//        if (mMidiInEnabled) {
//            [CsoundMIDI setMidiInCallbacks:cs];
//        }
//
//        char *argv[2] = { "csound", (char*)[csdFilePath cStringUsingEncoding:NSASCIIStringEncoding]};
//        int ret = csoundCompile(cs, 2, argv);
//
//        if(!ret)
//        {
//            mCsData.cs = cs;
//            mCsData.ret = ret;
//            mCsData.nchnls = csoundGetNchnls(mCsData.cs);
//            mCsData.bufframes = (csoundGetOutputBufferSize(mCsData.cs))/mCsData.nchnls;
//            mCsData.running = true;
//            mCsData.valuesCache = valuesCache;
//
//            /* SETUP VALUE CACHEABLE */
//            for (int i = 0; i < valuesCache.count; i++) {
//                id<CsoundValueCacheable> cachedValue = [valuesCache objectAtIndex:i];
//                [cachedValue setup:self];
//            }
//
//            /* NOTIFY COMPLETION LISTENERS*/
//            for (id<CsoundObjCompletionListener> listener in completionListeners) {
//                [listener csoundObjDidStart:self];
//            }
//        }
//    }
//}
//
//
//-(void)stopCsound {
//
//    if(mCsData.cs && mCsData.running)
//    {
//
//        mCsData.running = false;
//        csoundDestroy(mCsData.cs);
//        mCsData.cs = nil;
//        mCsData.ret = 1;
//
//        /* CLEANUP VALUE CACHEABLE */
//        for (int i = 0; i < valuesCache.count; i++) {
//            id<CsoundValueCacheable> cachedValue = [valuesCache objectAtIndex:i];
//            [cachedValue cleanup];
//        }
//
//        /* NOTIFY COMPLETION LISTENERS*/
//        for (id<CsoundObjCompletionListener> listener in completionListeners) {
//            [listener csoundObjComplete:self];
//        }
//
//        [mMotionManager stopAccelerometerUpdates];
//        [mMotionManager stopGyroUpdates];
//        [mMotionManager stopDeviceMotionUpdates];
//    }
//}


-(void)startCsound:(NSString*)csdFilePath {
    
    [self performSelectorInBackground:@selector(runCsound:) withObject:csdFilePath];
}

-(void)stopCsound {
    mCsData.running = false;
}

-(void)muteCsound{
	mCsData.shouldMute = true;
}

-(void)unmuteCsound{
	mCsData.shouldMute = false;
}


-(void)recordToURL:(NSURL *)outputURL; {
    
    /* Start TAAE Recorder */
    if ( ![recorder beginRecordingToFileAtPath:outputURL.path
                                      fileType:kAudioFileWAVEType /* Must be Wav for AudioCopy compatibility */
                                         error:nil] ) {
        
        //report error here
    }
}

-(void)stopRecording {
    
    /* Stop TAAE Recorder */
    if (recorder) {
        [recorder finishRecording];
    }
}

-(void)startCsoundToDisk:(NSString*)csdFilePath outputFile:(NSString*)outputFile {
    //	mCsData.shouldRecord = false;
    
    [self performSelectorInBackground:@selector(runCsoundToDisk:)
                           withObject:[NSMutableArray arrayWithObjects:csdFilePath, outputFile, nil]];
}

-(void)runCsound:(NSString*)csdFilePath {
	
    @autoreleasepool {
		CSOUND *cs = csoundCreate(NULL);
        csoundSetHostImplementedAudioIO(cs, 1, 0);
		
		csoundSetMessageCallback(cs, messageCallback);
		csoundSetHostData(cs, (__bridge void *)(self));
        
        if (mMidiInEnabled) {
            [CsoundMIDI setMidiInCallbacks:cs];
        }
        
        char *argv[2] = { "csound", (char*)[csdFilePath cStringUsingEncoding:NSASCIIStringEncoding]};
		int ret = csoundCompile(cs, 2, argv);
		mCsData.running = true;
        
		if(!ret) {
            
			mCsData.cs = cs;
			mCsData.ret = ret;
			mCsData.nchnls = csoundGetNchnls(cs);
			mCsData.bufframes = (csoundGetOutputBufferSize(cs))/mCsData.nchnls;
			mCsData.running = true;
            mCsData.valuesCache = valuesCache;
            
            /* SETUP VALUE CACHEABLE */
            for (int i = 0; i < valuesCache.count; i++) {
                id<CsoundValueCacheable> cachedValue = [valuesCache objectAtIndex:i];
                [cachedValue setup:self];
            }
            
			/* NOTIFY COMPLETION LISTENERS*/
            for (id<CsoundObjCompletionListener> listener in completionListeners) {
                [listener csoundObjDidStart:self];
            }
            
            
            while (!mCsData.ret && mCsData.running) {
                [NSThread sleepForTimeInterval:.001];
            }
            
            mCsData.running = false;
            mCsData.ret = 1;
			csoundDestroy(cs);
            mCsData.cs = nil;
            
            /* CLEANUP VALUE CACHEABLE */
            for (int i = 0; i < valuesCache.count; i++) {
                id<CsoundValueCacheable> cachedValue = [valuesCache objectAtIndex:i];
                [cachedValue cleanup];
            }
            
            /* NOTIFY COMPLETION LISTENERS*/
            for (id<CsoundObjCompletionListener> listener in completionListeners) {
                [listener csoundObjComplete:self];
            }
            
            [mMotionManager stopAccelerometerUpdates];
            [mMotionManager stopGyroUpdates];
            [mMotionManager stopDeviceMotionUpdates];
        }
    }
}

-(void)runCsoundToDisk:(NSArray*)paths {
	
    @autoreleasepool {
        
        CSOUND *cs = csoundCreate(NULL);
        
        char *argv[4] = { "csound",
            (char*)[[paths objectAtIndex:0] cStringUsingEncoding:NSASCIIStringEncoding], "-o", (char*)[[paths objectAtIndex:1] cStringUsingEncoding:NSASCIIStringEncoding]};
        int ret = csoundCompile(cs, 4, argv);
        
        /* SETUP VALUE CACHEABLE */
        
        for (int i = 0; i < valuesCache.count; i++) {
            id<CsoundValueCacheable> cachedValue = [valuesCache objectAtIndex:i];
            [cachedValue setup:self];
        }
        
        /* NOTIFY COMPLETION LISTENERS*/
        
        for (id<CsoundObjCompletionListener> listener in completionListeners) {
            [listener csoundObjDidStart:self];
        }
        
        /* SET VALUES FROM CACHE */
        for (int i = 0; i < valuesCache.count; i++) {
			id<CsoundValueCacheable> cachedValue = [valuesCache objectAtIndex:i];
			[cachedValue updateValuesToCsound];
		}
        
        if(!ret) {
            
            csoundPerform(cs);
            csoundCleanup(cs);
            csoundDestroy(cs);
        }
        
        /* CLEANUP VALUE CACHEABLE */
        for (int i = 0; i < valuesCache.count; i++) {
            id<CsoundValueCacheable> cachedValue = [valuesCache objectAtIndex:i];
            [cachedValue cleanup];
        }
        
        /* NOTIFY COMPLETION LISTENERS*/
        for (id<CsoundObjCompletionListener> listener in completionListeners) {
            [listener csoundObjComplete:self];
        }
        
        [mMotionManager stopAccelerometerUpdates];
        [mMotionManager stopGyroUpdates];
        [mMotionManager stopDeviceMotionUpdates];
    }
}

-(void)INITIALIZE_TAAE_AB_IAA {
    
    /* CREATE TAAE CONTROLLER */
    AudioStreamBasicDescription asbd = [AEAudioController nonInterleavedFloatStereoAudioDescription];
    asbd.mSampleRate = SAMPLE_RATE_DEF;
    _TAAE_Controller = [[AEAudioController alloc] initWithAudioDescription:asbd inputEnabled:USE_AUDIO_INPUT];
    _TAAE_Controller.allowMixingWithOtherApps = YES;
    Float32 bufferSize = BUFFER_SIZE_DEF / SAMPLE_RATE_DEF;
    _TAAE_Controller.preferredBufferDuration = bufferSize;
    
    
    /* ==================================== AUDIOBUS ========================================= */
    /* ======================================================================================= */
    
    /* CREATE AUDIOBUS CONTROLLER */
    _AB_Controller = [[ABAudiobusController alloc] initWithApiKey:AUDIOBUS_KEY];
    
    /* Set ourselves as the State IO delegate, so we can send and receive app state when the user saves/loads a preset */
    //    _AB_Controller.stateIODelegate = self;
    
    /* Report that we're okay with appearing in multiple places in the connection graph at once */
    _AB_Controller.allowsConnectionsToSelf = NO;
    
#warning It Must Be Conforms to the Audio Components .plist Info
    /* Create sender and Filter ports */
    AudioComponentDescription desc_gen = { kAudioUnitType_RemoteGenerator,'csou','your', 0, 0 };
    AudioComponentDescription desc_fx = { kAudioUnitType_RemoteEffect,'csou','your', 0, 0 };
    _output = [[ABSenderPort alloc] initWithName:@"cs4dev Sender" title:@"cs4dev Sender" audioComponentDescription:desc_gen audioUnit:_TAAE_Controller.audioUnit];
    _output.derivedFromLiveAudioSource = YES;
    
    [_AB_Controller addSenderPort:_output];
    //    [_TAAE_Controller setAudiobusSenderPort:_output];
    
    _filter = [[ABFilterPort alloc] initWithName:@"cs4dev Filter" title:@"cs4dev Filter" audioComponentDescription:desc_fx  audioUnit:_TAAE_Controller.audioUnit];
    _filter.latency = BUFFER_SIZE_DEF;
    
    [_AB_Controller addFilterPort:_filter];
    
    
    /* ======================================================================================= */
    /* ======================================================================================= */
    
    // USE PLAYTHROUGH/FILTER
    //    __block void* _self = (__bridge void*) self;
    
    //AEPlaythroughChannel get Audio From Mic
    audioInput = [[AEPlaythroughChannel alloc] initWithAudioController:_TAAE_Controller];
    [_TAAE_Controller addInputReceiver:audioInput];
    [_TAAE_Controller addChannels:@[audioInput]];
    
    audioEngine = [AEBlockFilter filterWithBlock:^(AEAudioControllerFilterProducer producer,
                                                   void                     *producerToken,
                                                   const AudioTimeStamp     *time,
                                                   UInt32                   frames,
                                                   AudioBufferList          *audio) {
        
        
        /* Pull audio from System */
        OSStatus status = producer(producerToken, audio, &frames);
        if ( status != noErr ) return;
        
        /* CSOUND PERFORM */
        if (mCsData.running && mCsData.cs)
        {
            int i,j,k;
            int slices = frames/csoundGetKsmps(mCsData.cs);
            int ksmps = csoundGetKsmps(mCsData.cs);
            MYFLT *spin = csoundGetSpin(mCsData.cs);
            MYFLT *spout = csoundGetSpout(mCsData.cs);
            MYFLT *buffer;
            
            NSMutableArray* cache = mCsData.valuesCache;
            
            for(i=0; i < slices; i++){
                
                for (int i = 0; i < cache.count; i++) {
                    id<CsoundValueCacheable> cachedValue = [cache objectAtIndex:i];
                    [cachedValue updateValuesToCsound];
                }
                
                /* performance */
                if(USE_AUDIO_INPUT)
                {
                    for (k = 0; k < mCsData.nchnls; k++){
                        buffer = (MYFLT *) audio->mBuffers[k].mData;
                        for(j=0; j < ksmps; j++){
                            spin[j*mCsData.nchnls+k] = buffer[j+i*ksmps];
                        }
                    }
                }
                if(!mCsData.ret) {
                    mCsData.ret = csoundPerformKsmps(mCsData.cs);
                } else {
                    mCsData.running = false;
                }
                
                for (k = 0; k < mCsData.nchnls; k++) {
                    buffer = (MYFLT *) audio->mBuffers[k].mData;
                    if (mCsData.shouldMute == false) {
                        for(j=0; j < ksmps; j++){
                            buffer[j+i*ksmps] = (MYFLT) spout[j*mCsData.nchnls+k] ;
                        }
                    } else {
                        memset(buffer, 0, sizeof(MYFLT) * frames);
                    }
                }
                
                
                for (int i = 0; i < cache.count; i++) {
                    id<CsoundValueCacheable> cachedValue = [cache objectAtIndex:i];
                    [cachedValue updateValuesFromCsound];
                }
            }
        }
        else {
            
            
            /* Mute your audio input */
            for( UInt32 i = 0; i < audio->mNumberBuffers; i++ )
                memset( audio->mBuffers[i].mData, 0, audio->mBuffers[i].mDataByteSize );
        }
    }];
    
    [_TAAE_Controller addFilter:audioEngine toChannel:audioInput];
    
    
    /* FOR EXAMPLE SETUP HERE YOUR AUDIO UNIT */
    /* Low Shelf */
    //    auEffectUnit_1 = [[AEAudioUnitFilter alloc] initWithComponentDescription:AEAudioComponentDescriptionMake(kAudioUnitManufacturer_Apple,
    //                                                                                                             kAudioUnitType_Effect,
    //                                                                                                             kAudioUnitSubType_LowShelfFilter)
    //                                                             audioController:_TAAE_Controller error:NULL];
    //
    //
    //    /* High Shelf */
    //    auEffectUnit_2 = [[AEAudioUnitFilter alloc] initWithComponentDescription:AEAudioComponentDescriptionMake(kAudioUnitManufacturer_Apple,
    //                                                                                                             kAudioUnitType_Effect,
    //                                                                                                             kAudioUnitSubType_HighShelfFilter)
    //                                                             audioController:_TAAE_Controller error:NULL];
    //
    //    /* Dynamic */
    //    auEffectUnit_3 = [[AEAudioUnitFilter alloc] initWithComponentDescription:AEAudioComponentDescriptionMake(kAudioUnitManufacturer_Apple,
    //                                                                                                             kAudioUnitType_Effect,
    //                                                                                                             kAudioUnitSubType_DynamicsProcessor)
    //                                                             audioController:_TAAE_Controller error:NULL];
    //
    //
    
    
    /* CONNECTIONS  */
    //    [_TAAE_Controller addFilter:auEffectUnit_1];
    //    [_TAAE_Controller addFilter:auEffectUnit_2];
    //    [_TAAE_Controller addFilter:auEffectUnit_3];
    
    
    /* Init recorder */
    recorder = [[AERecorder alloc] initWithAudioController:_TAAE_Controller];
    // Receive both audio input and audio output. Note that if you're using
    // AEPlaythroughChannel, mentioned above, you may not need to receive the input again.
    
    //    [_TAAE_Controller addInputReceiver:recorder];
    [_TAAE_Controller addOutputReceiver:recorder];
    
    
    /* START TAAE */
    [_TAAE_Controller start:NULL];
    
#ifdef IAA
    [self addAudioUnitPropertyListener];
    [self publishInterAudioApp];
#endif
}

#ifdef IAA
#pragma Inter-App Audio
-(void)publishInterAudioApp {
    
    /* Alessandro Petrolati Set's Inter-app Audio You need Set's AudioComponent in Plist and entitlements */
    if ([CsoundObj interAppReady] && _TAAE_Controller.audioUnit)
    {
#warning It Must Be Conforms to the Audio Components .plist Info
        AudioComponentDescription desc_instr = { kAudioUnitType_RemoteInstrument,'csou','your', 0, 0 };
        
        OSStatus result = AudioOutputUnitPublish(&desc_instr, (CFStringRef)@"cs4dev (Instrument)", 1, _TAAE_Controller.audioUnit);
        if (result != noErr)
            NSLog(@"AudioOutputUnitPublish Instrument result: %d", (int)result);
        
        
        AudioComponentDescription desc_fx = { kAudioUnitType_RemoteEffect,'csou','your', 0, 0 };
        
        result = AudioOutputUnitPublish(&desc_fx, (CFStringRef)@"cs4dev (FX)", 1, _TAAE_Controller.audioUnit);
        if (result != noErr)
            NSLog(@"AudioOutputUnitPublish FX result: %d", (int)result);
        
        AudioComponentDescription desc_gen = { kAudioUnitType_RemoteGenerator,'csou','your', 0, 0 };
        
        result = AudioOutputUnitPublish(&desc_gen, (CFStringRef)@"cs4dev (Generator)", 1, _TAAE_Controller.audioUnit);
        if (result != noErr)
            NSLog(@"AudioOutputUnitPublish Generator result: %d", (int)result);
        
        /* Manage MIDI from IAA */
        [self setupMidiCallBacks:NULL userData:(__bridge void *)(self)];
    }
}

+ (BOOL)interAppReady
{
    NSArray *versionCompatibility = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
    if (versionCompatibility && versionCompatibility.count)
    {
        NSNumber *version = [versionCompatibility objectAtIndex:0];
        if (version && version.integerValue > 6)
        {
            return YES;
        }
    }
    return NO;
}

#pragma mark - Inter-app Audio
+ (void) setAudioSessionActive {
    
    //get your app's audioSession singleton object
    AVAudioSession *session = [AVAudioSession sharedInstance];
    
    //error handling
    BOOL success;
    NSError* error;
    
    success = [session setCategory: AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionMixWithOthers error:  &error];
    if (!success)  NSLog(@"AVAudioSession error setting category:%@", error);
    
    // 2. Changing the default output audio route
    //    UInt32 doChangeDefaultRoute = 1;
    //    Check(AudioSessionSetProperty(kAudioSessionProperty_OverrideCategoryDefaultToSpeaker, sizeof(doChangeDefaultRoute), &doChangeDefaultRoute));
    
    success = [session overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker
                                         error:&error];
    if (!success)  NSLog(@"AVAudioSession error overrideOutputAudioPort:%@", error);
    
    //activate the audio session
    success = [session setActive:YES error:&error];
    if (!success) NSLog(@"AVAudioSession error activating: %@", error);
    else NSLog(@"audioSession active");
}

+ (void) setAudioSessionInActive {
    
    AVAudioSession *session = [AVAudioSession sharedInstance];
    NSError *err;
    [session setActive: NO error:  &err];
}

-(void) setupMidiCallBacks:(AudioUnit*)output userData:(void*)inUserData
{
    
    CsoundObj* _self = (__bridge CsoundObj*) inUserData;
    AudioUnit au_ = _self->_TAAE_Controller.audioUnit;
    
    AudioOutputUnitMIDICallbacks callBackStruct;
    callBackStruct.userData = inUserData;
    callBackStruct.MIDIEventProc = MIDIEventProcCallBack;
    callBackStruct.MIDISysExProc = NULL;
    AudioUnitSetProperty (au_,
                          kAudioOutputUnitProperty_MIDICallbacks,
                          kAudioUnitScope_Global,
                          0,
                          &callBackStruct,
                          sizeof(callBackStruct));
}

void MIDIEventProcCallBack(void *userData, UInt32 inStatus, UInt32 inData1, UInt32 inData2, UInt32 inOffsetSampleFrame)
{
    //fprintf(stderr,"midi received from host\n");
    
    //parse note on/off
    if (inStatus == 144) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
//            NSArray* state = [[NSArray alloc] initWithObjects:[NSNumber numberWithInt:inData1], [NSNumber numberWithInt:inData2], nil];
//            [[NSNotificationCenter defaultCenter]
//             postNotificationName:@"MIDI_NOTE_ON"
//             object:state];
            NSLog(@"MIDI_NOTE_ON");
        });
    }
    else if (inStatus == 128) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
//            NSArray* state = [[NSArray alloc] initWithObjects:[NSNumber numberWithInt:inData1], [NSNumber numberWithInt:inData2], nil];
//            [[NSNotificationCenter defaultCenter]
//             postNotificationName:@"MIDI_NOTE_OFF"
//             object:state];
            NSLog(@"MIDI_NOTE_OFF");
        });
        
    }
}

-(void)addAudioUnitPropertyListener
{
    
    if ([CsoundObj interAppReady] && _TAAE_Controller.audioUnit)
    {
        /* APE commented */
        AudioUnitRemovePropertyListenerWithUserData(_TAAE_Controller.audioUnit, kAudioUnitProperty_IsInterAppConnected, AudioUnitPropertyChangeDispatcher, (__bridge void *)(self));
        AudioUnitRemovePropertyListenerWithUserData(_TAAE_Controller.audioUnit, kAudioOutputUnitProperty_HostTransportState, AudioUnitPropertyChangeDispatcher, (__bridge void *)(self));
        
        AudioUnitAddPropertyListener(_TAAE_Controller.audioUnit,
                                     kAudioUnitProperty_IsInterAppConnected,
                                     AudioUnitPropertyChangeDispatcher,
                                     (__bridge void *)(self));
        AudioUnitAddPropertyListener(_TAAE_Controller.audioUnit,
                                     kAudioOutputUnitProperty_HostTransportState,
                                     AudioUnitPropertyChangeDispatcher,
                                     (__bridge void *)(self));
        
    }
}

void AudioUnitPropertyChangeDispatcher(void *inRefCon, AudioUnit inUnit, AudioUnitPropertyID inID, AudioUnitScope inScope, AudioUnitElement inElement)
{
    CsoundObj *audio = (__bridge CsoundObj *)inRefCon;
    
    if (inID==kAudioUnitProperty_IsInterAppConnected)
    {
        UInt32 connect;
        UInt32 dataSize = sizeof(UInt32);
        AudioUnitGetProperty(inUnit, kAudioUnitProperty_IsInterAppConnected, kAudioUnitScope_Global, 0, &connect, &dataSize);
        if (connect)
        {
            NSLog(@"%s -------------------------------------- %@", __FUNCTION__, @"CONNECTED");
            
            //            audio->_connected = YES;
            
            [CsoundObj setAudioSessionActive];
            
            AudioOutputUnitStart(audio.TAAE_Controller.audioUnit);
            
            /* VERIFY IT!!!! */
            AudioOutputUnitStart(inUnit);
            
#warning setup here IAA transport, see Apple Documentation
            //            [audio updateTransportInfo];
            //            [audio postUpdateStateNotification];
            
        }
        else
        {
            NSLog(@"%s -------------------------------------- %@", __FUNCTION__, @"UN-CONNECTED");
            //            audio->_connected = NO;
            [CsoundObj setAudioSessionActive];
            /* APE very important here, we don't wont audio stops when disconnecting */
            AudioOutputUnitStart(audio.TAAE_Controller.audioUnit);
            
            //            [audio updateTransportInfo];
            //            [audio postUpdateStateNotification];
        }
    }
    if (inID==kAudioOutputUnitProperty_HostTransportState)
    {
        NSLog(@"%s -------------------------------------- %@", __FUNCTION__, @"kAudioOutputUnitProperty_HostTransportState");
        
#warning setup here IAA transport, see Apple Documentation
        //        [audio updateTransportInfo];
        //        [audio postUpdateStateNotification];
    }
}
#endif
@end
