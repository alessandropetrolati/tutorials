/*
cs4dev tutorial 02
by Alessandro Petrolati
www.apesoft.it
*/

<CsoundSynthesizer>
<CsOptions>

-o dac
-+rtmidi=null
-+rtaudio=null
-d
-+msg_color=0
--expression-opt
-M0
-m0
-i adc

</CsOptions>
<CsInstruments>

sr = 44100
ksmps = 64
nchnls = 2
0dbfs = 1

instr 1
ain1, ain2 ins

kfr chnget "freq"
a1 oscili 1, kfr
outs a1*ain1,a1*ain2
endin

instr 2

kamp chnget "noise_amp"
;a1 noise kamp, 0
a1 oscili kamp, 880

outs a1, a1
endin

</CsInstruments>
<CsScore>

i 1 0 10000
i 2 0 10000

</CsScore>
</CsoundSynthesizer>
