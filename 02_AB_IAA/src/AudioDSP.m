//
//  AudioDSP.m
//  02_AB_IAA
//
//  Created by Alessandro Petrolati on 29/05/15.
//  Copyright (c) 2015 apeSoft. All rights reserved.
//

#import "AudioDSP.h"
#ifdef ENABLE_MIDI
#import "CsoundMIDI.h"
#endif

@interface AudioDSP ()

-(void)runCsound:(NSString*)csdFilePath;

@end

@implementation AudioDSP

@synthesize cs = _cs;

#ifdef IAA
@synthesize connected = _connected;
#endif

#ifdef AB
@synthesize AB_Controller = _AB_Controller;
@synthesize output = _output;
@synthesize filter = _filter;
#endif

- (instancetype)init {
    self = [super init];
    if (self) {
        
        NSLog(@"%s", __FUNCTION__);
        
        // Creates an instance of Csound
        _cs = csoundCreate(NULL);
        
        // Setup CoreAudio
        [self initializeAudio];
    }
    return self;
}

-(void)dealloc {
    
    if (_cs) {
        csoundDestroy(_cs);
    }
}

-(void)awakeFromNib {
    
    [super awakeFromNib];
    
    NSLog(@"%s", __FUNCTION__);
    
    // Configure UI Widgets
}

-(void)initializeAudio {
    
    /* Audio Session handler */
    AVAudioSession* session = [AVAudioSession sharedInstance];
    
    NSError* error = nil;
    BOOL success = NO;
    
    success = [session setCategory:AVAudioSessionCategoryPlayAndRecord
                       withOptions:(AVAudioSessionCategoryOptionMixWithOthers |
                                    AVAudioSessionCategoryOptionDefaultToSpeaker
#if ENABLE_BLUETOOTH
                                    | AVAudioSessionCategoryOptionAllowBluetooth
#endif
                                    )
                             error:&error];
    
    success = [session setActive:YES error:&error];
    
    
    /* Sets Interruption Listner */
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(InterruptionListener:)
                                                 name:AVAudioSessionInterruptionNotification
                                               object:session];
    
    AudioComponentDescription defaultOutputDescription;
    defaultOutputDescription.componentType = kAudioUnitType_Output;
    defaultOutputDescription.componentSubType = kAudioUnitSubType_RemoteIO;
    defaultOutputDescription.componentManufacturer = kAudioUnitManufacturer_Apple;
    defaultOutputDescription.componentFlags = 0;
    defaultOutputDescription.componentFlagsMask = 0;
    
    // Get the default playback output unit
    AudioComponent HALOutput = AudioComponentFindNext(NULL, &defaultOutputDescription);
    NSAssert(HALOutput, @"Can't find default output");
    
    // Create a new unit based on this that we'll use for output
    err = AudioComponentInstanceNew(HALOutput, &csAUHAL);
    
    // Enable IO for recording
    UInt32 flag = 1;
    err = AudioUnitSetProperty(csAUHAL,
                               kAudioOutputUnitProperty_EnableIO,
                               kAudioUnitScope_Input,
                               1,
                               &flag,
                               sizeof(flag));
    // Enable IO for playback
    err = AudioUnitSetProperty(csAUHAL,
                               kAudioOutputUnitProperty_EnableIO,
                               kAudioUnitScope_Output,
                               0,
                               &flag,
                               sizeof(flag));
    
    err = AudioUnitInitialize(csAUHAL);
    
    /* AUDIOBUS and IAA  */
    [self initializeAB_IAA];
}

-(void)initializeAB_IAA {
    
#if! TARGET_OS_MACCATALYST
#warning: in the AudioComponents key of Info.plist you must declare the same informations
    /* icso, iyou, xcso, xyou */
    
    /* Create Sender and Filter ports */
    AudioComponentDescription desc_instr = {
        kAudioUnitType_RemoteInstrument,
        'icso',
        'iyou', 0, 0 };
    
    AudioComponentDescription desc_fx = {
        kAudioUnitType_RemoteEffect,
        'xcso',
        'xyou', 0, 0 };
#endif
    
    /* ===================== Inter-App Audio Setup ========================== */
#ifdef IAA
    
    [self addAudioUnitPropertyListener];
    [self setupMidiCallBacks:&csAUHAL userData:(__bridge void *)(self)];
    
    OSStatus result;
    
    result = AudioOutputUnitPublish(&desc_instr,
                                             (CFStringRef)@"02_AB_IAA (instr)",
                                             1, csAUHAL);
    
    result = AudioOutputUnitPublish(&desc_fx,
                                    (CFStringRef)@"02_AB_IAA (fx)",
                                    1, csAUHAL);
    
#endif
    
    
    /* ===================== Audiobus Setup ========================== */
#ifdef AB
    
//    MTQ4OTI0MjIyOSoqKjAyX0FCX0lBQSoqKjAyLUFCLUlBQS5hdWRpb2J1czovLyoqKlthdXJpLml5b3UuaWNzby4xXVthdXJ4Lnh5b3UueGNzby4xXQ==:Nvv08/8XaZ6RyJZwI3KGn4Ze+3ReW33XxTm5X5mRN/vMO66Wfsztdqlou6/2aai/jUho4NkKc5CUmtKEfEuKa1FAKzSOrF2JsqnZv5pzjAiivcjWyWmfRLEaM376QVtn
    
    _AB_Controller = [[ABAudiobusController alloc] initWithApiKey:@"YOUR_AB_KEY"];
    _AB_Controller.allowsConnectionsToSelf = NO;
    _AB_Controller.connectionPanelPosition = ABConnectionPanelPositionRight;
    
    _output = [[ABSenderPort alloc] initWithName:@"02_AB_IAA (instr)"
                                           title:@"02_AB_IAA (instr)"
                       audioComponentDescription:desc_instr
                                       audioUnit:csAUHAL];
    
    _output.derivedFromLiveAudioSource = YES;
    
    [_AB_Controller addSenderPort:_output];
    
    _filter = [[ABFilterPort alloc] initWithName:@"cs4dev 02_AB_IAA(fx)"
                                           title:@"cs4dev 02_AB_IAA(fx)"
                       audioComponentDescription:desc_fx
                                       audioUnit:csAUHAL];
    
    //_filter.latency = _currentKsmps;
    
    [_AB_Controller addFilterPort:_filter];
    
#endif
    
}

-(void)startCsound:(NSString*)csdFilePath {
    
    shouldRecord = false;
    
    [self runCsound:csdFilePath];

    /* NOTIFY COMPLETION LISTENER */

    [self csoundObjDidStart];

    // must be at the end of this function
    AudioOutputUnitStart(csAUHAL);
}

-(void)stopCsound {

    // must be at the beginning
    AudioOutputUnitStop(csAUHAL);
    
    running = false;
    
    if (shouldRecord)
        [self stopRecording];
    
    csoundStop(_cs);
    csoundCleanup(_cs);
    csoundReset(_cs);

    /* NOTIFY COMPLETION LISTENER */
    [self csoundObjComplete];
}

-(void)recordToURL:(NSURL *)outputURL_
{
    if (!_cs || !running) {
        return;
    }
    
    // Define format for the audio file.
    AudioStreamBasicDescription destFormat, clientFormat;
    memset(&destFormat, 0, sizeof(AudioStreamBasicDescription));
    memset(&clientFormat, 0, sizeof(AudioStreamBasicDescription));
    destFormat.mFormatID = kAudioFormatLinearPCM;
    destFormat.mFormatFlags = kLinearPCMFormatFlagIsPacked | kLinearPCMFormatFlagIsSignedInteger;
    destFormat.mSampleRate = csoundGetSr(_cs);
    destFormat.mChannelsPerFrame = nchnls;
    destFormat.mBytesPerPacket = nchnls * 2;
    destFormat.mBytesPerFrame = nchnls * 2;
    destFormat.mBitsPerChannel = 16;
    destFormat.mFramesPerPacket = 1;
    
    // Create the audio file.
    CFURLRef fileURL = (__bridge CFURLRef)outputURL_;
    err = ExtAudioFileCreateWithURL(fileURL, kAudioFileWAVEType, &destFormat, NULL, kAudioFileFlags_EraseFile, &(recFile));
    if (err == noErr) {
        // Get the stream format from the AU...
        UInt32 propSize = sizeof(AudioStreamBasicDescription);
        AudioUnitGetProperty((csAUHAL), kAudioUnitProperty_StreamFormat, kAudioUnitScope_Input, 0, &clientFormat, &propSize);
        // ...and set it as the client format for the audio file. The file will use this
        // format to perform any necessary conversions when asked to read or write.
        ExtAudioFileSetProperty(recFile, kExtAudioFileProperty_ClientDataFormat, sizeof(clientFormat), &clientFormat);
        // Warm the file up.
        ExtAudioFileWriteAsync(recFile, 0, NULL);
        
        shouldRecord = true;
        
    } else {
        printf("***Not recording. Error: %d\n", (int)err);
        
        shouldRecord = false;
    }
}

-(void)stopRecording
{
    shouldRecord = false;
    ExtAudioFileDispose(recFile);
}

-(void)runCsound:(NSString*)csdFilePath {
    
    csoundSetHostImplementedAudioIO(_cs, 1, 0);
    csoundSetHostData(_cs, (__bridge void *)(self));
#ifdef ENABLE_MIDI
    [CsoundMIDI setMidiInCallbacks:_cs];
#endif
    
    // Set's Environment Sound Files Dir
    NSString *resourcesPath = [[NSBundle mainBundle] resourcePath];
    
    NSString* envFlag = @"--env:SFDIR+=";
    char* SFDIR = (char*)[[envFlag stringByAppendingString:resourcesPath] cStringUsingEncoding:NSASCIIStringEncoding];
    envFlag = @"--env:SADIR+=";
    char* SADIR = (char*)[[envFlag stringByAppendingString:resourcesPath] cStringUsingEncoding:NSASCIIStringEncoding];
    
    const char *argv[4] = { "csound", SFDIR, SADIR, (char*)[csdFilePath cStringUsingEncoding:NSASCIIStringEncoding]};
    ret = csoundCompile(_cs, 4, argv);
    
    if(!ret) {
        nchnls = csoundGetNchnls(_cs);
        bufframes = (csoundGetOutputBufferSize(_cs))/nchnls;
        running = true;
    }
    
    AudioStreamBasicDescription format;
    
    if(!err) {
        
        UInt32 maxFPS;
        UInt32 outsize;
        int elem;
        for(elem = 1; elem >= 0; elem--){
            outsize = sizeof(maxFPS);
            AudioUnitGetProperty(csAUHAL, kAudioUnitProperty_MaximumFramesPerSlice, kAudioUnitScope_Global, elem, &maxFPS, &outsize);
            AudioUnitSetProperty(csAUHAL, kAudioUnitProperty_MaximumFramesPerSlice, kAudioUnitScope_Global, elem, (UInt32*)&(bufframes), sizeof(UInt32));
            outsize = sizeof(AudioStreamBasicDescription);
            AudioUnitGetProperty(csAUHAL, kAudioUnitProperty_StreamFormat, (elem ? kAudioUnitScope_Output : kAudioUnitScope_Input), elem, &format, &outsize);
            format.mSampleRate	= csoundGetSr(_cs);
            format.mFormatID = kAudioFormatLinearPCM;
            format.mFormatFlags = kAudioFormatFlagIsFloat | kAudioFormatFlagIsPacked | kAudioFormatFlagIsNonInterleaved;
            format.mBytesPerPacket = sizeof(MYFLT);
            format.mFramesPerPacket = 1;
            format.mBytesPerFrame = sizeof(MYFLT);
            format.mChannelsPerFrame = nchnls;
            format.mBitsPerChannel = sizeof(MYFLT)*8;
            err = AudioUnitSetProperty(csAUHAL, kAudioUnitProperty_StreamFormat, (elem ? kAudioUnitScope_Output : kAudioUnitScope_Input), elem, &format, sizeof(AudioStreamBasicDescription));
        }
        
        if(!err) {
            // Set input callback
            AURenderCallbackStruct callbackStruct;
            // Set output callback
            callbackStruct.inputProc = Csound_Perform;
            callbackStruct.inputProcRefCon = (__bridge void *)(self);
            err = AudioUnitSetProperty(csAUHAL,
                                       kAudioUnitProperty_SetRenderCallback,
                                       kAudioUnitScope_Global,
                                       0,
                                       &callbackStruct,
                                       sizeof(callbackStruct));
            
         
        }
    }
}


#pragma mark Csound Code

//Called when inNumberFrames >= ksmps
OSStatus  Csound_Perform(void *inRefCon,
                         AudioUnitRenderActionFlags *ioActionFlags,
                         const AudioTimeStamp *inTimeStamp,
                         UInt32 dump,
                         UInt32 inNumberFrames,
                         AudioBufferList *ioData
                         )
{
    
    AudioDSP *cdata = (__bridge AudioDSP*) inRefCon;
    
    AudioUnitRender(cdata->csAUHAL, ioActionFlags, inTimeStamp, 1, inNumberFrames, ioData);
    
    int ret = cdata->ret, nchnls = cdata->nchnls;
    CSOUND *cs = cdata->_cs;
    float slices = inNumberFrames/csoundGetKsmps(cs);
    int ksmps = csoundGetKsmps(cs);
    MYFLT *spin = csoundGetSpin(cs);
    MYFLT *spout = csoundGetSpout(cs);
    MYFLT *buffer;
    
    
    /* CSOUND PERFORM */
    if (slices < 1.0) {
        /* inNumberFrames < ksmps */
        Csound_Perform_DOWNSAMP(inRefCon, ioActionFlags, inTimeStamp, dump, inNumberFrames, ioData);
    }
    else
    {
        /* inNumberFrames => ksmps */
        for(int i = 0; i < (int)slices; ++i){
            
            /* performance */
            for (int k = 0; k < nchnls; ++k) {
                buffer = (MYFLT *) ioData->mBuffers[k].mData;
                for(int j = 0; j < ksmps; ++j) {
                    spin[j*nchnls+k] = buffer[j+i*ksmps];
                }
            }
            
            if(!ret) {
                ret = csoundPerformKsmps(cs);
            } else {
                cdata->running = false;
            }
            
            for (int k = 0; k < nchnls; ++k) {
                buffer = (MYFLT *) ioData->mBuffers[k].mData;
                
                for(int j = 0; j < ksmps; ++j) {
                    buffer[j+i*ksmps] = (MYFLT) spout[j*nchnls+k];
                }
            }
            
        }
        
        cdata->ret = ret;
    }
    
    /* Write to file */
    if (cdata->shouldRecord) {
        OSStatus err = ExtAudioFileWriteAsync(cdata->recFile, inNumberFrames, ioData);
        if (err != noErr) {
            printf("***Error writing to file: %d\n", (int)err);
        }
    }
    
    return noErr;
}

//Called when inNumberFrames < ksmps
OSStatus  Csound_Perform_DOWNSAMP(void *inRefCon,
                                  AudioUnitRenderActionFlags *ioActionFlags,
                                  const AudioTimeStamp *inTimeStamp,
                                  UInt32 dump,
                                  UInt32 inNumberFrames,
                                  AudioBufferList *ioData
                                  )
{
    AudioDSP *cdata = (__bridge AudioDSP*) inRefCon;
    
    int ret = cdata->ret, nchnls = cdata->nchnls;
    CSOUND *cs = cdata->_cs;
    
    MYFLT *spin = csoundGetSpin(cs);
    MYFLT *spout = csoundGetSpout(cs);
    MYFLT *buffer;
    
    
    /* DOWNSAMPLING FACTOR */
    int UNSAMPLING = csoundGetKsmps(cs)/inNumberFrames;
    
    if (cdata->counter < UNSAMPLING-1) {
        
        cdata->counter++;
    }
    else {
        
        cdata->counter = 0;
        
        /* CSOUND PROCESS KSMPS */
        if(!cdata->ret) {
            /* PERFORM CSOUND */
            cdata->ret = csoundPerformKsmps(cs);
        } else {
            cdata->running = false;
            
        }
    }
    
    /* INCREMENTS DOWNSAMPLING COUNTER */
    int slice_downsamp = inNumberFrames * cdata->counter;
    
    /* COPY IN CSOUND SYSTEM SLICE INPUT */
    for (int k = 0; k < nchnls; ++k){
        buffer = (MYFLT *) ioData->mBuffers[k].mData;
        for(int j = 0; j < inNumberFrames; ++j){
            spin[(j+slice_downsamp)*nchnls+k] = buffer[j];
        }
    }
    
    /* COPY OUT CSOUND KSMPS SLICE */
    for (int k = 0; k < nchnls; ++k) {
        buffer = (MYFLT *) ioData->mBuffers[k].mData;
        for(int j = 0; j < inNumberFrames; ++j) {
            
            buffer[j] = (MYFLT) spout[(j+slice_downsamp)*nchnls+k];
        }
    }
    
    cdata->ret = ret;
    return  noErr;
}

#pragma mark CsoundObjCompletionListener

-(void)csoundObjDidStart {
    
    NSLog(@"%s", __FUNCTION__);
    
    [freq sendActionsForControlEvents:UIControlEventAllEvents];
    [noiseAmp sendActionsForControlEvents:UIControlEventAllEvents];
    
    [rec setEnabled:YES];
}

-(void)csoundObjComplete {
    
    NSLog(@"%s", __FUNCTION__);
    
    if (rec.on) {
        rec.on = NO;
    }
    
    [rec setEnabled:NO];
}

#pragma mark - Inter-app Audio Change Dispather delegate
void AudioUnitPropertyChangeDispatcher(void *inRefCon, AudioUnit inUnit, AudioUnitPropertyID inID, AudioUnitScope inScope, AudioUnitElement inElement) {
#ifdef IAA
    AudioDSP *audio = (__bridge AudioDSP *)inRefCon;
    
    if (inID==kAudioUnitProperty_IsInterAppConnected)
    {
        UInt32 connect;
        UInt32 dataSize = sizeof(UInt32);
        AudioUnitGetProperty(inUnit, kAudioUnitProperty_IsInterAppConnected, kAudioUnitScope_Global, 0, &connect, &dataSize);
        if (connect)
        {
            NSLog(@"%s -> %@", __FUNCTION__, @"CONNECTED");
            
            audio->_connected = YES;
            [AudioDSP setAudioSessionActive];
            AudioOutputUnitStart(audio->csAUHAL);
            AudioOutputUnitStart(inUnit);
            
        }
        else
        {
            NSLog(@"%s -> %@", __FUNCTION__, @"UN-CONNECTED");
            audio->_connected = NO;
            [AudioDSP setAudioSessionActive];
            /* Important, we don't wont audio stops when disconnecting */
            AudioOutputUnitStart(audio->csAUHAL);
            
            // update and post IAA transport state
        }
    }
    if (inID==kAudioOutputUnitProperty_HostTransportState)
    {
        NSLog(@"%s -> %@", __FUNCTION__, @"kAudioOutputUnitProperty_HostTransportState");
        // update and post IAA transport state
    }
#endif
}

#pragma mark - Inter-App Audio
+ (void) setAudioSessionActive {
    
    AVAudioSession *session = [AVAudioSession sharedInstance];
    NSError *err;
    [session setCategory: AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionMixWithOthers
     
#if ENABLE_BLUETOOTH
     | AVAudioSessionCategoryOptionAllowBluetooth
#endif
                   error:  &err];
    
    [session setActive: YES error:  &err];
}

+ (void) setAudioSessionInActive {
    
    AVAudioSession *session = [AVAudioSession sharedInstance];
    NSError *err;
    [session setActive: NO error:  &err];
}

-(void)addAudioUnitPropertyListener {
#ifdef IAA
    AudioUnitAddPropertyListener(csAUHAL,
                                 kAudioUnitProperty_IsInterAppConnected,
                                 AudioUnitPropertyChangeDispatcher,
                                 (__bridge void *)(self));
    AudioUnitAddPropertyListener(csAUHAL,
                                 kAudioOutputUnitProperty_HostTransportState,
                                 AudioUnitPropertyChangeDispatcher,
                                 (__bridge void *)(self));
#endif
}

-(void) setupMidiCallBacks:(AudioUnit*)output userData:(void*)inUserData {
#ifdef IAA
    AudioOutputUnitMIDICallbacks callBackStruct;
    callBackStruct.userData = inUserData;
    callBackStruct.MIDIEventProc = MIDIEventProcCallBack;
    callBackStruct.MIDISysExProc = NULL;
    AudioUnitSetProperty (*output,
                          kAudioOutputUnitProperty_MIDICallbacks,
                          kAudioUnitScope_Global,
                          0,
                          &callBackStruct,
                          sizeof(callBackStruct));
#endif
}

#pragma mark IAA MIDI Receive Data
void MIDIEventProcCallBack(void *userData, UInt32 inStatus, UInt32 inData1, UInt32 inData2, UInt32 inOffsetSampleFrame) {
    fprintf(stderr,"midi received from host\n");
    
    //parse note on/off
    if (inStatus == 144 || inStatus == 128) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            int noteNum = inData1;
            int noteVel = inData2;
            
            //Receive MIDI Note On
            if (inStatus == 144) {
                NSLog(@"%s -> NOTE_ON_NUM -> NOTE_VELOC %d %d", __FUNCTION__, noteNum, noteVel);
            }
            
            //Receive MIDI Note Off
            if(inStatus == 128) {
                NSLog(@"%s -> NOTE_OFF_NUM -> NOTE_VELOC %d %d", __FUNCTION__, noteNum, noteVel);
            }
        });
    }
    
    // Parse CC and others MIDI events
    //...
}

#pragma mark AVAudioSessionInterruptionNotification

-(void)InterruptionListener: (NSNotification*) aNotification
{
    NSDictionary *interuptionDict = aNotification.userInfo;
    NSNumber* interuptionType = (NSNumber*)[interuptionDict valueForKey:AVAudioSessionInterruptionTypeKey];
    
    if([interuptionType intValue] == AVAudioSessionInterruptionTypeBegan) {
        
        NSLog(@"_____________________________%s Begin Interruption", __FUNCTION__);
        
        AudioOutputUnitStop(csAUHAL);
    }
    
    else if ([interuptionType intValue] == AVAudioSessionInterruptionTypeEnded) {
        
        NSLog(@"_____________________________%s End Interruption", __FUNCTION__);
        AudioOutputUnitStart(csAUHAL);
    }
}

#pragma  mark - User Interface Actions

-(IBAction)toggleOnOff:(id)component {
    
    UISwitch* uiswitch = (UISwitch*)component;
    NSLog(@"%s -> Status: %d", __FUNCTION__, [uiswitch isOn]);
    
    if(uiswitch.on) {
        
        NSString *tempFile = [[NSBundle mainBundle] pathForResource:@"csound_rm" ofType:@"csd"];
        NSLog(@"FILE PATH: %@", tempFile);
        
        [self stopCsound];
        [self startCsound:tempFile];
        
    } else {
        [self stopCsound];
    }
}

-(IBAction)toggleRec:(id)component {
    
    UISwitch* uiswitch = (UISwitch*)component;
    NSLog(@"%s -> Status: %d", __FUNCTION__, [uiswitch isOn]);
    
    if(uiswitch.on) {
        
        NSArray *documentPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentFolder = [documentPath objectAtIndex:0];
        NSString *soundFilePath = [documentFolder stringByAppendingPathComponent:[NSString stringWithFormat: @"output.wav"]];
        NSURL* outUrl = [NSURL fileURLWithPath:soundFilePath];
        
        // create 32 bit output file
        [self recordToURL:outUrl];
        
        NSLog(@"REC FILE PATH: %@", outUrl);
        
    } else {
        
        [self stopRecording];
    }
}

- (IBAction)setRmFrequency:(id)sender {
    
    UISlider* sld = sender;
    
    NSLog(@"%s -> RM FREQ. = %f", __FUNCTION__, sld.value);
    [hzLabel setText:[NSString stringWithFormat:@"Frequency = %f", sld.value]];
    
    if (!_cs || !running) return;
    
    NSString* channelName = @"freq";
    float *value;
    csoundGetChannelPtr(_cs, &value, [channelName cStringUsingEncoding:NSASCIIStringEncoding],
                        CSOUND_CONTROL_CHANNEL | CSOUND_INPUT_CHANNEL);
    
    *value = (float) sld.value;
}

- (IBAction)setNoiseAmplitude:(id)sender {

    UISlider* sld = sender;
    
    NSLog(@"%s -> NOISE AMP = %f", __FUNCTION__, sld.value);
    
    if (!_cs || !running) return;
    
    NSString* channelName = @"noise_amp";
    float *value;
    csoundGetChannelPtr(_cs, &value, [channelName cStringUsingEncoding:NSASCIIStringEncoding],
                        CSOUND_CONTROL_CHANNEL | CSOUND_INPUT_CHANNEL);
    
    *value = (float) sld.value;
}
@end
