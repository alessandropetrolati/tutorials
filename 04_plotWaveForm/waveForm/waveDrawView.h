//
//  waveDrawView.h
//  cs4dev_tutorial
//
//  Created by Alessandro Petrolati on 13/07/15.
//  Copyright 2015 Prof. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AudioDSP.h"

@class waveLoopPointsView;

@interface waveDrawView : UIView {
    
    IBOutlet waveLoopPointsView *waveZoomLoopoint;
    
    UIColor* _currentColor;
    CGFloat _scrubOld;
    UIView *_scrubSelectionLayer;
    CGRect highlight;
    CGFloat _start_X_Location;
    
    CGPoint* points;
    int pointSize;
    
    // From Csound
    MYFLT* channelPtr_sync;
    MYFLT* channelPtr_start;
    MYFLT* channelPtr_end;
    MYFLT* channelPtr_file_position;
    
    MYFLT *table;
    long tableLength;
}
@property(nonatomic, readwrite) CGFloat scrub;

-(void)drawWaveFromCsoundGen:(CSOUND*)cs genNumber:(int)num;
-(void)setupCsound:(CSOUND*)cs;
-(void)clearView;

-(float)getScrubPosition;
-(void)restartScrubAtBeginning;
-(void)restartScrubAtPosition:(float)pos;

-(void)calculateWaveInPoints;
-(void)flashLayer:(id)sender;
-(void)changeColor:(UIColor*)color;
-(UIColor*)getCurrentColor;
-(void)updateGraphicallyScrubPosition:(float)val;
-(void)setScrubColor:(UIColor*)color;
-(void)updateScrubPosition:(CGFloat)ph;
@end

