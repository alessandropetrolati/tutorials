//
//  waveLoopPointsView.m
//  cs4dev_tutorial
//
//  Created by Alessandro Petrolati on 13/07/15.
//  Copyright 2015 Prof. All rights reserved.
//

#import "waveLoopPointsView.h"
#import "waveDrawView.h"
#import <QuartzCore/QuartzCore.h>

@implementation waveLoopPointsView
@synthesize waveView;
@synthesize channel;

- (void)awakeFromNib {
    [super awakeFromNib];
    
    myScrollView.minimumZoomScale = 1;
    myScrollView.maximumZoomScale = MAXIMUM_ZOOM_SCALE;
    myScrollView.delegate = self;
    myScrollView.bounces = NO;
    
    myScrollView.alwaysBounceVertical = NO;
    [myScrollView setShowsVerticalScrollIndicator:NO];
    
    [myScrollView setContentSize:CGSizeMake(self.frame.size.width, self.frame.size.height)];

    UILongPressGestureRecognizer * tapReconizer = [[UILongPressGestureRecognizer alloc]
                                                   initWithTarget:self action:@selector(fullZoomOut)];
    tapReconizer.delaysTouchesBegan = NO;
    tapReconizer.delaysTouchesEnded = NO;
    
    [self addGestureRecognizer:tapReconizer];
    
    [self setUserInteractionEnabled:YES];
    [self setMultipleTouchEnabled:YES];

    [self enableZoom:YES];
}

- (void)enableZoom:(BOOL)zoom {

    [myScrollView setScrollEnabled:zoom];
//    [waveView setHidden:!zoom];
//    [waveView setUserInteractionEnabled:!zoom];
}

- (void)fullZoomOut {
    
    [waveView flashLayer:nil];
    
    CGPoint newContentOffset = CGPointMake(0, 0);
    CGSize newContentSize = CGSizeMake(waveView.frame.size.width, waveView.frame.size.height);
    
    //don't call our own set..zoomScale, cause they eventually call this method.//Infinite recursion is uncool.
    [myScrollView setMinimumZoomScale:1.0];
    [myScrollView setMaximumZoomScale:1.0];
    [myScrollView setZoomScale:1.0 animated:NO];
    
    [self setFrame:CGRectMake(0, 0, newContentSize.width, newContentSize.height)];
    
    [myScrollView setContentSize:newContentSize];
    [myScrollView setContentOffset:newContentOffset animated:NO];
    
    [myScrollView setMinimumZoomScale:1.];
    [myScrollView setMaximumZoomScale:MAXIMUM_ZOOM_SCALE];
    
    // throw out all tiles so they'll reload at the new resolution
    [waveView performSelectorOnMainThread:@selector(setNeedsDisplay) withObject:nil waitUntilDone:NO];
    
    [self saveZoomAndStart];
}

- (void)setZoom:(float)zoom from: (float)start size:(float)size {
    
    /* reset full zoomOut START */
    CGPoint newContentOffset = CGPointMake(0, 0);
    CGSize newContentSize = CGSizeMake(waveView.frame.size.width, waveView.frame.size.height);
    
    //don't call our own set..zoomScale, cause they eventually call this method.//Infinite recursion is uncool.
    [myScrollView setMinimumZoomScale:1.0];
    [myScrollView setMaximumZoomScale:1.0];
    [myScrollView setZoomScale:1.0 animated:NO];
    
    [self setFrame:CGRectMake(0, 0, newContentSize.width, newContentSize.height)];
    
    [myScrollView setContentSize:newContentSize];
    [myScrollView setContentOffset:newContentOffset animated:NO];
    
    [myScrollView setMinimumZoomScale:1.];
    [myScrollView setMaximumZoomScale:MAXIMUM_ZOOM_SCALE];
    /* reset full zoomOut END */
    
    /* set new start and offset */
    hStart = start;
    hZoom = zoom;
    hSize = size;
    
    newContentOffset = CGPointMake(hStart, 0);
    newContentSize = CGSizeMake(hSize, 0);
    
    [myScrollView setZoomScale:hZoom animated:NO];
    [myScrollView setContentOffset:newContentOffset animated:NO];
    
    [waveView setNeedsDisplay];  //calls setNeedsDisplay, among other things forhousekeeping
    
    [self saveZoomAndStart];
}

-(UIScrollView*)getScrollView {

    return myScrollView;
}

#pragma mark- UIScrollView delegate methods

// Return self to zooming
- (UIView*)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return self;
}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView {
    
    [myScrollView setContentOffset:scrollView.contentOffset animated:YES];
    [self saveZoomAndStart];
    [waveView setNeedsDisplay];
}

- (void)scrollViewDidScroll:(UIScrollView *)inscrollView
{
    [waveView setNeedsDisplay];
}

- (void)setNeedsDisplay {
    
    [waveView setNeedsDisplay];
}

#pragma mark -
#pragma private methods
- (void)saveZoomAndStart {
    
    hZoom = [myScrollView zoomScale];
    hStart = [myScrollView contentOffset].x;
    hSize = [myScrollView contentSize].width;
}


#pragma mark - Observed Methods

-(void)saveAsDefault:(NSNotification *) notification {
    
    if (channel) {
        
        NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
        [preferences setObject:[self getState] forKey:[NSString stringWithFormat:@"%@_STATE_", channel]];
    }
}

-(void)loadAsDefault:(NSNotification *) notification {
    
    if (channel) {
        
        NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
        NSDictionary *retrieved = [preferences dictionaryForKey:[NSString stringWithFormat:@"%@_STATE_", channel]];
        
        if (retrieved) {
            NSMutableDictionary* array = [NSMutableDictionary dictionaryWithDictionary:retrieved];
            
            if(array)
                [self restoreState:array];
            
        }
    }
}

#pragma mark save and restore loop points state

-(NSMutableDictionary*)getState {
    
    NSMutableDictionary* connectionState = [[NSMutableDictionary alloc] init];
    
    /* SAMPLER */
    [connectionState setObject:[NSNumber numberWithFloat:hZoom] forKey:[NSString stringWithFormat:@"%@_ZOOM_", @"sampled"]];
    [connectionState setObject:[NSNumber numberWithFloat:hStart/**wRatio*/] forKey:[NSString stringWithFormat:@"%@_START_", @"sampled"]];
    [connectionState setObject:[NSNumber numberWithFloat:hSize] forKey:[NSString stringWithFormat:@"%@_ZOOMSIZE_", @"sampled"]];
    [connectionState setObject:[NSNumber numberWithFloat:waveView.scrub] forKey:[NSString stringWithFormat:@"%@_SCRUBPOSITION_", @"sampled"]];
    
    return connectionState;
}

-(void)restoreState:(NSMutableDictionary*)keyState {
    
    NSNumber *val;
    
    /* SAMPLER */
    // zoom
    [self zooming:keyState];
    
    // scrub
    if((val = [keyState objectForKey:[NSString stringWithFormat:@"%@_SCRUBPOSITION_", @"sampled"]])) {
        waveView.scrub = [val floatValue];

        dispatch_async(dispatch_get_main_queue(), ^{
            [self->waveView updateGraphicallyScrubPosition:val.floatValue];
        });
    }
}

-(void)zooming:(NSMutableDictionary*) preferences {
    
    /* zoom */
    if (channel)
    {
        NSNumber *zoom, *start, *zoomsize;
        
        if((zoom = [preferences objectForKey:[NSString stringWithFormat:@"%@_ZOOM_", @"sampled"]])
           && (start = [preferences objectForKey:[NSString stringWithFormat:@"%@_START_", @"sampled"]])
           && (zoomsize = [preferences objectForKey:[NSString stringWithFormat:@"%@_ZOOMSIZE_", @"sampled"]])) {
            
            [self setZoom:[zoom floatValue] from:[start floatValue] /*/ wRatio*/ size:[zoomsize floatValue] ];
        }
    }
}
@end
