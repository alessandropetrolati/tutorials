//
//  waveLoopPointsView.h
//  cs4dev_tutorial
//
//  Created by Alessandro Petrolati on 13/07/15.
//  Copyright 2015 Prof. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "AudioDSP.h"

#define MAXIMUM_ZOOM_SCALE 99999

@class waveDrawView;

@interface waveLoopPointsView : UIView <UIScrollViewDelegate, UIGestureRecognizerDelegate> {
    
    IBOutlet UIScrollView *myScrollView;
    
    CGFloat hStart;
    CGFloat hZoom;
    CGFloat hSize;
}
//User Defined Runtime Attributes
@property(nonatomic, readonly) NSString* channel;

@property(nonatomic, strong) IBOutlet waveDrawView* waveView;

-(void)enableZoom:(BOOL)zoom;
-(void)fullZoomOut;
-(void)setZoom:(float)zoom from: (float)start size:(float)size;
-(UIScrollView*)getScrollView;
-(NSMutableDictionary*)getState;
-(void)restoreState:(NSMutableDictionary*)keyState;
@end

@interface waveLoopPointsView ()
-(void)saveZoomAndStart;
@end