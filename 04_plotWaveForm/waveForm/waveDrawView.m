//
//  waveDrawView.m
//  cs4dev_tutorial
//
//  Created by Alessandro Petrolati on 13/07/15.
//  Copyright 2015 Prof. All rights reserved.
//

#import "waveDrawView.h"
#import "QuartzCore/CALayer.h"
#import "waveLoopPointsView.h"

#define CONTENT_SCALE_FACTOR 0.8

@implementation waveDrawView
@synthesize scrub = _scrub;

-(void)layoutSubviews
{
    [super layoutSubviews];
    self.contentScaleFactor = CONTENT_SCALE_FACTOR;
    self.layer.cornerRadius = 10;
    self.layer.masksToBounds = YES;
}

-(void)awakeFromNib {
    
    [super awakeFromNib];
    
    _currentColor = [UIColor whiteColor];
    
    highlight.size.width = 2;
    highlight.size.height = self.bounds.size.height;
    highlight.origin.y = 0;
    
    UITapGestureRecognizer* singleTapRec = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTap:)];
    singleTapRec.numberOfTapsRequired = 1;
    singleTapRec.numberOfTouchesRequired = 1;
    singleTapRec.delaysTouchesBegan = NO;
    singleTapRec.delaysTouchesEnded = NO;
    
    [waveZoomLoopoint addGestureRecognizer:singleTapRec];
    
    [self setUserInteractionEnabled:YES];
    [self setMultipleTouchEnabled:YES];
    
    /* initialize scrub layer selector */
    _scrubSelectionLayer = [[UIView alloc] init];
    [_scrubSelectionLayer setMultipleTouchEnabled:NO];
    
    _scrubSelectionLayer.backgroundColor = [UIColor redColor];
        
    /* add scrub selector */
    [self addSubview:_scrubSelectionLayer];
}

-(void)flashLayer:(id)sender {
    //add an explicit animation to our layer
    CABasicAnimation* theAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    theAnimation.duration = 0.15;
    theAnimation.repeatCount = 2;
    theAnimation.autoreverses=YES;
    theAnimation.fromValue=[NSNumber numberWithFloat:0.0];
    theAnimation.toValue=[NSNumber numberWithFloat:1.0];
    [self.layer addAnimation:theAnimation forKey:@"animateOpacity"];
}

-(void)setNeedsDisplay {
    
    [self performSelectorOnMainThread:@selector(calculateWaveInPoints) withObject:nil waitUntilDone:NO];
    
    [super setNeedsDisplay];
    
    highlight.size.height = self.bounds.size.height;
}

-(void)calculateWaveInPoints {
    
    if(![self isBufferOk])
        return;
    
    CGFloat width = self.frame.size.width;
    CGFloat halfHeight = self.frame.size.height / 2.0;
    
    if (width != pointSize) {
        
        if (points)
            free(points);
        
        pointSize = width * 2;
        points = (CGPoint*) malloc(pointSize * sizeof(CGPoint));
    }
    
    /* Loop Points state */
    CGFloat _start = [[waveZoomLoopoint getScrollView] contentOffset].x / [[waveZoomLoopoint getScrollView] contentSize].width;
    CGFloat _hZoom = ([[waveZoomLoopoint getScrollView] maximumZoomScale] / [[waveZoomLoopoint getScrollView] zoomScale]) / MAXIMUM_ZOOM_SCALE;
    
    if (_start < 0.0)
        _start = 0.0;
    
//    CGFloat range = (_start + _hZoom);
//    if (range > 1.0)
//        range = 1.0;
    
    if (channelPtr_start && channelPtr_end) {
        *channelPtr_start =_start;
        *channelPtr_end = (_start + _hZoom);
    }
    
    /* -------------------------------- */
    long bufferLength = tableLength;
    CGFloat samplesPerPixel = bufferLength / waveZoomLoopoint.frame.size.width;
    samplesPerPixel = samplesPerPixel < 1.0 ? 1.0 : samplesPerPixel;
    CGFloat sampleStartAudio = bufferLength * _start;
    CGFloat gap = 1.0;
    
    /* Fixed Reduce Resolution, for better performance */
    gap = (0.05 * (samplesPerPixel-1)) + 1;
    
    // Very important for CPU optimization
    self.contentScaleFactor = CONTENT_SCALE_FACTOR;
    
    unsigned long i = 0;
    unsigned long start;
    unsigned long ndx;
    MYFLT sample = 0.0;
    CGFloat min, max, _max;
    CGFloat _min;
    
    float* audioBuffer = table;
    
    for (unsigned long x = 0; x < width; ++x)
    {
        ++i;
        start = i * samplesPerPixel;
        min = max = 0;
        
        for(unsigned long j = start; j < start + samplesPerPixel; j+= gap) {
            
            ndx = (sampleStartAudio + j);
            ndx = ndx < bufferLength ? ndx : (ndx - bufferLength);
            
            if (ndx < tableLength)
                sample = (MYFLT) audioBuffer[ndx];
            
            if (sample > 0.0) {
                if (sample > max) max = sample;
                
            }
            else {
                if (sample < min) min = sample;
            }
        }
        
        _max = halfHeight + (max * halfHeight);
        _min = halfHeight + (min * halfHeight);
        
        unsigned long x_ndx = x * 2;
        points[x_ndx].x = x;
        points[x_ndx].y = _max;
        points[x_ndx+1].x = x;
        points[x_ndx+1].y = _min;
    }
    
    /* Clear remaining points when Zoom > 1 */
    for (unsigned long x = width /*/ resc*/; x < width; ++x) {
        unsigned long x_ndx = x * 2;
        points[x_ndx].x = x;
        points[x_ndx].y = halfHeight;
        points[x_ndx+1].x = x;
        points[x_ndx+1].y = halfHeight;
    }
}

-(void)drawRect:(CGRect)rect {
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    // Draw line must be to 1
    CGContextSetLineWidth(ctx, 1);
    CGContextSetStrokeColorWithColor(ctx, _currentColor.CGColor);
    CGContextBeginPath(ctx);
    CGContextAddLines(ctx, (CGPoint*)points, self.frame.size.width * 2);
    CGContextStrokePath(ctx);
}

-(void)clearView {
    
    CGFloat width = self.frame.size.width;
    CGFloat halfHeight = self.frame.size.height / 2.0;
    
    if (width != pointSize) {
        
        if (points)
            free(points);
        
        pointSize = width * 2;
        points = (CGPoint*) malloc(pointSize * sizeof(CGPoint));
    }
    
    /* Clear remaining points when Zoom > 1 */
    for (unsigned long x = 0; x < width; ++x) {
        unsigned long x_ndx = x * 2;
        points[x_ndx].x = x;
        points[x_ndx].y = halfHeight;
        points[x_ndx+1].x = x;
        points[x_ndx+1].y = halfHeight;
    }
    
    [self setNeedsDisplay];
}

- (void)changeColor:(UIColor*)color {
    
    _currentColor = color;
}

- (UIColor*)getCurrentColor {
    return _currentColor;
}

-(float)getScrubPosition {
    
    return _scrub;
}

-(void)singleTap:(UITapGestureRecognizer *)sender {
    
    _scrub = _scrubOld + ( (([sender locationInView:self].x - _start_X_Location) / self.frame.size.width) );
    _scrub = _scrub < 1.0 ? _scrub : 1.0;
    _scrub = _scrub > 0.0 ? _scrub : 0.0;
    
    [self updateGraphicallyScrubPosition:_scrub];
    
    /* Update Csound Phasor */
    if(channelPtr_sync)
        *channelPtr_sync = _scrub;
}

-(void)setScrubColor:(UIColor*)color {
    
    _scrubSelectionLayer.backgroundColor = color;
}

#pragma mark -
#pragma mark Drawing

- (void)updateScrubPosition:(CGFloat)ph {
    _scrub = ph;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self updateGraphicallyScrubPosition:ph];
    });
}

-(void)updateGraphicallyScrubPosition:(float) val {
    
    highlight.origin.x  = val * self.bounds.size.width; /* from granulator to GUI */
    
    /* copy highlight rect in the layer frame animation */
    [CATransaction setAnimationDuration:.1];
    _scrubSelectionLayer.frame = highlight;
    
    /* Update Csound Phasor */
    if(channelPtr_sync)
        *channelPtr_sync = _scrub;
}

-(void)restartScrubAtBeginning {
    
    double dither = (((double)arc4random() / 0x100000000)) * 0.000001;
    _scrub = dither;
    
    [self performSelectorOnMainThread:@selector(updateGraphicallyScrubPosition:)
                               withObject:[NSNumber numberWithFloat:dither] waitUntilDone:NO];
    
}

-(void)restartScrubAtPosition:(float)pos {
    
    double dither = (((double)arc4random() / 0x100000000)) * 0.000001;
    _scrub = pos+dither;
    
    [self performSelectorOnMainThread:@selector(updateGraphicallyScrubPosition:)
                               withObject:[NSNumber numberWithFloat:pos+dither] waitUntilDone:NO];
    
}

#pragma mark - Get Values From Csound

- (void)setupCsound:(CSOUND*)cs
{
    if (cs) {
        csoundGetChannelPtr(cs, &channelPtr_sync, [@"file_sync" cStringUsingEncoding:NSASCIIStringEncoding],
                            CSOUND_CONTROL_CHANNEL | CSOUND_INPUT_CHANNEL);
        
        csoundGetChannelPtr(cs, &channelPtr_start, [@"file_start" cStringUsingEncoding:NSASCIIStringEncoding],
                            CSOUND_CONTROL_CHANNEL | CSOUND_INPUT_CHANNEL);
        
        csoundGetChannelPtr(cs, &channelPtr_end, [@"file_end" cStringUsingEncoding:NSASCIIStringEncoding],
                            CSOUND_CONTROL_CHANNEL | CSOUND_INPUT_CHANNEL);
        
        csoundGetChannelPtr(cs, &channelPtr_file_position, [@"file_position_from_csound" cStringUsingEncoding:NSASCIIStringEncoding],
                            CSOUND_CONTROL_CHANNEL | CSOUND_INPUT_CHANNEL);
        
        
        *channelPtr_sync = 1.0;
        *channelPtr_start = 0.0;
        *channelPtr_end = 1.0;
        *channelPtr_file_position = 0.0;
    }
}

- (void)drawWaveFromCsoundGen:(CSOUND*)cs genNumber:(int)num
{
    if (cs) {
        
        tableLength = csoundGetTable(cs, &table, num);
        
        if (tableLength > 0 && table) {
            [self performSelectorOnMainThread:@selector(setNeedsDisplay) withObject:nil waitUntilDone:NO];
        }
    }
}

-(BOOL)isBufferOk {
    return tableLength > 0 && table;
}
@end
