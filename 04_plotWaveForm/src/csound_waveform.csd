/*
cs4dev tutorial 04
by Alessandro Petrolati
www.apesoft.it
*/
<CsoundSynthesizer>
<CsOptions>

-o dac
-+rtmidi=null
-+rtaudio=null
-d
-+msg_color=0
--expression-opt
-M0
-m0
-i adc

</CsOptions>
<CsInstruments>


sr = 44100
ksmps = 64
nchnls = 2
0dbfs = 1

instr 1 ; soundfile phase and jittering

;--------------------------------------------------------------------------------------------------------------------------------
ifun = p4 ;current audio function

ifilen = ftlen(ifun)
ifilen = ifilen / sr

ifilsr = ftsr(ifun)
irescale = ifilsr / sr


/* parameters */
kspeed chnget "file_speed"		; 1 = original speed 
kjitter chnget "file_jitter"	; time position randomness (offset) of the pointer in ms
ksync_phase chnget "file_sync"	; waveform update values
kstart chnget "file_start"		; waveform start zoom
kend chnget "file_end"			; waveform end zoom

krange_scan = abs(kend - kstart)

ktrig changed ksync_phase
if ktrig = 1 then
	reinit set_phase
	endif

set_phase:	
kspeed /= krange_scan
kspeed *= irescale

/* time pointer and jittering */
kfilposphas	 phasor (kspeed / ifilen), i(ksync_phase)

rireturn

kposrandsec	= kjitter / 2000			; ms -> sec n.b. /2000 becouse it's bipolar
kposrand = kposrandsec / ifilen		; phase values (0-1)z
krndpos = birnd(kposrand)			; random offset in phase values

kfilposphas += krndpos

	chnset kfilposphas, "file_position_from_csound"

kfilposphas *= krange_scan
kfilposphas += kstart

ksamplepos wrap kfilposphas, kstart, kend

kresample = irescale * 1	;do not change pitch
ibeg = i(kstart)			;start at beginning
iwsize = 4410			;window size in samples with
irandw = 882			;bandwidth of a random number generator
itimemode = 1			;ktimewarp is "time" pointer
ioverlap = 4

ktimewarp  = abs(ksamplepos * ifilen) ;calculate phase in seconds

asig sndwarp 1, ktimewarp, kresample, ifun, ibeg, iwsize, irandw, ioverlap, 10, itimemode
    
     outs asig, asig

endin

</CsInstruments>
<CsScore>
f1 0 4096 10 1	;sine wave
f2 0 0 1 "TimeAgo.wav" 0 0 1
f3 0 0 1 "Density_Sample08.wav" 0 0 1
f10 0 1024 9 0.5 1 0

f0 999999999

</CsScore>
</CsoundSynthesizer>
