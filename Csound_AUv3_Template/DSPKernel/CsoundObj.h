/*
 
 CsoundObj.h:
 
 Copyright (C) 2011 Steven Yi, Victor Lazzarini
 
 This file is part of Csound for iOS.
 
 The Csound for iOS Library is free software; you can redistribute it
 and/or modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 Csound is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with Csound; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
 02111-1307 USA
 
 */

#import <UIKit/UIKit.h>
#import <AudioToolbox/ExtendedAudioFile.h>
#import <AudioToolbox/AudioConverter.h>
#import <AudioToolbox/AudioServices.h>
#import <AudioUnit/AudioUnit.h>
#import <AVFoundation/AVFoundation.h>
#import <Foundation/Foundation.h>
#import "csound.h"
#import "csdl.h"

#define USE_CALLBACK_CHANNEL

@class AudioDSP;
@class VCS3_LFO_WIDGETS_MAPPING;
@class APE_SAMPLER;
@class APESeq;
@class VCS3_Voltometer;
@class EnvelopeLampFromCsound;
@class APE_SYNC;
@class APE_METERS_VERT;
@class UISwitch;
@class UISlider;
@class UIButton;

/* Csound external Opcode Implementation */
#define ENVELOPE_MANUAL_THRESHOLD 4.98

typedef struct csdata_ {
	CSOUND *cs;
	long bufframes;
	int ret;
	int nchnls;
    float sr;
    bool running;
	bool shouldRecord;
	bool shouldMute;
	ExtAudioFileRef file;
    AudioUnit csAUHAL;

    OSStatus err;
    BOOL inForeground;
    BOOL interAppConnected;
    
#ifndef USE_VECTOR
    __unsafe_unretained NSMutableArray* valuesCache;
#endif
    __unsafe_unretained AudioDSP* audioDSP;
    __unsafe_unretained VCS3_LFO_WIDGETS_MAPPING* lfoWidgetsManager;
    __unsafe_unretained APE_SAMPLER* sampler_wave_1;
    __unsafe_unretained APE_SAMPLER* sampler_wave_2;
    __unsafe_unretained APESeq* sequencer;
    __unsafe_unretained VCS3_Voltometer *voltometer;
    __unsafe_unretained EnvelopeLampFromCsound* envelopeLamp;
    __unsafe_unretained APE_SYNC* syncRefCon;
    __unsafe_unretained APE_METERS_VERT* levelMeterInput;
    __unsafe_unretained APE_METERS_VERT* levelMeterOutput;
    
    int counter;
//    BOOL ext_input_selector_ch1_isSampler;
//    BOOL ext_input_selector_ch2_isSampler;

} csdata;

typedef struct {
	CSOUND *cs;
	int attr;
	const char *format;
	va_list valist;
} Message;

@class CsoundObj;
@protocol CsoundValueCacheable;

@protocol CsoundObjCompletionListener

-(void)csoundObjDidStart:(CsoundObj*)csoundObj;
-(void)csoundObjComplete:(CsoundObj*)csoundObj;

@end

@interface CsoundObj : NSObject
{
#ifndef USE_VECTOR
    NSMutableArray* valuesCache;
#endif

    NSMutableArray* completionListeners;
    csdata mCsData;
    BOOL mMidiInEnabled;
	NSURL *outputURL;
	SEL mMessageCallback;
	id  mMessageListener;
    
	HostCallbackInfo *callBackInfo;
}
#pragma mark Inter-App Audio Properties
@property (strong, nonatomic) UIImage *audioUnitIcon;
@property (nonatomic) bool playing;
@property (nonatomic) bool recording;
@property (nonatomic) bool connected;
@property (nonatomic) Float64 playTime;
NSString *formattedTimeStringForFrameCount(UInt64 inFrameCount, Float64 inSampleRate, BOOL inShowMilliseconds);

@property (assign) SEL mMessageCallback;
@property (assign) BOOL midiInEnabled;

-(AudioUnit*)getAudioUnit;


#pragma mark UI and Hardware Methods

-(id<CsoundValueCacheable>)addSwitch:(UISwitch*)uiSwitch forChannelName:(NSString*)channelName;
-(id<CsoundValueCacheable>)addSlider:(UISlider*)uiSlider forChannelName:(NSString*)channelName;
-(id<CsoundValueCacheable>)addButton:(UIButton*)uiButton forChannelName:(NSString*)channelName;

-(void)addValueCacheable:(id<CsoundValueCacheable>)valueCacheable;
-(void)removeValueCaheable:(id<CsoundValueCacheable>)valueCacheable;

#pragma mark -

-(void)sendScore:(NSString*)score;

#pragma mark -

-(void)addCompletionListener:(id<CsoundObjCompletionListener>)listener;

#pragma mark -

-(void)startCsound:(NSString*)csdFilePath;
-(void)recordToURL:(NSURL *)outputURL;
-(void)stopRecording;
-(void)stopCsound;
-(void)muteCsound;
-(void)unmuteCsound;

-(CSOUND*)getCsound;

/** get a float* output channel that maps to a channel name and type, where type is
 CSOUND_AUDIO_CHANNEL, CSOUND_CONTROL_CHANNEL, etc. */
-(float*)getInputChannelPtr:(NSString*)channelName channelType:(controlChannelType)channelType;

/** get a float* output channel that maps to a channel name and type, where type is
 CSOUND_AUDIO_CHANNEL, CSOUND_CONTROL_CHANNEL, etc. */
-(float*)getOutputChannelPtr:(NSString*)channelName channelType:(controlChannelType)channelType;
-(NSData*)getOutSamples;

-(int)getNumChannels;
-(int)getKsmps;

//-(void)setEnableSampler_ch1:(BOOL)state;
//-(void)setEnableSampler_ch2:(BOOL)state;

-(void)setMessageCallback:(SEL)method withListener:(id)listener;
-(void)performMessageCallback:(NSValue *)infoObj;

+ (AudioStreamBasicDescription)nonInterleavedFloatStereoAudioDescription;

#pragma mark Inter-App Audio
+ (void) setAudioSessionActive;
+ (void) setAudioSessionInActive;

@end

