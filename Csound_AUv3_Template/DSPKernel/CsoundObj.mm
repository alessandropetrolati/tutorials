/*
 
 CsoundObj.m:
 
 Copyright (C) 2011 Steven Yi, Victor Lazzarini
 
 This file is part of Csound for iOS.
 
 The Csound for iOS Library is free software; you can redistribute it
 and/or modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 Csound is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with Csound; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
 02111-1307 USA
 
 */

#import "CsoundObj.h"
#import "CachedSlider.h"
#import "CachedButton.h"
#import "CachedSwitch.h"
#import "CachedAccelerometer.h"
#import "CachedGyroscope.h"
#import "CachedAttitude.h"
#import "CsoundValueCacheable.h"
//#import "CsoundMIDI.h"

OSStatus  Csound_Perform(void *inRefCon,
                         AudioUnitRenderActionFlags *ioActionFlags,
                         const AudioTimeStamp *inTimeStamp,
                         UInt32 dump,
                         UInt32 inNumberFrames,
                         AudioBufferList *ioData
                         );

OSStatus  Csound_Perform_DOWNSAMP(void *inRefCon,
                                  AudioUnitRenderActionFlags *ioActionFlags,
                                  const AudioTimeStamp *inTimeStamp,
                                  UInt32 dump,
                                  UInt32 inNumberFrames,
                                  AudioBufferList *ioData
                                  );

@interface CsoundObj()

-(void)runCsound:(NSString*)csdFilePath;

@end

@implementation CsoundObj

@synthesize midiInEnabled = mMidiInEnabled;
@synthesize mMessageCallback = _mMessageCallback;

#pragma mark Properties
@synthesize audioUnitIcon = _audioUnitIcon;

#pragma mark CAUITransportEngine Protocol- Required properties
@synthesize playing   = _playing;
@synthesize recording = _recording;
@synthesize connected = _connected;
@synthesize playTime  = _playTime;


-(id)init
{
    self = [super init];
    if (self) {

        CSOUND *cs = mCsData.cs;
        cs = csoundCreate(NULL);
        csoundSetHostImplementedAudioIO(cs, 1, 0);
        csoundSetMessageCallback(cs, messageCallback);
        csoundSetHostData(cs, (__bridge void *)(self));
        mCsData.cs = cs;
        
        valuesCache = [[NSMutableArray alloc] init];
        completionListeners = [[NSMutableArray alloc] init];
		
        mCsData.shouldMute = false;
        //        mCsData.interAppConnected = NO;
        mMidiInEnabled = NO;
        
        self.connected = NO;
        self.playing   = NO;
        self.recording = NO;
        
        [self initializeAudio];
    }
    
    return self;
}

-(void)initializeAudio {
    
    /* Audio Session handler */
    AVAudioSession* session = [AVAudioSession sharedInstance];
    
    NSError* error = nil;
    BOOL success = NO;
    
    success = [session setCategory:AVAudioSessionCategoryPlayAndRecord
                       withOptions:(AVAudioSessionCategoryOptionMixWithOthers |
                                    AVAudioSessionCategoryOptionDefaultToSpeaker
#ifdef ENABLE_AUDIO_BLUETOOTH
     | AVAudioSessionCategoryOptionAllowBluetooth
#endif
                                    )
                             error:&error];
    
    
    if (!success) {
        NSLog(@"%@ Error setting category: %@",
              NSStringFromSelector(_cmd), [error localizedDescription]);
    }
    
    
    success = [session setActive:YES error:&error];
    
    if (!success) {
        NSLog(@"%@", [error localizedDescription]);
    }
    
    /* Sets Interruption Listner */
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(InterruptionListener:)
                                                 name:AVAudioSessionInterruptionNotification
                                               object:session];

    /* Publish Inter-App when app is off */
    AudioComponentDescription defaultOutputDescription;
    defaultOutputDescription.componentType = kAudioUnitType_Output;
    defaultOutputDescription.componentSubType = kAudioUnitSubType_RemoteIO;
    defaultOutputDescription.componentManufacturer = kAudioUnitManufacturer_Apple;
    defaultOutputDescription.componentFlags = 0;
    defaultOutputDescription.componentFlagsMask = 0;
    
    // Get the default playback output unit
    AudioComponent HALOutput = AudioComponentFindNext(NULL, &defaultOutputDescription);
    NSAssert(HALOutput, @"Can't find default output");
    
    // Create a new unit based on this that we'll use for output
    mCsData.err = AudioComponentInstanceNew(HALOutput, &mCsData.csAUHAL);
    if (mCsData.err)
    {
        fprintf(stderr,"Audio Component New Instance error\n");
    }
    
    /*
     | i                   o |
     -- BUS 1 -- from mic --> | n    REMOTE I/O     u | -- BUS 1 -- to app -->
     | p      AUDIO        t |
     -- BUS 0 -- from app --> | u       UNIT        p | -- BUS 0 -- to speaker -->
     | t                   u |
     |                     t |
     -------------------------
     */
    
    // Enable IO for recording
    UInt32 flag = 1;
    mCsData.err = AudioUnitSetProperty(mCsData.csAUHAL,
                                       kAudioOutputUnitProperty_EnableIO,
                                       kAudioUnitScope_Input,
                                       1,
                                       &flag,
                                       sizeof(flag));
    if (mCsData.err)
    {
        fprintf(stderr,"Enable Input IO error\n");
    }
    
    // Enable IO for playback
    mCsData.err = AudioUnitSetProperty(mCsData.csAUHAL,
                                       kAudioOutputUnitProperty_EnableIO,
                                       kAudioUnitScope_Output,
                                       0,
                                       &flag,
                                       sizeof(flag));
    if (mCsData.err)
    {
        fprintf(stderr,"Enable Output IO error\n");
    }
    
    mCsData.err = AudioUnitInitialize(mCsData.csAUHAL);
    if (mCsData.err)
    {
        fprintf(stderr,"Initialize error\n");
    }
    
    
    // If we're in the background, we've been launched by interapp audio,
    // and we need to hold off on starting until the connected callback comes in.
    
    //    mCsData.err = AudioOutputUnitStart(mCsData.csAUHAL);
    //    if (mCsData.err)
    //    {
    //        fprintf(stderr,"start error\n");
    //    }
    
    
    /* ======================================================================================= */
    /* ======================================================================================= */
    
    [self addAudioUnitPropertyListener];
    [self publishInterAudioApp];
}

-(void)publishInterAudioApp {
    
    /* Alessandro Petrolati Set's Inter-app Audio You need Set's AudioComponent in Plist and entitlements */
    
    AudioComponentDescription desc_instr = { kAudioUnitType_RemoteInstrument,'ivci','iape', 0, 0 };
    OSStatus result = AudioOutputUnitPublish(&desc_instr, (CFStringRef)@"iVCS3 (instr)", 1, mCsData.csAUHAL);
    if (result != noErr)
        NSLog(@"AudioOutputUnitPublish Instrument result: %d", (int)result);
    
    
    AudioComponentDescription desc_fx = { kAudioUnitType_RemoteEffect,'ivcx','xape', 0, 0 };
    result = AudioOutputUnitPublish(&desc_fx, (CFStringRef)@"iVCS3 (fx)", 1, mCsData.csAUHAL);
    if (result != noErr)
        NSLog(@"AudioOutputUnitPublish FX result: %d", (int)result);
    
//    AudioComponentDescription desc_gen = { kAudioUnitType_RemoteGenerator,'ivcs','apes', 0, 0 };
//    result = AudioOutputUnitPublish(&desc_gen, (CFStringRef)@"iVCS3_gen", 1, mCsData.csAUHAL);
//    if (result != noErr)
//        NSLog(@"AudioOutputUnitPublish Generator result: %d", (int)result);

}


-(void) updateTransportInfo {
    
    if (_connected) {
        if (!callBackInfo)
            [self getHostCallBackInfo];
        if (callBackInfo) {
            Boolean isPlaying  = self.playing;
            Boolean isRecording = self.recording;
            Float64 outCurrentSampleInTimeLine = 0;
            void * hostUserData = callBackInfo->hostUserData;
            OSStatus result =  callBackInfo->transportStateProc2( hostUserData,
                                                                 &isPlaying,
                                                                 &isRecording, NULL,
                                                                 &outCurrentSampleInTimeLine,
                                                                 NULL, NULL, NULL);
            if (result == noErr) {
                self.playing = isPlaying;
                self.recording = isRecording;
                self.playTime = outCurrentSampleInTimeLine;
            } else
                NSLog(@"Error occured fetching callBackInfo->transportStateProc2 : %d", (int)result);
        }
    }
}

-(void)addAudioUnitPropertyListener
{
    
    /* APE commented */
    //        AudioUnitRemovePropertyListenerWithUserData(mCsData.csAUHAL, kAudioUnitProperty_IsInterAppConnected, AudioUnitPropertyChangeDispatcher, (__bridge void *)(self));
    //        AudioUnitRemovePropertyListenerWithUserData(mCsData.csAUHAL, kAudioOutputUnitProperty_HostTransportState, AudioUnitPropertyChangeDispatcher, (__bridge void *)(self));
    
    AudioUnitAddPropertyListener(mCsData.csAUHAL,
                                 kAudioUnitProperty_IsInterAppConnected,
                                 AudioUnitPropertyChangeDispatcher,
                                 (__bridge void *)(self));
    AudioUnitAddPropertyListener(mCsData.csAUHAL,
                                 kAudioOutputUnitProperty_HostTransportState,
                                 AudioUnitPropertyChangeDispatcher,
                                 (__bridge void *)(self));
    
    [self setupMidiCallBacks:&mCsData.csAUHAL userData:(__bridge void *)(self)];
}


#pragma mark - AudioBus delegate


void AudioUnitPropertyChangeDispatcher(void *inRefCon, AudioUnit inUnit, AudioUnitPropertyID inID, AudioUnitScope inScope, AudioUnitElement inElement)
{
    //NSLog(@"%s", __FUNCTION__);
    CsoundObj *audio = (__bridge CsoundObj *)inRefCon;
    
    if (inID==kAudioUnitProperty_IsInterAppConnected)
    {
        UInt32 connect;
        UInt32 dataSize = sizeof(UInt32);
        AudioUnitGetProperty(inUnit, kAudioUnitProperty_IsInterAppConnected, kAudioUnitScope_Global, 0, &connect, &dataSize);
        if (connect)
        {
//            NSLog(@"%s -------------------------------------- %@", __FUNCTION__, @"CONNECTED");
            
            audio->_connected = YES;
            audio->mCsData.interAppConnected = YES;
            /* APE commented without removeListner it crash */
            //            [audio addAudioUnitPropertyListener];
            //            [audio updateTransportInfo];
            [CsoundObj setAudioSessionActive];
            AudioOutputUnitStart(audio->mCsData.csAUHAL);
            
            /* VERIFY IT!!!! */
            AudioOutputUnitStart(inUnit);
            
            
        }
        else
        {
//            NSLog(@"%s -------------------------------------- %@", __FUNCTION__, @"UN-CONNECTED");
            audio->_connected = NO;
            audio->mCsData.interAppConnected = NO;
            [CsoundObj setAudioSessionActive];
            /* APE very important here, we don't wont audio stops when disconnecting */
            AudioOutputUnitStart(audio->mCsData.csAUHAL);
            
            [audio updateTransportInfo];
            [audio postUpdateStateNotification];
            
        }
    }
    if (inID==kAudioOutputUnitProperty_HostTransportState)
    {
//        NSLog(@"%s -------------------------------------- %@", __FUNCTION__, @"kAudioOutputUnitProperty_HostTransportState");
        [audio updateTransportInfo];
        [audio postUpdateStateNotification];
    }
}

-(id<CsoundValueCacheable>)addSwitch:(UISwitch*)uiSwitch forChannelName:(NSString*)channelName {
    CachedSwitch* cachedSwitch = [[CachedSwitch alloc] init:uiSwitch
                                                channelName:channelName];
    [valuesCache addObject:cachedSwitch];

    return cachedSwitch;
}

-(id<CsoundValueCacheable>)addSlider:(UISlider*)uiSlider forChannelName:(NSString*)channelName {
    
    CachedSlider* cachedSlider = [[CachedSlider alloc] init:uiSlider
                                                channelName:channelName];

    [valuesCache addObject:cachedSlider];

    return cachedSlider;
}

-(id<CsoundValueCacheable>)addButton:(UIButton*)uiButton forChannelName:(NSString*)channelName {
    CachedButton* cachedButton = [[CachedButton alloc] init:uiButton
                                                channelName:channelName];
    [valuesCache addObject:cachedButton];

    return cachedButton;
}

-(void)addValueCacheable:(id<CsoundValueCacheable>)valueCacheable {
    if (valueCacheable != nil) {

        [valuesCache addObject:valueCacheable];
    }
}

-(void)removeValueCaheable:(id<CsoundValueCacheable>)valueCacheable {

    if (valueCacheable != nil && [valuesCache containsObject:valueCacheable]) {
        [valuesCache removeObject:valueCacheable];
    }
}

#pragma mark -

static void messageCallback(CSOUND *cs, int attr, const char *format, va_list valist)
{
	@autoreleasepool {
		CsoundObj *obj = (__bridge CsoundObj *)(csoundGetHostData(cs));
		Message info;
		info.cs = cs;
		info.attr = attr;
		info.format = format;
        va_copy(info.valist,valist);
		NSValue *infoObj = [NSValue value:&info withObjCType:@encode(Message)];
		[obj performSelector:@selector(performMessageCallback:) withObject:infoObj];
	}
}

- (void)setMessageCallback:(SEL)method withListener:(id)listener
{
	self.mMessageCallback = method;
	mMessageListener = listener;
}

- (void)performMessageCallback:(NSValue *)infoObj
{
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
	[mMessageListener performSelector:_mMessageCallback withObject:infoObj];
#pragma clang diagnostic pop
}

#pragma mark -

-(void)sendScore:(NSString *)score {
    if (mCsData.cs != NULL) {
        csoundInputMessage(mCsData.cs, [score cStringUsingEncoding:NSASCIIStringEncoding]);
    }
}

#pragma mark -

-(void)addCompletionListener:(id<CsoundObjCompletionListener>)listener {
    [completionListeners addObject:listener];
}

-(CSOUND*)getCsound {
    if (!mCsData.running) {
        return NULL;
    }
    return mCsData.cs;
}
-(AudioUnit*)getAudioUnit {
    if (!mCsData.running) {
        return NULL;
    }
    return &mCsData.csAUHAL;
}

-(float*)getInputChannelPtr:(NSString*)channelName channelType:(controlChannelType)channelType
{
    float *value;
    csoundGetChannelPtr(mCsData.cs, &value, [channelName cStringUsingEncoding:NSASCIIStringEncoding],
                        channelType | CSOUND_INPUT_CHANNEL);
    return value;
}

-(float*)getOutputChannelPtr:(NSString *)channelName channelType:(controlChannelType)channelType
{
	float *value;
	csoundGetChannelPtr(mCsData.cs, &value, [channelName cStringUsingEncoding:NSASCIIStringEncoding],
                        channelType | CSOUND_OUTPUT_CHANNEL);
	return value;
}

-(NSData*)getOutSamples {
    if (!mCsData.running) {
        return nil;
    }
    CSOUND* csound = [self getCsound];
    float* spout = csoundGetSpout(csound);
    int nchnls = csoundGetNchnls(csound);
    int ksmps = csoundGetKsmps(csound);
    NSData* data = [NSData dataWithBytes:spout length:(nchnls * ksmps * sizeof(MYFLT))];
    return data;
}

-(int)getNumChannels {
    if (!mCsData.running) {
        return -1;
    }
    return csoundGetNchnls(mCsData.cs);
}
-(int)getKsmps {
    if (!mCsData.running) {
        return -1;
    }
    return csoundGetKsmps(mCsData.cs);
}

//-(void)setEnableSampler_ch1:(BOOL)state {
//
//    mCsData.ext_input_selector_ch1_isSampler = state;
//}
//
//-(void)setEnableSampler_ch2:(BOOL)state {
//    
//    mCsData.ext_input_selector_ch2_isSampler = state;
//}

#pragma mark Csound Callbacks

OSStatus  Csound_Perform(void *inRefCon,
                         AudioUnitRenderActionFlags *ioActionFlags,
                         const AudioTimeStamp *inTimeStamp,
                         UInt32 dump,
                         UInt32 inNumberFrames,
                         AudioBufferList *ioData
                         )
{
    
    csdata *cdata = (csdata *) inRefCon;
    
    AudioUnitRender(cdata->csAUHAL, ioActionFlags, inTimeStamp, 1, inNumberFrames, ioData);
    
    
//    Float32* sample_L = (Float32*) ioData->mBuffers[0].mData;
//    Float32* sample_R = (Float32*) ioData->mBuffers[1].mData;
//    NSLog(@"---------------%f ------------ % f", sample_L[0], sample_R[0]);

    if(cdata->running == false) {
        
        // Clear audio input
        for ( int i = 0; i < ioData->mNumberBuffers; i++ )
            memset(ioData->mBuffers[i].mData, 0, ioData->mBuffers[i].mDataByteSize);

        return noErr;
    }
    
    int ret = cdata->ret, nchnls = cdata->nchnls;
    CSOUND *cs = cdata->cs;
    float slices = inNumberFrames/csoundGetKsmps(cs);
    int ksmps = csoundGetKsmps(cs);
    MYFLT *spin = csoundGetSpin(cs);
    MYFLT *spout = csoundGetSpout(cs);
    MYFLT *buffer;

#ifndef USE_CALLBACK_CHANNEL
    NSMutableArray* cache = cdata->valuesCache;
#endif
    
    /* CSOUND PERFORM */
    if (slices < 1.0) {
        /* inNumberFrames < ksmps */
        Csound_Perform_DOWNSAMP(inRefCon, ioActionFlags, inTimeStamp, dump, inNumberFrames, ioData);
    }
    else
    {
        /* inNumberFrames => ksmps */
        for(int i = 0; i < (int)slices; ++i){

#ifndef USE_CALLBACK_CHANNEL
            for (int i = 0; i < cache.count; ++i) {
                id<CsoundValueCacheable> cachedValue = [cache objectAtIndex:i];
                [cachedValue updateValuesToCsound];
            }
#endif
            /* performance */
            for (int k = 0; k < nchnls; ++k) {
                buffer = (MYFLT *) ioData->mBuffers[k].mData;
                for(int j = 0; j < ksmps; ++j) {
                    spin[j*nchnls+k] = buffer[j+i*ksmps];
                }
            }
            
            if(!ret) {
                ret = csoundPerformKsmps(cs);
            } else {
                cdata->running = false;
            }
            
            for (int k = 0; k < nchnls; ++k) {
                buffer = (MYFLT *) ioData->mBuffers[k].mData;
                if (cdata->shouldMute == false) {
                    for(int j = 0; j < ksmps; ++j) {
                        buffer[j+i*ksmps] = (MYFLT) spout[j*nchnls+k];
                    }
                } else {
                    memset(buffer, 0, sizeof(MYFLT) * inNumberFrames);
                }
            }
            
#ifdef USE_CALLBACK_CHANNEL
            
#else
            for (int i = 0; i < cache.count; ++i) {
                id<CsoundValueCacheable> cachedValue = [cache objectAtIndex:i];
                [cachedValue updateValuesFromCsound];
            }
#endif
        }
        
        cdata->ret = ret;
    }
    

    /* Write to file */
	if (cdata->shouldRecord) {
		OSStatus err = ExtAudioFileWriteAsync(cdata->file, inNumberFrames, ioData);
		if (err != noErr) {
			printf("***Error writing to file: %d\n", (int)err);
		}
	}
    
    return noErr;
}

OSStatus  Csound_Perform_DOWNSAMP(void *inRefCon,
                                  AudioUnitRenderActionFlags *ioActionFlags,
                                  const AudioTimeStamp *inTimeStamp,
                                  UInt32 dump,
                                  UInt32 inNumberFrames,
                                  AudioBufferList *ioData
                                  )
{
    
    csdata *cdata = (csdata *) inRefCon;
    int ret = cdata->ret, nchnls = cdata->nchnls;
    CSOUND *cs = cdata->cs;
    
    MYFLT *spin = csoundGetSpin(cs);
    MYFLT *spout = csoundGetSpout(cs);
    MYFLT *buffer;
    
    NSMutableArray* cache = cdata->valuesCache;
    
    /* DOWNSAMPLING FACTOR */
    int UNSAMPLING = csoundGetKsmps(cs)/inNumberFrames;
    
    if (cdata->counter < UNSAMPLING-1) {
        
        cdata->counter++;
    }
    else {
        
        cdata->counter = 0;
        
        /* UPDATE CSOUND CHANNELS IN */
        for (int i = 0; i < cache.count; i++) {
            id<CsoundValueCacheable> cachedValue = [cache objectAtIndex:i];
            [cachedValue updateValuesToCsound];
        }

        /* CSOUND PROCESS KSMPS */
        if(!cdata->ret) {
            /* PERFORM CSOUND */
            cdata->ret = csoundPerformKsmps(cs);
        } else {
            cdata->running = false;
            
        }
        
        /* UPDATE CSOUND CHANNELS OUT */
        for (int i = 0; i < cache.count; i++) {
            id<CsoundValueCacheable> cachedValue = [cache objectAtIndex:i];
            [cachedValue updateValuesFromCsound];
        }
    }
    
    /* INCREMENTS DOWNSAMPLING COUNTER */
    int slice_downsamp = inNumberFrames * cdata->counter;
    
    /* COPY IN CSOUND SYSTEM SLICE INPUT */
    for (int k = 0; k < nchnls; ++k){
        buffer = (MYFLT *) ioData->mBuffers[k].mData;
        for(int j = 0; j < inNumberFrames; ++j){
            spin[(j+slice_downsamp)*nchnls+k] = buffer[j];
        }
    }
    
    /* COPY OUT CSOUND KSMPS SLICE */
    for (int k = 0; k < nchnls; ++k) {
        buffer = (MYFLT *) ioData->mBuffers[k].mData;
        //        if (cdata->shouldMute == false) {
        for(int j = 0; j < inNumberFrames; ++j) {
            
            buffer[j] = (MYFLT) spout[(j+slice_downsamp)*nchnls+k];
        }
        //        } else {
        //            memset(buffer, 0, sizeof(AudioUnitSampleType) * inNumberFrames);
        //        }
    }
    
    cdata->ret = ret;
    return  noErr;
}

-(void)startCsound:(NSString*)csdFilePath {
    
	mCsData.shouldRecord = false;
    
    [self runCsound:csdFilePath];
    
    //    AudioUnitInitialize(mCsData.csAUHAL);
    AudioOutputUnitStart(mCsData.csAUHAL);
}

-(void)stopCsound {
    mCsData.running = false;
    
//    mCsData.audioDSP = nil;
//    mCsData.sampler_wave_1 = nil;
//    mCsData.sampler_wave_2 = nil;
//    mCsData.lfoWidgetsManager = nil;
//    mCsData.sequencer = nil;
//    mCsData.voltometer = nil;
//    mCsData.envelopeLamp = nil;
    
    
    //    ExtAudioFileDispose(mCsData.file);
    //    mCsData.shouldRecord = false;
    if (mCsData.shouldRecord)
        [self stopRecording];
    
    
    AudioOutputUnitStop(mCsData.csAUHAL);
    //    AudioUnitUninitialize(mCsData.csAUHAL);
    
    
    /* Alessandro Petrolati's code: Dispose AudioUnit */
    //    if (mCsData.csAUHAL) {
    //        AudioComponentInstanceDispose(mCsData.csAUHAL);
    //        mCsData.csAUHAL = nil;
    //    }
    if(mCsData.cs) csoundDestroy(mCsData.cs);
    
    /* CLEANUP VALUE CACHEABLE */
    
    for (int i = 0; i < valuesCache.count; i++) {
        id<CsoundValueCacheable> cachedValue = [valuesCache objectAtIndex:i];
        [cachedValue cleanup];
    }
    
    /* NOTIFY COMPLETION LISTENERS*/
    
    for (id<CsoundObjCompletionListener> listener in completionListeners) {
        [listener csoundObjComplete:self];
    }
}

-(void)recordToURL:(NSURL *)outputURL_
{
    // Define format for the audio file.
    AudioStreamBasicDescription destFormat, clientFormat;
    memset(&destFormat, 0, sizeof(AudioStreamBasicDescription));
    memset(&clientFormat, 0, sizeof(AudioStreamBasicDescription));
    destFormat.mFormatID = kAudioFormatLinearPCM;
    destFormat.mFormatFlags = kLinearPCMFormatFlagIsPacked | kLinearPCMFormatFlagIsSignedInteger;
    destFormat.mSampleRate = csoundGetSr(mCsData.cs);
    destFormat.mChannelsPerFrame = mCsData.nchnls;
    destFormat.mBytesPerPacket = mCsData.nchnls * 2;
    destFormat.mBytesPerFrame = mCsData.nchnls * 2;
    destFormat.mBitsPerChannel = 16;
    destFormat.mFramesPerPacket = 1;
    
    // Create the audio file.
    OSStatus err = noErr;
    CFURLRef fileURL = (__bridge CFURLRef)outputURL_;
    err = ExtAudioFileCreateWithURL(fileURL, kAudioFileWAVEType, &destFormat, NULL, kAudioFileFlags_EraseFile, &(mCsData.file));
    if (err == noErr) {
        // Get the stream format from the AU...
        UInt32 propSize = sizeof(AudioStreamBasicDescription);
        AudioUnitGetProperty((mCsData.csAUHAL), kAudioUnitProperty_StreamFormat, kAudioUnitScope_Input, 0, &clientFormat, &propSize);
        // ...and set it as the client format for the audio file. The file will use this
        // format to perform any necessary conversions when asked to read or write.
        ExtAudioFileSetProperty(mCsData.file, kExtAudioFileProperty_ClientDataFormat, sizeof(clientFormat), &clientFormat);
        // Warm the file up.
        ExtAudioFileWriteAsync(mCsData.file, 0, NULL);
        
        mCsData.shouldRecord = true;
        
    } else {
        printf("***Not recording. Error: %d\n", (int)err);
        //        err = noErr;
        
        mCsData.shouldRecord = false;
    }
}

-(void)stopRecording
{
    mCsData.shouldRecord = false;
    ExtAudioFileDispose(mCsData.file);
}

-(void)muteCsound{
	mCsData.shouldMute = true;
}

-(void)unmuteCsound{
	mCsData.shouldMute = false;
}

-(void)runCsound:(NSString*)csdFilePath {
	
    CSOUND *cs = mCsData.cs;
//    cs = csoundCreate(NULL);
//    csoundSetHostImplementedAudioIO(cs, 1, 0);
//    csoundSetMessageCallback(cs, messageCallback);
//    csoundSetHostData(cs, (__bridge void *)(self));
    
    if (mMidiInEnabled) {
        //        [CsoundMIDI setMidiInCallbacks:cs]; //use CsoundMIDI
//Managed from Midibus
        //        [MIDI_WIDGETS_MAPPING setCsoundMidiInCallbacks:cs]; //use MidiWidgetsManager
    }
    
    // Set's Enviorement Sounnd Files Dir
    NSString* envFlag = @"--env:SFDIR+=";
    NSString *resourcesPath = [[NSBundle mainBundle] resourcePath];
    //    if([[NSFileManager defaultManager] fileExistsAtPath:resourcesPath])
    char* SFDIR = (char*)[[envFlag stringByAppendingString:resourcesPath] cStringUsingEncoding:NSASCIIStringEncoding];
    
    envFlag = @"--env:SADIR+=";
    char* SADIR = (char*)[[envFlag stringByAppendingString:resourcesPath] cStringUsingEncoding:NSASCIIStringEncoding];
    
    char *argv[4] = {(char*)"csound", SFDIR, SADIR, (char*)[csdFilePath cStringUsingEncoding:NSASCIIStringEncoding]};
    int ret = csoundCompile(cs, 4, argv);
    mCsData.running = true;
    
    if(!ret) {
        mCsData.cs = cs;
        mCsData.ret = ret;
        mCsData.nchnls = csoundGetNchnls(cs);
        mCsData.sr  = csoundGetSr(cs);
        mCsData.bufframes = (csoundGetOutputBufferSize(cs))/mCsData.nchnls;
        mCsData.running = true;

        mCsData.valuesCache = valuesCache;
    }
    
    AudioStreamBasicDescription format;
    
    /* SETUP VALUE CACHEABLE */
    for (int i = 0; i < valuesCache.count; i++) {
        id<CsoundValueCacheable> cachedValue = [valuesCache objectAtIndex:i];
        [cachedValue setup:self];
#ifdef USE_CALLBACK_CHANNEL
        [cachedValue updateValuesToCsound];
#endif
    }

    if(!mCsData.err) {

        UInt32 maxFPS;
        UInt32 outsize;
        int elem;
        for(elem = 1; elem >= 0; elem--){
            outsize = sizeof(maxFPS);
            AudioUnitGetProperty(mCsData.csAUHAL, kAudioUnitProperty_MaximumFramesPerSlice, kAudioUnitScope_Global, elem, &maxFPS, &outsize);
            AudioUnitSetProperty(mCsData.csAUHAL, kAudioUnitProperty_MaximumFramesPerSlice, kAudioUnitScope_Global, elem, (UInt32*)&(mCsData.bufframes), sizeof(UInt32));

            outsize = sizeof(AudioStreamBasicDescription);
//            AudioUnitGetProperty(mCsData.csAUHAL, kAudioUnitProperty_StreamFormat, (elem ? kAudioUnitScope_Output : kAudioUnitScope_Input), elem, &format, &outsize);
  
            format.mSampleRate	= csoundGetSr(cs);
            format.mFormatID = kAudioFormatLinearPCM;
            format.mFormatFlags = kAudioFormatFlagIsFloat | kAudioFormatFlagIsPacked | kAudioFormatFlagIsNonInterleaved;
            format.mBytesPerPacket = sizeof(MYFLT);
            format.mFramesPerPacket = 1;
            format.mBytesPerFrame = sizeof(MYFLT);
            format.mChannelsPerFrame = mCsData.nchnls;
            format.mBitsPerChannel = sizeof(MYFLT)*8;
            
//            mCsData.err = AudioUnitSetProperty(mCsData.csAUHAL, kAudioUnitProperty_StreamFormat, kAudioUnitScope_Output, elem, &format, sizeof(AudioStreamBasicDescription));
            mCsData.err = AudioUnitSetProperty(mCsData.csAUHAL, kAudioUnitProperty_StreamFormat, kAudioUnitScope_Input, elem, &format, sizeof(AudioStreamBasicDescription));
        }
        
        if(!mCsData.err) {

            // Set input callback
            AURenderCallbackStruct callbackStruct;
            // Set output callback
            callbackStruct.inputProc = Csound_Perform;
            callbackStruct.inputProcRefCon = &mCsData;
            mCsData.err = AudioUnitSetProperty(mCsData.csAUHAL,
                                               kAudioUnitProperty_SetRenderCallback,
                                               kAudioUnitScope_Global,
                                               0,
                                               &callbackStruct,
                                               sizeof(callbackStruct));
            
            
            /* NOTIFY COMPLETION LISTENERS*/
            for (id<CsoundObjCompletionListener> listener in completionListeners) {
                [listener csoundObjDidStart:self];
            }
        }
    }
}

#pragma mark Memory Handling

-(void)dealloc {
    
    if (callBackInfo)
        free(callBackInfo);
    
    
    AudioComponentInstanceDispose(mCsData.csAUHAL);
    
    // On cleanup...
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:AVAudioSessionInterruptionNotification
                                                  object:nil];

}


#pragma mark - Inter-app Audio
+ (void) setAudioSessionActive {
    
    AVAudioSession *session = [AVAudioSession sharedInstance];
    NSError *err;

//    [session setCategory: AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionMixWithOthers error:  &err];
    
    [session setCategory: AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionMixWithOthers
#ifdef ENABLE_AUDIO_BLUETOOTH
               | AVAudioSessionCategoryOptionAllowBluetooth
#endif
                             error:  &err];

    
    [session setActive: YES error:  &err];
}

+ (void) setAudioSessionInActive {
    
    AVAudioSession *session = [AVAudioSession sharedInstance];
    NSError *err;
    [session setActive: NO error:  &err];
}


-(void) setupMidiCallBacks:(AudioUnit*)output userData:(void*)inUserData
{
    AudioOutputUnitMIDICallbacks callBackStruct;
    callBackStruct.userData = inUserData;
    callBackStruct.MIDIEventProc = MIDIEventProcCallBack;
    callBackStruct.MIDISysExProc = NULL;
    AudioUnitSetProperty (*output,
                          kAudioOutputUnitProperty_MIDICallbacks,
                          kAudioUnitScope_Global,
                          0,
                          &callBackStruct,
                          sizeof(callBackStruct));
}

void MIDIEventProcCallBack(void *userData, UInt32 inStatus, UInt32 inData1, UInt32 inData2, UInt32 inOffsetSampleFrame)
{
    
    //...
}

#pragma mark CAUITransportEngine Protocol- Required methods
- (BOOL) canPlay   { return _connected;}
- (BOOL) canRewind { return _connected;}
- (BOOL) canRecord { return mCsData.csAUHAL != nil && ![self isHostPlaying]; }

- (BOOL) isHostPlaying   { return self.playing; }
- (BOOL) isHostRecording { return self.recording; }
- (BOOL) isHostConnected {
    
    
    if (mCsData.csAUHAL)
    {
        UInt32 connect;
        UInt32 dataSize = sizeof(UInt32);
        AudioUnitGetProperty(mCsData.csAUHAL, kAudioUnitProperty_IsInterAppConnected, kAudioUnitScope_Global, 0, &connect, &dataSize);
        
        if (connect != self.connected) {
            self.connected = connect;
            mCsData.interAppConnected = connect;
            //Transition is from not connected to connected
            if (self.connected) {
                //                [self checkStartStopGraph];
                //Get the appropriate callback info
                [self getHostCallBackInfo];
                [self getAudioUnitIcon];
            }
            //Transition is from connected to not connected;
            else {
                //If the graph is started stop it.
                //                if ([self isGraphStarted])
                //                    [self stopGraph];
                //                //Attempt to restart the graph
                //                [self checkStartStopGraph];
                if (callBackInfo) {
                    free(callBackInfo);
                    callBackInfo = NULL;
                }
            }
        }
    }
    return self.connected;
}

-(void) gotoHost {
    if (mCsData.csAUHAL) {
    }
}

-(void) getHostCallBackInfo {
    if (self.connected) {
        if (callBackInfo)
            free(callBackInfo);
        UInt32 dataSize = sizeof(HostCallbackInfo);
        callBackInfo = (HostCallbackInfo*) malloc(dataSize);
        OSStatus result = AudioUnitGetProperty(mCsData.csAUHAL, kAudioUnitProperty_HostCallbacks, kAudioUnitScope_Global, 0, callBackInfo, &dataSize);
        if (result != noErr) {
            NSLog(@"Error occured fetching kAudioUnitProperty_HostCallbacks : %d", (int)result);
            free(callBackInfo);
            callBackInfo = NULL;
        }
    }
}

-(void) togglePlay {
    if (self.recording)
        [self toggleRecord];
    else
        [self sendStateToRemoteHost:kAudioUnitRemoteControlEvent_TogglePlayPause];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"kTransportStateChangedNotificiation" object:self];
}

-(void) toggleRecord {
    [self sendStateToRemoteHost:kAudioUnitRemoteControlEvent_ToggleRecord];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"kTransportStateChangedNotificiation" object:self];
}

-(void) rewind {
    [self sendStateToRemoteHost:kAudioUnitRemoteControlEvent_Rewind];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"kTransportStateChangedNotificiation" object:self];
}

-(void) sendStateToRemoteHost:(AudioUnitRemoteControlEvent)state {
    if (mCsData.csAUHAL) {
     
    }
}

//Fetch the host's icon via AudioOutputUnitGetHostIcon, draw that in the view
-(UIImage *) getAudioUnitIcon {
    if (mCsData.csAUHAL)
        self.audioUnitIcon = scaleImageToSize(AudioOutputUnitGetHostIcon(mCsData.csAUHAL, 114), CGSizeMake(35, 35)) ;
    
	return self.audioUnitIcon;
}

-(NSString *) getPlayTimeString {
    
    [self updateTransportInfo];
    return formattedTimeStringForFrameCount(self.playTime, [[AVAudioSession sharedInstance] sampleRate], NO);
}

-(void) postUpdateStateNotification {
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"kTransportStateChangedNotificiation" object:self];
    });
}

#pragma mark Utility functions
NSString *formattedTimeStringForFrameCount(UInt64 inFrameCount, Float64 inSampleRate, BOOL inShowMilliseconds) {
    
    return @"NULL";
}

UIImage *scaleImageToSize(UIImage *image, CGSize newSize) {
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
	
    return newImage;
}

#pragma Audio Session Listner delegate

-(void) InterruptionListener: (NSNotification*) aNotification
{
    NSDictionary *interuptionDict = aNotification.userInfo;
    NSNumber* interuptionType = (NSNumber*)[interuptionDict valueForKey:AVAudioSessionInterruptionTypeKey];
    
    if([interuptionType intValue] == AVAudioSessionInterruptionTypeBegan) {
        
        //        NSLog(@"_____________________________%s Begin Interruption", __FUNCTION__);
        
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"audioGrabAbort"
         object:self];
        
        AudioOutputUnitStop(mCsData.csAUHAL);
    }
    
    else if ([interuptionType intValue] == AVAudioSessionInterruptionTypeEnded) {
        
        //        NSLog(@"_____________________________%s End Interruption", __FUNCTION__);
        AudioOutputUnitStart(mCsData.csAUHAL);
    }
}

-(void) appHasGoneInBackground {
    NSLog(@"%s", __FUNCTION__);
    mCsData.inForeground = NO;
}

-(void) appHasGoneForeground {
    NSLog(@"%s", __FUNCTION__);
    
    mCsData.inForeground = YES;
    
    [CsoundObj setAudioSessionActive];
    if (!mCsData.interAppConnected)
    {
        OSStatus err = AudioOutputUnitStart(mCsData.csAUHAL);
        NSLog(@"started audio after foregrounding, err: %d", (int)err);
    }
}


#pragma mark - Csound External Opcodes

+ (AudioStreamBasicDescription)nonInterleavedFloatStereoAudioDescription {
    AudioStreamBasicDescription audioDescription;
    memset(&audioDescription, 0, sizeof(audioDescription));
    audioDescription.mFormatID          = kAudioFormatLinearPCM;
    audioDescription.mFormatFlags       = kAudioFormatFlagIsFloat | kAudioFormatFlagIsPacked | kAudioFormatFlagIsNonInterleaved;
    audioDescription.mChannelsPerFrame  = 2;
    audioDescription.mBytesPerPacket    = sizeof(float);
    audioDescription.mFramesPerPacket   = 1;
    audioDescription.mBytesPerFrame     = sizeof(float);
    audioDescription.mBitsPerChannel    = 8 * sizeof(float);
    audioDescription.mSampleRate        = 44100.0;
    return audioDescription;
}
@end
