//
//  AudioUnitViewController_AUAudioUnitFactory.h
//  AUExtension
//
//  Created by Alessandro Petrolati on 10/08/16
//  Copyright (c) 2016 apeSoft. All rights reserved.
//

//@import CoreAudioKit;
#import "AudioUnitViewController.h"

@interface AudioUnitViewController (AUAudioUnitFactory) <AUAudioUnitFactory>

@end



