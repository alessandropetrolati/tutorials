//
//  MyAudioUnit.mm
//  AUExtension
//
//  Created by Alessandro Petrolati on 13/08/16.
//  Copyright © 2016 apeSoft. All rights reserved.
//

#import "MyAudioUnit.h"
#import "BufferedAudioBus.hpp"
#import "AudioUnitViewController.h"

/* DSP */
#define SAMPLE_RATE_DEF 44100.0
#define KSMPS_DEF 1024
#define EXTREME_BUFFER_SIZE 4096
#define CHANNELS_NUM 2

#define SNAP_BANK_SUBFIX @"ivcs3"
#define LOAD_FADE_TIME 0.5
#define UNSELECT_STRING @"---"
#define APP_SNAPSHOTS_TAG @"monogranulator_presets_tag"

#define ENABLE_INPUT

//const NSInteger kDefaultFactoryPreset = 0;

AUAudioUnitPreset* NewAUPreset(NSInteger number, NSString *name)
{
    AUAudioUnitPreset *aPreset = [AUAudioUnitPreset new];
    aPreset.number = number;
    aPreset.name = name;
    return aPreset;
}

@interface MyAudioUnit ()

@property AUAudioUnitBus *outputBus;
#ifdef ENABLE_INPUT
@property AUAudioUnitBusArray *inputBusArray;
#endif
@property AUAudioUnitBusArray *outputBusArray;
@property AUParameterTree *parameterTree;

@end

@implementation MyAudioUnit
{
    // C++ members need to be ivars; they would be copied on access if they were properties.
#ifdef ENABLE_INPUT
    BufferedInputBus _inputBus;
#else
    BufferedOutputBus _outputBusBuffer;
#endif
    
    AUHostMusicalContextBlock _musicalContext;
    
    __block AudioUnitViewController* controller;
    
    Float64 CURRENT_SAMPLING_RATE;
    Float64 CURRENT_BUFFER_FRAME;
    
    // Factory Presets
    AUAudioUnitPreset   *_currentPreset;
    NSInteger           _currentFactoryPresetIndex;
    NSArray<AUAudioUnitPreset *> *_presets;
    NSMutableArray* presetParameters;
    
    double lastTempoValue;
}

@synthesize parameterTree = _parameterTree;
@synthesize factoryPresets = _presets;

-(void)setController:(AudioUnitViewController*)ctrl andParameters:(NSArray*)childrens {
    
    if (!ctrl) {
        return;
    }
    
    controller = ctrl;
    
    [self createParametersTree:childrens];
    
    // set default preset as current
    // must be called with non null 'controller' ptr
    //    self.currentPreset = _presets.firstObject;
    
    [ctrl refreshParameters];
}

#pragma mark - AUAudioUnit owerride, suitable for saving as a user preset from/to Host
- (NSDictionary*)getFullState {
    
    return [controller getState];
}

- (void)setFullState:(NSDictionary*)state {
    
    [controller restoreState:state];
}

- (NSDictionary*)getFullStateForDocument {
    
    return [controller getState];
}

- (void)setFullStateForDocument:(NSDictionary*)state {
    
    [controller restoreState:state];
}

- (instancetype)initWithComponentDescription:(AudioComponentDescription)componentDescription
                                     options:(AudioComponentInstantiationOptions)options
                                       error:(NSError **)outError
{
    self = [super initWithComponentDescription:componentDescription options:options error:outError];
    
    if (self == nil)
    {
        return nil;
    }
    
    lastTempoValue = -1;
    
    // Initialize a default format for the busses.
    AVAudioFormat *defaultFormat = [[AVAudioFormat alloc] initStandardFormatWithSampleRate:SAMPLE_RATE_DEF channels:CHANNELS_NUM];
    
#ifdef ENABLE_INPUT
    // Create the input and output busses.
    _inputBus.init(defaultFormat, 8);
    _outputBus = [[AUAudioUnitBus alloc] initWithFormat:defaultFormat error:nil];
    
    // Create the input and output bus arrays.
    _inputBusArray  = [[AUAudioUnitBusArray alloc] initWithAudioUnit:self busType:AUAudioUnitBusTypeInput busses: @[_inputBus.bus]];
    _outputBusArray = [[AUAudioUnitBusArray alloc] initWithAudioUnit:self busType:AUAudioUnitBusTypeOutput busses: @[_outputBus]];
    
#else
    
    // Create the output bus.
    _outputBusBuffer.init(defaultFormat, CHANNELS_NUM);
    _outputBus = _outputBusBuffer.bus;
    
    // Create the input and output bus arrays.
    _outputBusArray = [[AUAudioUnitBusArray alloc] initWithAudioUnit:self busType:AUAudioUnitBusTypeOutput busses: @[_outputBus]];
#endif
    
    self.maximumFramesToRender = 1024;
    
    // Create factory preset array
    //[self makeFactoryPresets]; //warning no factory presets
    
    return self;
}

- (void)createParametersTree:(NSArray*)childrens {
    
    // Create the parameter tree.
    _parameterTree = [AUParameterTree createTreeWithChildren:childrens];
    
    __block struct audio_struct* data_ = &data;
    __block MyAudioUnit* self_ = self;
    
    // implementorValueObserver is called when a parameter changes value.
    _parameterTree.implementorValueObserver = ^(AUParameter *param, AUValue value) {

        switch (param.address) {
#warning aggiungi gli altri parametri che hai creato
            case slider_1_Parameter_INDEX:
                [self_ setSlider1Value:value];
                break;
        }
    };
    
    
    // implementorValueProvider is called when the value needs to be refreshed.
    _parameterTree.implementorValueProvider = ^(AUParameter *param) {
        
        AUValue value = 0.0;
        
        switch (param.address) {
#warning aggiungi gli altri parametri che hai creato
            case slider_1_Parameter_INDEX:
                value = data_->slider_1_Value;
                break;

        }
        return value;
    };
}

#ifdef ENABLE_INPUT
- (AUAudioUnitBusArray *)inputBusses {
    return _inputBusArray;
}
#endif

- (AUAudioUnitBusArray *)outputBusses {
    return _outputBusArray;
}

- (BOOL)allocateRenderResourcesAndReturnError:(NSError **)outError {
    
    if (![super allocateRenderResourcesAndReturnError:outError]) {
        return NO;
    }
    
#ifdef ENABLE_INPUT
    if (self.outputBus.format.channelCount != _inputBus.bus.format.channelCount) {
        if (outError) {
            *outError = [NSError errorWithDomain:NSOSStatusErrorDomain code:kAudioUnitErr_FailedInitialization userInfo:nil];
        }
        // Notify superclass that initialization was not successful
        self.renderResourcesAllocated = NO;
        
        return NO;
    }
    
    _inputBus.allocateRenderResources(self.maximumFramesToRender);
#else
    
    _outputBusBuffer.allocateRenderResources(self.maximumFramesToRender);
#endif
    
    //Tempo provided by host?
    if (self.musicalContextBlock) { _musicalContext = self.musicalContextBlock; } else _musicalContext = nil;
    
    // Initialize a default format for the busses.
    AVAudioSession *session = [AVAudioSession sharedInstance];
    NSTimeInterval bufferDurationVerbose = session.IOBufferDuration;
    
    Float32 srVerbose = [AVAudioSession sharedInstance].sampleRate;
    Float64 bufferFrameVerbose = roundf(bufferDurationVerbose * srVerbose);
    Float64 latency = (bufferFrameVerbose / srVerbose) * 1000.f;
    
    CURRENT_SAMPLING_RATE = srVerbose;
    CURRENT_BUFFER_FRAME = bufferFrameVerbose;
    
    NSLog(@"%@", [NSString stringWithFormat:@"Sampling Rate %.0f; Buffer Size %d; Latency %.3f msec. (large buffer increase latency but reduce CPU)",
                  CURRENT_SAMPLING_RATE, (int)CURRENT_BUFFER_FRAME, latency]);

    /* Creates and run Csound */
    data.cs = csoundCreate(NULL);
    
    csoundSetHostImplementedAudioIO(data.cs, 1, 0);
    csoundSetHostData(data.cs, (__bridge void *)(self));

    // Set's Environment Sound Files Dir
    NSString *resourcesPath = [[NSBundle mainBundle] resourcePath];
    NSString *csdFilePath = [[NSBundle mainBundle] pathForResource:@"VCA" ofType:@"csd"];
    
    /* Apply SR and BUFFER to DSPs */
    if ([[NSFileManager defaultManager] fileExistsAtPath:csdFilePath])
    {
        //        NSLog(@"file %@ Exists At Path!!!", pathAndName);
        
        NSString *myString = [[NSString alloc] initWithContentsOfFile:csdFilePath encoding:NSUTF8StringEncoding error:NULL];
        
        myString = [myString stringByReplacingOccurrencesOfString:@";;;;SR;;;;" withString:[NSString stringWithFormat:@"sr = %f", CURRENT_SAMPLING_RATE]];
        myString = [myString stringByReplacingOccurrencesOfString:@";;;;KSMPS;;;;" withString:[NSString stringWithFormat:@"ksmps = %f", CURRENT_BUFFER_FRAME]];
        
        // Generate the path of the tmp directory
        NSString *pathAndNameRUN = [NSString stringWithFormat:@"%@VCA_RUN.csd", NSTemporaryDirectory()];
        
        NSError* error = nil;
        
        //save copy of VCS3_dsp.csd in library directory
        [myString writeToFile:pathAndNameRUN
                   atomically:NO
                     encoding:NSUTF8StringEncoding
                        error:&error];
        
        /* Take a little time to absicure .csd was correctly written */
        NSString* envFlag = @"--env:SFDIR+=";
        char* SFDIR = (char*)[[envFlag stringByAppendingString:resourcesPath] cStringUsingEncoding:NSASCIIStringEncoding];
        envFlag = @"--env:SADIR+=";
        char* SADIR = (char*)[[envFlag stringByAppendingString:resourcesPath] cStringUsingEncoding:NSASCIIStringEncoding];
        
        const char *argv[4] = { "csound", SFDIR, SADIR, (const char*)[pathAndNameRUN cStringUsingEncoding:NSASCIIStringEncoding]};
        data.ret = csoundCompile(data.cs, 4, (char**)argv);
        
        if(!data.ret) {
            data.nchnls = csoundGetNchnls(data.cs);
            data.bufframes = (csoundGetOutputBufferSize(data.cs))/data.nchnls;
            data.running = true;
        }
    }
    else {
        NSLog(@"file %@ Does Not Exists At Path!!!", csdFilePath);
    }
    
#ifdef ENABLE_MIDI
    [CsoundMIDI setMidiInCallbacks:data.cs];
#endif

    if (controller) {
        
        [controller refreshParameters];
    }
    
    return YES;
}

- (void)deallocateRenderResources {
    
    _musicalContext = nullptr;
    
#ifdef ENABLE_INPUT
    _inputBus.deallocateRenderResources();
#else
    _outputBusBuffer.deallocateRenderResources();
#endif
    
    data.running = false;
    
    csoundStop(data.cs);
    csoundCleanup(data.cs);
    csoundReset(data.cs);
    csoundDestroy(data.cs);
    
    data.cs = nil;
    
    [super deallocateRenderResources];
}

-(void)dealloc {
    
    _presets = nil;
    presetParameters = nil;
}

#pragma mark - Audio Processing
- (AUInternalRenderBlock)internalRenderBlock {
    
    /*
     Capture in locals to avoid ObjC member lookups. If "self" is captured in
     render, we're doing it wrong.
     */
    __block MyAudioUnit *self_ = self;
    __block struct audio_struct* data_ = &data;
#ifdef ENABLE_INPUT
    __block BufferedInputBus *input = &_inputBus;
#endif
    
    return ^AUAudioUnitStatus(
                              AudioUnitRenderActionFlags *actionFlags,
                              const AudioTimeStamp       *timestamp,
                              AVAudioFrameCount           frameCount,
                              NSInteger                   outputBusNumber,
                              AudioBufferList            *outputData,
                              const AURenderEvent        *realtimeEventListHead,
                              AURenderPullInputBlock      pullInputBlock) {
        
#ifdef ENABLE_INPUT
        AudioUnitRenderActionFlags pullFlags = 0;
        AUAudioUnitStatus err = input->pullInput(&pullFlags, timestamp, frameCount, 0, pullInputBlock);
        if (err != 0) { return err; }
#endif
        
        AURenderEvent const *event = realtimeEventListHead;
        
        while (event) {
            
            switch (event->head.eventType) {
                case AURenderEventParameter:
                case AURenderEventParameterRamp:
                {
                    AUParameterEvent const& paramEvent = event->parameter;
                    //                        AUParameter * param = [__parameterTree__ parameterWithAddress:paramEvent.parameterAddress];
                    //                        param.value = paramEvent.value;
                    
                    switch (paramEvent.parameterAddress) {
#warning aggiorna le variabili di appoggio provenienti dalle automazioni dell'host
                        case slider_1_Parameter_INDEX:
                            [self_ setSlider1Value:paramEvent.value];
                            break;
                    }
                    
                    break;
                }
                    
                case AURenderEventMIDI:
                {
                    /* Get MIDI Event from Keyboard */

                    break;
                }
                    
                default:
                    break;
            }
            
            // Go to next event.
            event = event->parameter.next;
        }
        
        /* Get tempo from host */
        double currentTempo;
        
        if ( self->_musicalContext ) { // only use this if the host supports it...
            if (self->_musicalContext( &currentTempo, NULL, NULL, NULL, NULL, NULL ) ) {
                //            plugin->handleTempoSetting(currentTempo);
                
                if (currentTempo != self_->lastTempoValue) {
                    self_->lastTempoValue = currentTempo;
                    dispatch_async(dispatch_get_main_queue(), ^{
                        //BPM did changed from Host
                    });
                }
            }
        }
        /*
         If the caller passed non-nil output pointers, use those. Otherwise,
         process in-place in the input buffer. If your algorithm cannot process
         in-place, then you will need to preallocate an output buffer and use
         it here.
         */
        
#ifdef ENABLE_INPUT
        
        AudioBufferList *inAudioBufferList = input->mutableAudioBufferList;
        AudioBufferList *outAudioBufferList = outputData;
        
        if (outAudioBufferList->mBuffers[0].mData == nullptr)
        {
            for (UInt32 i = 0; i < outAudioBufferList->mNumberBuffers; ++i) {
                outAudioBufferList->mBuffers[i].mData = inAudioBufferList->mBuffers[i].mData;
            }
        }
#endif
        
        /* ==================== CSOUND PROCESS ==================== */

        Csound_Perform(data_, frameCount, outputData
#ifdef ENABLE_INPUT
                       , inAudioBufferList
#endif
                       );
        
        /*  ==================== END ==================== */
        
        return noErr;
    };
}

#pragma - Mark Factory Presets
-(void)makeFactoryPresets
{
#warning quando avrai un dizionario dei presets puoi aggiungerli per default
//    if (!presetParameters) {
//        presetParameters = [[NSMutableArray alloc] init];
//    }
//    else {
//
//        [presetParameters removeAllObjects];
//    }
//
//    NSMutableArray* presetsTMP = [[NSMutableArray alloc] init];
//    NSMutableArray *keysSorted = [[NSMutableArray alloc] init];
//
//    int i = 0;
//
//    /* Read Built-In Preset Banks */
//    for (NSString* name in [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[[NSBundle mainBundle] resourcePath]  error:nil]) {
//        BOOL subfix = [[name pathExtension] isEqualToString:SNAP_BANK_SUBFIX];
//
//        if(subfix && name) {
//
//            NSString* bankName = [name stringByDeletingPathExtension];
//
//            /* Retrive Keys for Presets */
//            NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:bankName ofType:SNAP_BANK_SUBFIX]];
//            NSArray *keys = [dictionary allKeys];
//            keysSorted = [[keys sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)] mutableCopy];
//
//            /* Iterate through rough every preset and filter apeFilter AUv3 Informations, according to AUv3 Implemantation  */
//            for (NSString *key in keysSorted) {
//
//                NSDictionary *preset = [dictionary objectForKey:key];
//
//
//                /* Copy all keys for the factory presets parameters */
//                if (preset) {
//                    NSString* name = [preset objectForKey:APP_SNAPSHOTS_TAG];
//                    [presetsTMP addObject:NewAUPreset(i++, [NSString stringWithFormat:@"%@ (%@)", name, bankName])];
//                    [presetParameters addObject:preset];
//                }
//            }
//        }
//    }
//
//    // Create factory preset array.
//    _currentFactoryPresetIndex = kDefaultFactoryPreset;
//    _presets = presetsTMP;
//
//    presetsTMP = nil;
//    keysSorted = nil;
}

- (AUAudioUnitPreset *)currentPreset
{
    
    if (_currentPreset.number >= 0) {
        NSLog(@"Returning Current Factory Preset: %ld\n", (long)_currentFactoryPresetIndex);
        return [_presets objectAtIndex:_currentFactoryPresetIndex];
    } else {
        NSLog(@"Returning Current Custom Preset: %ld, %@\n", (long)_currentPreset.number, _currentPreset.name);
        return _currentPreset;
    }
}

- (void)setCurrentPreset:(AUAudioUnitPreset *)currentPreset
{
    if (nil == currentPreset) { NSLog(@"nil passed to setCurrentPreset!"); return; }
    
    if (currentPreset.number >= 0) {
        // factory preset
        for (AUAudioUnitPreset *factoryPreset in _presets) {
            if (currentPreset.number == factoryPreset.number) {
                
                [controller restoreState:presetParameters[factoryPreset.number]];
                
                // set factory preset as current
                _currentPreset = currentPreset;
                _currentFactoryPresetIndex = factoryPreset.number;
                NSLog(@"currentPreset Factory: %ld, %@\n", (long)_currentFactoryPresetIndex, factoryPreset.name);
                
                break;
            }
        }
    } else if (nil != currentPreset.name) {
        // set custom preset as current
        _currentPreset = currentPreset;
        NSLog(@"currentPreset Custom: %ld, %@\n", (long)_currentPreset.number, _currentPreset.name);
    } else {
        NSLog(@"setCurrentPreset not set! - invalid AUAudioUnitPreset\n");
    }
}

#pragma mark - PARAM -> DSP

#warning aggiorna Csound sul canale che vuoi, in questo caso slider_1
-(void) setSlider1Value:(double)value {
    
    if (!data.cs || !data.running) return;
    
    NSString* channelName = @"slider_1";
    float *val;
    csoundGetChannelPtr(data.cs, &val, [channelName cStringUsingEncoding:NSASCIIStringEncoding],
                        CSOUND_CONTROL_CHANNEL | CSOUND_INPUT_CHANNEL);
    
    *val = (float) value;
    data.slider_1_Value = value;
}

#pragma mark - Csound Render Processing
OSStatus  Csound_Perform(void *inRefCon,
                         UInt32 inNumberFrames,
                         AudioBufferList *outData
#ifdef ENABLE_INPUT
                         , AudioBufferList* inData
#endif
)
{
    
    audio_struct  *cdata = (audio_struct *) inRefCon;
    
    if(cdata->running == false) {
        
        // Clear audio input
        for ( int i = 0; i < outData->mNumberBuffers; i++ )
            memset(outData->mBuffers[i].mData, 0, outData->mBuffers[i].mDataByteSize);
        
        return noErr;
    }
    
    int ret = cdata->ret, nchnls = cdata->nchnls;
    CSOUND *cs = cdata->cs;
    float slices = inNumberFrames/csoundGetKsmps(cs);
    int ksmps = csoundGetKsmps(cs);
#ifdef ENABLE_INPUT
    MYFLT *spin = csoundGetSpin(cs);
#endif
    MYFLT *spout = csoundGetSpout(cs);
    MYFLT *buffer;
    
    
    /* CSOUND PERFORM */
    if (slices < 1.0) {
        /* inNumberFrames < ksmps */
        Csound_Perform_DOWNSAMP(inRefCon, inNumberFrames, outData
#ifdef ENABLE_INPUT
                                , inData
#endif
                                );
    }
    else
    {
        /* inNumberFrames => ksmps */
        for(int i = 0; i < (int)slices; ++i){
            
            /* performance */
#ifdef ENABLE_INPUT
            for (int k = 0; k < nchnls; ++k) {
                buffer = (MYFLT *) inData->mBuffers[k].mData;
                for(int j = 0; j < ksmps; ++j) {
                    spin[j*nchnls+k] = buffer[j+i*ksmps];
                }
            }
#endif
            
            if(!ret) {
                ret = csoundPerformKsmps(cs);
            } else {
                cdata->running = false;
            }
            
            for (int k = 0; k < nchnls; ++k) {
                buffer = (MYFLT *) outData->mBuffers[k].mData;
                //                if (cdata->shouldMute == false) {
                for(int j = 0; j < ksmps; ++j) {
                    buffer[j+i*ksmps] = (MYFLT) spout[j*nchnls+k];
                }
                //                } else {
                //                    memset(buffer, 0, sizeof(MYFLT) * inNumberFrames);
                //                }
            }
            
        }
        
        cdata->ret = ret;
    }
    
    
    return noErr;
}

OSStatus  Csound_Perform_DOWNSAMP(void *inRefCon,
                                  UInt32 inNumberFrames,
                                  AudioBufferList *outData
#ifdef ENABLE_INPUT
                                  , AudioBufferList* inData
#endif

)
{
    
    audio_struct *cdata = (audio_struct *) inRefCon;
    int ret = cdata->ret, nchnls = cdata->nchnls;
    CSOUND *cs = cdata->cs;
#ifdef ENABLE_INPUT
    MYFLT *spin = csoundGetSpin(cs);
#endif
    MYFLT *spout = csoundGetSpout(cs);
    MYFLT *buffer;
    
    
    /* DOWNSAMPLING FACTOR */
    int UNSAMPLING = csoundGetKsmps(cs)/inNumberFrames;
    
    if (cdata->counter < UNSAMPLING-1) {
        
        cdata->counter++;
    }
    else {
        
        cdata->counter = 0;
        
        /* CSOUND PROCESS KSMPS */
        if(!cdata->ret) {
            /* PERFORM CSOUND */
            cdata->ret = csoundPerformKsmps(cs);
        } else {
            cdata->running = false;
            
        }
    }
    
    /* INCREMENTS DOWNSAMPLING COUNTER */
    int slice_downsamp = inNumberFrames * cdata->counter;
    
    /* COPY IN CSOUND SYSTEM SLICE INPUT */
#ifdef ENABLE_INPUT
    for (int k = 0; k < nchnls; ++k){
        buffer = (MYFLT *) inData->mBuffers[k].mData;
        for(int j = 0; j < inNumberFrames; ++j){
            spin[(j+slice_downsamp)*nchnls+k] = buffer[j];
        }
    }
#endif
    
    /* COPY OUT CSOUND KSMPS SLICE */
    for (int k = 0; k < nchnls; ++k) {
        buffer = (MYFLT *) outData->mBuffers[k].mData;
        //        if (cdata->shouldMute == false) {
        for(int j = 0; j < inNumberFrames; ++j) {
            
            buffer[j] = (MYFLT) spout[(j+slice_downsamp)*nchnls+k];
        }
        //        } else {
        //            memset(buffer, 0, sizeof(AudioUnitSampleType) * inNumberFrames);
        //        }
    }
    
    cdata->ret = ret;
    return  noErr;
}
@end
