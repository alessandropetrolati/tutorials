
/*

monogranulator csd

*/

<CsoundSynthesizer>
<CsOptions>

-o dac
--realtime
;--sample-accurate
-+rtmidi=null
-+rtaudio=null
-d
-+msg_color=0
--expression-opt
-M0
-m0
-i adc

</CsOptions>
<CsInstruments>

sr = 44100
ksmps = 64

;;;;SR;;;;		//strings replaced from app with own values
;;;;KSMPS;;;;

nchnls = 2
0dbfs = 1

;------------------------------------------------------------
;to make sure does not plays everything...
;------------------------------------------------------------
massign 1,0
massign 2,0
massign 3,0
massign 4,0
massign 5,0
massign 6,0
massign 7,0
massign 8,0
massign 9,0
massign 10,0
massign 11,0
massign 12,0
massign 13,0
massign 14,0
massign 15,0
massign 16,0

pgmassign 0, 0

maxalloc 50,1 ;OUT 1

;---------------------------------------------------------
instr 50 ; OUTPUT LEVEL
;---------------------------------------------------------

kamp 	chnget "slider_1"

aSignalIn1, aSignalIn2 ins

outs aSignalIn1*kamp, aSignalIn2*kamp

endin

</CsInstruments>
<CsScore>

i50 0 360000000000

</CsScore>
</CsoundSynthesizer>

