//
//  AudioUnitViewController_AUAudioUnitFactory.h
//  AUExtension
//
//  Created by Alessandro Petrolati on 10/08/16
//  Copyright (c) 2016 apeSoft. All rights reserved.
//

#import "AudioUnitViewController+AUAudioUnitFactory.h"
#import "MyAudioUnit.h"

@implementation AudioUnitViewController (AUAudioUnitFactory)

- (MyAudioUnit *) createAudioUnitWithComponentDescription:(AudioComponentDescription) desc error:(NSError **)error
{
    self.audioUnit = [[MyAudioUnit alloc] initWithComponentDescription:desc error:error];
    
    // set here bridge controller and data
    [self.audioUnit setController:self andParameters:self.paramChildrens];
    
    return self.audioUnit;
}
@end
