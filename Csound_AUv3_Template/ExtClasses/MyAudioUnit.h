//
//  MyAudioUnit.h
//  AUExtension
//
//  Created by Alessandro Petrolati on 13/08/16.
//  Copyright © 2016 apeSoft. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import <Accelerate/Accelerate.h>

#import "csound.h"
#import "csdl.h"

enum {
#warning aggiungi gli indici relativi ai parametri che hai creato
    slider_1_Parameter_INDEX = 0,
};


@class AudioUnitViewController;

struct audio_struct
{
    
    CSOUND* cs;
    
//    __unsafe_unretained YOUR_OBJ *myObjCClass;
    
    /* Csound data */
    OSStatus err;
    long bufframes;
    int ret;
    int nchnls;
    int counter;
    bool running;
    
#warning crea variabili d'appoggio sulla base dei parametri
    float slider_1_Value;
};

@interface MyAudioUnit : AUAudioUnit //AUAudioUnitV2Bridge
{
    
    struct audio_struct data;
    
}
-(void)setController:(AudioUnitViewController*)ctrl andParameters:(NSArray*)childrens;
-(void)createParametersTree:(NSArray*)childrens;

@end
