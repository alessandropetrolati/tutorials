//
//  AudioUnitViewController.m
//  AUExtension
//
//  Created by Alessandro Petrolati on 10/08/16
//  Copyright (c) 2016 apeSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "AudioUnitViewController.h"
#import "MyAudioUnit.h"

@interface AudioUnitViewController ()
{
#warning add widgets
    IBOutlet UISlider* _slider_1;

#warning create relative AUParameters
    /* AUParameters */
    AUParameter* SLIDER_1_PARAMETER;
}

@end

@implementation AudioUnitViewController

@synthesize paramChildrens = _paramChildrens;

-(void)setAudioUnit:(MyAudioUnit *)audioUnit
{
    _audioUnit = audioUnit;
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if ([self isViewLoaded]) {
            [self connectViewWithAU];
        }
    });
}

-(MyAudioUnit *)getAudioUnit
{
    return _audioUnit;
}

#pragma mark - AUAudioUnit owerride, suitable for saving as a user preset from/to Host
-(NSDictionary*)getState {
    
    NSMutableDictionary* connectionState = [[NSMutableDictionary alloc] init];

    [connectionState setObject:[NSNumber numberWithFloat:_slider_1.value]  forKey:@"mySliderChannel"];

    return (NSDictionary*)connectionState;
}

-(void)restoreState:(NSDictionary*)state {

    if (!state) {
        return;
    }
    [self resizeWidgets];

    dispatch_async(dispatch_get_main_queue(), ^{
        
        NSNumber *val = nil;
        if((val = [state objectForKey:@"mySliderChannel"])) { self->_slider_1.value = [val floatValue]; }
        
        [self refreshParameters];
    });
}

-(void)refreshParameters {
    
//    NSLog(@"------------------------------%s", __func__);

#warning refresh widgets
    [_slider_1 sendActionsForControlEvents:UIControlEventAllEvents];
}

- (void)connectViewWithAU
{
    AUParameterTree *parameterTree = self.audioUnit.parameterTree;
    
    if (parameterTree)
    {
        [parameterTree tokenByAddingParameterObserver:^(AUParameterAddress address, AUValue value) {

            dispatch_async(dispatch_get_main_queue(), ^{
                
//                NSLog(@"tokenByAddingParameterObserver -> %llu -> %f", address, value);
                
                /* Update DSP Parameter */
                switch (address) {
#warning update UI parameters
                    case slider_1_Parameter_INDEX:
                        self->_slider_1.value = value;
                        break;
                }
            });
        }];
    }
}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    
    /* Resize View According to Host View*/
    CGFloat scale = self.view.bounds.size.width / 1024.0;
    
    /* Audiobus 3 UI issue, workaround */
    if (/*self.view.frame.size.width >= 1024.0 && */ scale >= 2.0) return;

    self.view.transform = CGAffineTransformMakeScale(scale, scale);
    
    [self.view setFrame:self.view.bounds];
    
    [self resizeWidgets];
    [self refreshParameters];
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
}


- (void)viewDidLoad
{
    
//    NSLog(@"------------------------------%s", __func__);
    
    [super viewDidLoad];
    
    [self addObserver:self forKeyPath:@"view.bounds" options:NSKeyValueObservingOptionOld context:NULL];


    // Create the parameter tree.
    AudioUnitParameterOptions flags = kAudioUnitParameterFlag_IsWritable | kAudioUnitParameterFlag_IsReadable;
#warning create AUparameters according to UI parameters
    SLIDER_1_PARAMETER = [AUParameterTree createParameterWithIdentifier:@"mySliderChannel" name:@"Slider Name"
                                                            address:slider_1_Parameter_INDEX
                                                                min:_slider_1.minimumValue
                                                                max:_slider_1.maximumValue
                                                               unit:kAudioUnitParameterUnit_Generic
                                                           unitName:nil
                                                              flags:flags
                                                       valueStrings:nil
                                                dependentParameters:nil];

#warning add here AUparams
    // Create the parameter tree.
    _paramChildrens = @[SLIDER_1_PARAMETER];
    
    if (_audioUnit)
    {
        [self connectViewWithAU];
    }
    [self refreshParameters];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext>  _Nonnull context) {
        
        [self deprecate_willRotateToInterfaceOrientation];
        
    } completion:^(id<UIViewControllerTransitionCoordinatorContext>  _Nonnull context) {
        
        [self deprecate_didRotateFromInterfaceOrientation];
    }];
}

- (void)deprecate_willRotateToInterfaceOrientation {

    [self resizeWidgets];
}

- (void)deprecate_didRotateFromInterfaceOrientation {

    [self resizeWidgets];
}

-(void)resizeWidgets {
    
    [self.view setNeedsDisplay];
    
    for (id w in self.view.subviews) {
        
        if ([w isKindOfClass:[UIView class]]) {
            
            UIView* r = w;
            for (UIView* r_ in r.subviews) {
                if ([r_ respondsToSelector:@selector(setNeedsDisplay)]) {
                    [r_ setNeedsDisplay];
                }
            }
        }
        
        if ([w respondsToSelector:@selector(setNeedsDisplay)])
            [w setNeedsDisplay];
    }
}

- (void)dealloc {
    
    [self removeObserver:self forKeyPath:@"view.bounds"];
}


#warning add UI actions for any widgets and parameter
#pragma mark - UI actions
-(IBAction)ui_set_slider_1_value_from_ui:(UISlider*)sender {
    
    SLIDER_1_PARAMETER.value = _slider_1.value;
}
@end
