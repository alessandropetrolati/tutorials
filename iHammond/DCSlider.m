//
//  DCSlider.m
//
//  Copyright 2011 Domestic Cat. All rights reserved.
//  https://github.com/domesticcatsoftware/DCControls
//  All work is under MIT license.

#import "DCSlider.h"

@implementation DCSlider
@synthesize handleSize, cornerRadius, biDirectional;
@synthesize doubleTapValue, tripleTapValue;


#pragma mark -
#pragma mark Init

- (id)initWithDelegate:(id)aDelegate
{
    if ((self = [super initWithDelegate:aDelegate]))
    {
        // add the gesture recognizers for double & triple taps
        UITapGestureRecognizer *doubleTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTap:)];
        doubleTapGesture.numberOfTapsRequired = 2;
        [self addGestureRecognizer:doubleTapGesture];
        
        self.cornerRadius = 3.0;
    }
    
    return self;
}

// to setup handle size
- (void)setFrame:(CGRect)frame
{
    self.handleSize = self.bounds.size.height / 6;
    if (self.handleSize < 35.0)
        self.handleSize = 35.0;
    [super setFrame:frame];
}

#pragma mark -
#pragma mark Touch Handling

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint touchPosition = [touch locationInView:self];
    CGFloat handleOrigin;
    CGFloat valueInternal = (self.value - self.min) / (self.max - self.min);
    
    handleOrigin = self.bounds.size.height - self.handleSize - (self.bounds.size.height - self.handleSize) * valueInternal;
    
    // if the touch is inside the handle then save the offset of the touch from the handle
    if ((touchPosition.y > handleOrigin && touchPosition.y < handleOrigin + handleSize))
    {
        touchHandleOffset = touchPosition.y - handleOrigin;
    }
    else
    {
        // set the handle offset to -1 so touchesmoved events are ignored
        touchHandleOffset = -1;
    }
    
    [self.delegate controlValueDidDown:self.value sender:self];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (touchHandleOffset == -1)
        return;
    
    UITouch *touch = [touches anyObject];
    CGPoint touchPosition = [touch locationInView:self];
    
    CGFloat newValue;
    
    newValue = 1 - (touchPosition.y - touchHandleOffset) / (self.bounds.size.height - self.handleSize);
    
    [self setValue:self.min + newValue * (self.max - self.min)];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.delegate controlValueDidUp:self.value sender:self];
}

#pragma mark -
#pragma mark Drawing

- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGRect boundsRect;
    
    if (drawBarMode)
        boundsRect = CGRectMake(self.bounds.origin.x, self.bounds.origin.y, self.bounds.size.width,
                                self.bounds.size.height - (self.bounds.size.height - CGRectGetMinY([self rectForHandle])));
    else
        boundsRect = self.bounds;
    
    //	const CGFloat *colorComponents = CGColorGetComponents(self.color.CGColor);
    
    //	UIColor *backgroundColor = [UIColor colorWithRed:colorComponents[0]
    //											   green:colorComponents[1]
    //												blue:colorComponents[2]
    //											   alpha:self.backgroundColorAlpha];
    //
    //	UIColor *lighterBackgroundColor = [UIColor colorWithRed:colorComponents[0]
    //                                                      green:colorComponents[1]
    //                                                       blue:colorComponents[2]
    //                                                      alpha:self.backgroundColorAlpha / 2.0];
    
    UIColor *backgroundColor;
    UIColor *lighterBackgroundColor;
    
    if (drawBarMode) {
        backgroundColor = [UIColor clearColor];
        lighterBackgroundColor = self.color;
    }
    else {
        backgroundColor = [self.color colorWithAlphaComponent:0.5];
        lighterBackgroundColor = [self.color colorWithAlphaComponent:0.5];
    }
    
    // draw background of slider
    [lighterBackgroundColor set];
    [self context:context addRoundedRect:boundsRect cornerRadius:self.cornerRadius];
    CGContextFillPath(context);
    
    // draw the 'filled' section to the left of the handle (or from the handle if in bidirectional mode)
    CGRect valueRect;
    [backgroundColor set];
    
    // draw the 'filled' section below the handle (or from the handle if in bidirectional mode) using a colour slightly lighter than the theme
    CGFloat handlePos = CGRectGetMinY([self rectForHandle]);
    CGFloat handleMid = CGRectGetMidY([self rectForHandle]);
    CGFloat handleMin = CGRectGetMinY([self rectForHandle]);
    
    if (self.biDirectional)
    {
        if (self.value > (self.max - self.min) / 2)
            valueRect = CGRectMake(0, handleMid, self.bounds.size.width, self.bounds.size.height / 2.0 - handleMid);
        else
            valueRect = CGRectMake(0, self.bounds.size.height / 2.0, self.bounds.size.width, handleMid - self.bounds.size.height / 2.0);
        [self context:context addRoundedRect:valueRect cornerRadius:0];
    }
    else
    {
        valueRect = CGRectMake(0, handleMin, self.bounds.size.width, self.bounds.size.height - handleMin);
        [self context:context addRoundedRect:valueRect cornerRadius:self.cornerRadius];
    }
    
    CGContextFillPath(context);
    
    valueRect = CGRectMake(0, handlePos, self.bounds.size.width, handleSize);
    
    // draw the handle
    [self.color set];
    [self context:context addRoundedRect:valueRect cornerRadius:self.cornerRadius];
    CGContextStrokePath(context);
    
    // draw value string as needed
    if (self.displaysValue)
    {
        [self.labelColor set];
        NSString *valueString = nil;
        if (self.biDirectional) {
            if (drawBarMode)
                valueString = [NSString stringWithFormat:@"%2.0f%%", (([self max] - self.value - self.min - (self.max - self.min) / 2) / (self.max - self.min)) * 100];
            else
                valueString = [NSString stringWithFormat:@"%2.0f%%", ((self.value - self.min - (self.max - self.min) / 2) / (self.max - self.min)) * 100];
        }
        else {
            if (drawBarMode)
                valueString = [NSString stringWithFormat:@"%0.0f", (([self max] - self.value - self.min) / (self.max - self.min)) * 10];
            else
                valueString = [NSString stringWithFormat:@"%0.0f", ((self.value - self.min) / (self.max - self.min)) * 10];
        }
        
        CGSize valueStringSize = [valueString sizeWithAttributes:@{NSFontAttributeName:self.labelFont}];
        
        CGRect handleRect = [self rectForHandle];
        
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle defaultParagraphStyle] mutableCopy];
        paragraphStyle.lineBreakMode = NSLineBreakByTruncatingTail;
        [valueString drawInRect:CGRectMake(handleRect.origin.x + (handleRect.size.width - valueStringSize.width) / 2,
                                           handleRect.origin.y + (handleRect.size.height - valueStringSize.height) / 2,
                                           valueRect.size.width,
                                           valueRect.size.height)
                 withAttributes: @{NSFontAttributeName:self.labelFont, NSParagraphStyleAttributeName: paragraphStyle }];
    }
}

- (CGRect)rectForHandle
{
    //    UIColor *lighterBackgroundColor = [UIColor clearColor];
    //    [lighterBackgroundColor set];
    
    CGRect valueRect;
    
    float handlePos = (self.bounds.size.height - handleSize) - ((self.bounds.size.height - handleSize) * ((self.value - self.min) / (self.max - self.min)));
    valueRect = CGRectMake(0, handlePos, self.bounds.size.width, handleSize);
    
    return valueRect;
}

#pragma mark -
#pragma mark Gestures

- (void)doubleTap:(UIGestureRecognizer *)gestureRecognizer
{
    
    if (self.allowsGestures)
    {
        [self performSelector:@selector(setValueFromGesture:) withObject:[NSNumber numberWithFloat:self.doubleTapValue]];
    }
}

- (void)tripleTap:(UIGestureRecognizer *)gestureRecognizer
{
    if (self.allowsGestures)
    {
        // cancel the double tap
        [NSThread cancelPreviousPerformRequestsWithTarget:self selector:@selector(setValueFromGesture:) object:[NSNumber numberWithFloat:self.doubleTapValue]];
        
        [self performSelector:@selector(setValueFromGesture:) withObject:[NSNumber numberWithFloat:self.tripleTapValue]];
    }
}

- (void)setValueFromGesture:(NSNumber *)newValue
{
    
    self.value = [newValue floatValue];
}

- (void)setDrawBarMode:(BOOL) mode {
    
    drawBarMode = mode;
}

-(BOOL)getDrawBarMode {
    
    return drawBarMode;
}

@end
