//
//  APEKnob.m
//  apeSoft
//
//  Created by Alessandro Petrolati on 01/02/12.
//  Copyright (c) 2012 apeSoft. All rights reserved.
//

#import "APEKnob.h"
#import <QuartzCore/QuartzCore.h>
#import "CsoundObj.h"

@implementation APEKnob
@synthesize knob;
//@synthesize value;
@synthesize cacheDirty = mCacheDirty;

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, self.frame.size.width+20);
    
    /* Shadows */
	self.layer.shadowColor = BACKGROUND_PARAMS_UICOLOR.CGColor;
	self.layer.shadowOffset = CGSizeZero;
	self.layer.shadowRadius = 9.0;
	self.layer.shadowOpacity = 0.7;
    
	self.knob = [[DCKnob alloc] initWithDelegate:self];
	CGFloat initialKnobSize = self.frame.size.width;
	self.knob.frame = CGRectMake(floorf((self.frame.size.width - initialKnobSize) / 2),
								 floorf(((self.frame.size.height - initialKnobSize)-20) / 2),
								 initialKnobSize,
								 initialKnobSize);
    
    
    BOOL fineButtons = NO;
    
	self.knob.labelFont = [UIFont fontWithName:@"Helvetica" size:12.0]; //[UIFont boldSystemFontOfSize:15.0];
    self.knob.labelOffset = CGPointMake(0, 0); //text X offset
    self.knob.cutoutSize = 15; //spazio tra l'inizio e la fine del loop in pixels
    
    /* Color */
	self.backgroundColor = [UIColor clearColor];    
    //    self.knob.color = [UIColor darkGrayColor];
    int ndx = 4; //black, red, green, blue
	switch (ndx)
	{
		case 0: self.knob.color = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.7]; break;
		case 1: self.knob.color = [UIColor colorWithRed:1.0 green:0.0 blue:0.0 alpha:1.0]; break;
		case 2: self.knob.color = [UIColor colorWithRed:0.0 green:1.0 blue:0.0 alpha:1.0]; break;
		case 3: self.knob.color = [UIColor colorWithRed:0.0 green:0.0 blue:1.0 alpha:1.0]; break;
		case 4: self.knob.color = BACKGROUND_PARAMS_UICOLOR; break;            
        case 5: self.knob.color = BACKGROUND_WAVE_UICOLOR; break;            
	}
    
	self.knob.backgroundColor = [UIColor clearColor];  
	self.knob.backgroundColorAlpha = 0.5; //set unused knob arc alpha
    self.knob.labelColor = [UIColor blackColor];
    
    
    self.knob.arcStartAngle = 180; //posizione apertura anello in gradi
    self.knob.valueArcWidth = 12;  //dimensioni dell'anello in pixels
    
    self.knob.biDirectional = bipolar;
    
    if (bipolar)
        self.knob.min = -[max floatValue]; //min
    else
        self.knob.min = [min floatValue]; //min
    
	self.knob.max = [max floatValue]; //max
	self.knob.value = [def floatValue]; //default value
    
	self.knob.doubleTapValue = [def floatValue]; //double tap value
	self.knob.tripleTapValue = [def floatValue]; //triple tap value
    
    [self addSubview:knob];
    
    if (fineButtons) {
        // Fine Step
        Float32 stepsNumbers = 350.f;
        Float32 range = self.knob.max - self.knob.min;    
        fineTuneAmount = range / stepsNumbers;
        
        decreaseButton = [UIButton buttonWithType:UIButtonTypeInfoLight];
        [decreaseButton setImage:[UIImage imageNamed:@"chevron-rev~iphone"] forState:UIControlStateNormal];
        decreaseButton.frame = CGRectMake(0, self.frame.size.height-20, 20, 20);
        decreaseButton.layer.shadowColor = self.layer.shadowColor;
        decreaseButton.layer.shadowOffset = self.layer.shadowOffset;
        decreaseButton.layer.shadowRadius = self.layer.shadowRadius;
        decreaseButton.layer.shadowOpacity = self.layer.shadowOpacity;
        [self addSubview:decreaseButton];
        [decreaseButton addTarget:self action:@selector(fineTuneButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        increaseButton = [UIButton buttonWithType:UIButtonTypeInfoLight];
        [increaseButton setImage:[UIImage imageNamed:@"chevron~iphone"] forState:UIControlStateNormal];
        increaseButton.frame = CGRectMake(self.frame.size.width-20 , self.frame.size.height-20, 20, 20);
        increaseButton.layer.shadowColor = self.layer.shadowColor;
        increaseButton.layer.shadowOffset = self.layer.shadowOffset;
        increaseButton.layer.shadowRadius = self.layer.shadowRadius;
        increaseButton.layer.shadowOpacity = self.layer.shadowOpacity;
        [self addSubview:increaseButton];
        [increaseButton addTarget:self action:@selector(fineTuneButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        // add the gesture recognizers for the UILongPressGestureRecognizer 
        UILongPressGestureRecognizer *longPressIncrease = [[UILongPressGestureRecognizer alloc]
                                                            initWithTarget:self 
                                                            action:@selector(handleLongPressIncrease:)];
        longPressIncrease.minimumPressDuration = 0.5;
        [increaseButton addGestureRecognizer:longPressIncrease];
        
        UILongPressGestureRecognizer *longPressDecrease = [[UILongPressGestureRecognizer alloc]
                                                            initWithTarget:self 
                                                            action:@selector(handleLongPressDecrease:)];
        longPressDecrease.minimumPressDuration = 0.5;
        [decreaseButton addGestureRecognizer:longPressDecrease];
    }
    
    /* Label inherit value from display number */
    UILabel *myLabel = [[UILabel alloc] init];
    myLabel.text = label;
    myLabel.frame = CGRectMake(0 , -20, self.frame.size.width, 15);
    myLabel.backgroundColor = self.backgroundColor;
    myLabel.minimumScaleFactor = 2.0;
    myLabel.adjustsFontSizeToFitWidth = YES;
    myLabel.textAlignment = NSTextAlignmentCenter;
    
    //  myLabel.layer.shadowColor = self.layer.shadowColor;
    //	myLabel.layer.shadowOffset = self.layer.shadowOffset;
    //	myLabel.layer.shadowRadius = self.layer.shadowRadius;
    //	myLabel.layer.shadowOpacity = self.layer.shadowOpacity;
    
    myLabel.font = 	self.knob.labelFont;
    myLabel.textColor = self.knob.labelColor;
    
    [self addSubview:myLabel];
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
	return YES;
}


- (void)fineTuneButtonPressed:(id)sender
{
	if (sender == increaseButton)
        knob.value += fineTuneAmount;
    
	else if (sender == decreaseButton) {
        knob.value -= fineTuneAmount;
    }
}

#pragma mark -
#pragma mark Delegate Methods

- (void)controlValueDidChange:(float)value sender:(id)sender
{
    //    NSLog(@"Knob Value: %f", value);
    //    self.value = value;
    [super sendActionsForControlEvents:UIControlEventValueChanged];
    
}

- (void)controlValueDidUp:(float)value sender:(id)sender 
{
    //    self.value = value;
    [super sendActionsForControlEvents:UIControlEventTouchUpInside];
}

- (void)controlValueDidDown:(float)value sender:(id)sender
{
    //    self.value = value;
    [super sendActionsForControlEvents:UIControlEventTouchDown];
}

#pragma mark -
#pragma mark Accessor Methods
-(void) setValue:(float)value_ {
    
    //    value = value_;
    [knob setValue:value_];
}

-(float) getValue {
    //    value = knob.value;
    return knob.value;
}

-(void) setMaximumValue:(float)value_ {
    
    knob.max = value_;
    [knob setNeedsDisplay];
}

-(float) getMaximumValue {
    return knob.max;
}

-(void) setMinimumValue:(float)value_ {
    knob.min = value_;
    [knob setNeedsDisplay];
}

-(float) getMinimumValue {
    return knob.min;
}

-(float) getDefaultValue {
    return self.knob.doubleTapValue;
}

-(void) setDefaultValue:(float)value_ {
    knob.doubleTapValue = value_;
}

#pragma mark -
#pragma handleLongPress Gesture

- (void)handleLongPressIncrease:(UILongPressGestureRecognizer *)gesture {
    
    if(UIGestureRecognizerStateBegan == gesture.state) {
        // Do initial work here
        if(!_timerIncr.isValid)
            _timerIncr = [NSTimer scheduledTimerWithTimeInterval:0.1
                                                          target:self
                                                        selector:@selector(INCREASEfineTuneButtonPressed)
                                                        userInfo:nil
                                                         repeats:YES];
        
    }
    
    //    if(UIGestureRecognizerStateChanged == gesture.state) {
    //        // Do repeated work here (repeats continuously) while finger is down
    //
    //    }
    
    if(UIGestureRecognizerStateEnded == gesture.state) {
        // Do end work here when finger is lifted
        [_timerIncr invalidate];
        _timerIncr = nil;
    }
}

- (void)handleLongPressDecrease:(UILongPressGestureRecognizer *)gesture {
    
    
    if(UIGestureRecognizerStateBegan == gesture.state) {
        // Do initial work here
        
        if(!_timerDecr.isValid)
            _timerDecr = [NSTimer scheduledTimerWithTimeInterval:0.1
                                                          target:self
                                                        selector:@selector(DECREASEfineTuneButtonPressed)
                                                        userInfo:nil
                                                         repeats:YES];
        
    }
    
    if(UIGestureRecognizerStateChanged == gesture.state) {
        // Do repeated work here (repeats continuously) while finger is down
    }
    
    if(UIGestureRecognizerStateEnded == gesture.state) {
        // Do end work here when finger is lifted
        [_timerDecr invalidate];
        _timerDecr = nil;
    }
}

- (void)INCREASEfineTuneButtonPressed
{
    [self performSelector:@selector(fineTuneButtonPressed:) withObject:increaseButton];
    
}

- (void)DECREASEfineTuneButtonPressed
{
    [self performSelector:@selector(fineTuneButtonPressed:) withObject:decreaseButton];   
}

-(void)setEnabled:(BOOL)enable {
    
    [super setEnabled:enable];
    
    if(enable) {
        //        self.knob.color = [UIColor orangeColor];// [self.knob.color colorWithAlphaComponent:1.0];
        self.alpha = 1.; 
    }
    else {
        //        self.knob.color = [UIColor blackColor]; //[self.knob.color colorWithAlphaComponent:0.1];
        self.alpha = 0.4;
    }
}


#pragma mark - Value Cacheable

- (void)setup:(CsoundObj *)csoundObj
{
	channelPtr = [csoundObj getInputChannelPtr:[label lowercaseString] channelType:CSOUND_CONTROL_CHANNEL];
    cachedValue = knob.value;
    self.cacheDirty = YES;
    [self addTarget:self action:@selector(updateValueCache:) forControlEvents:UIControlEventValueChanged];
}

- (void)updateValueCache:(id)sender
{
	cachedValue = ((APEKnob*)sender).getValue;
    self.cacheDirty = YES;
}

- (void)updateValuesToCsound
{
	if (self.cacheDirty) {
        *channelPtr = cachedValue;
        self.cacheDirty = NO;
    }
}

- (void)updateValuesFromCsound
{
	
}

- (void)cleanup
{
	[self removeTarget:self action:@selector(updateValueCache:) forControlEvents:UIControlEventValueChanged];
}


@end
