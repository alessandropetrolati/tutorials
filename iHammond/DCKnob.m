//
//  DCKnob.m
//
//  Copyright 2011 Domestic Cat. All rights reserved.
//  https://github.com/domesticcatsoftware/DCControls
//  All work is under MIT license.

#import "DCKnob.h"

@implementation DCKnob
@synthesize biDirectional, arcStartAngle, cutoutSize, valueArcWidth;
@synthesize doubleTapValue, tripleTapValue;

#pragma mark -
#pragma mark Init

- (id)initWithDelegate:(id)aDelegate
{
    if ((self = [super initWithDelegate:aDelegate]))
    {
        _switch = FALSE;
        _valueTemp = self.value;
        
        self.arcStartAngle = 90.0;
        self.cutoutSize = 60.0;
        self.valueArcWidth = 15.0;
        
        // add the gesture recognizers for double & triple taps
        UITapGestureRecognizer *doubleTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTap:)];
        doubleTapGesture.numberOfTapsRequired = 2;
        [self addGestureRecognizer:doubleTapGesture];
        
        //		UITapGestureRecognizer *tripleTapGesture = [[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tripleTap:)] autorelease];
        //		tripleTapGesture.numberOfTapsRequired = 3;
        //		[self addGestureRecognizer:tripleTapGesture];
    }
    
    return self;
}

// overridden to make sure the frame is always square.
- (void)setFrame:(CGRect)frame
{
    if (frame.size.width != frame.size.height)
    {
        if (frame.size.width > frame.size.height)
            frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, frame.size.width);
        else
            frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.height, frame.size.height);
    }
    
    [super setFrame:frame];
}

#pragma mark -
#pragma mark Gestures

//ape double tap detect
- (void)doubleTap:(UIGestureRecognizer *)gestureRecognizer {
    
    if (!_switch) {
        _valueTemp = self.value;
        //		self.value = _defaultValue;
        [self performSelector:@selector(setValueFromGesture:) withObject:[NSNumber numberWithFloat:self.doubleTapValue]];
        _switch = TRUE;
    }
    else {
        _switch = FALSE;
        //		self.value = _valueTemp;
        [self performSelector:@selector(setValueFromGesture:) withObject:[NSNumber numberWithFloat:_valueTemp]];
        
    }
    
}

//- (void)doubleTap:(UIGestureRecognizer *)gestureRecognizer
//{
//	if (self.allowsGestures)
//	{
////		[self performSelector:@selector(setValueFromGesture:) withObject:[NSNumber numberWithFloat:self.doubleTapValue] afterDelay:0.17];
//        		[self performSelector:@selector(setValueFromGesture:) withObject:[NSNumber numberWithFloat:self.doubleTapValue]];
//	}
//}

- (void)tripleTap:(UIGestureRecognizer *)gestureRecognizer
{
    if (self.allowsGestures)
    {
        // cancel the double tap
        [NSThread cancelPreviousPerformRequestsWithTarget:self selector:@selector(setValueFromGesture:) object:[NSNumber numberWithFloat:self.doubleTapValue]];
        
        [self performSelector:@selector(setValueFromGesture:) withObject:[NSNumber numberWithFloat:self.tripleTapValue]];
    }
}

- (void)setValueFromGesture:(NSNumber *)newValue
{
    self.value = [newValue floatValue];
}

#pragma mark -
#pragma mark Touch Handling

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{   
    CGPoint thisPoint = [[touches anyObject] locationInView:self];
    CGPoint centerPoint = CGPointMake(self.frame.size.width / 2.0, self.frame.size.width / 2.0);
    initialAngle = angleBetweenPoints(thisPoint, centerPoint);
    
    // create the initial angle and initial transform
    initialTransform = [self initialTransform];
    
    [self.delegate controlValueDidDown:self.value sender:self];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{   
    //ape double tap detect
    _switch = FALSE;
    
    CGPoint thisPoint = [[touches anyObject] locationInView:self];
    CGPoint centerPoint = CGPointMake(self.frame.size.width / 2.0, self.frame.size.width / 2.0);
    
    CGFloat currentAngle = angleBetweenPoints(thisPoint, centerPoint);
    CGFloat angleDiff = (initialAngle - currentAngle);
    CGAffineTransform newTransform = CGAffineTransformRotate(initialTransform, angleDiff);
    
    CGFloat newValue = [self newValueFromTransform:newTransform];
    
    // only set the new value if it doesn't flip the knob around
    CGFloat diff = self.value - newValue;
    diff = (diff < 0) ? -diff : diff;
    if (diff < (self.max - self.min) / 10.0)
    {
        self.value = newValue;
    }
    else
    {
        // reset the initial angle & transform using the current value
        initialTransform = [self initialTransform];
        initialAngle = angleBetweenPoints(thisPoint, centerPoint);
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.delegate controlValueDidUp:self.value sender:self];
}

#pragma mark -
#pragma mark Helper Methods

- (CGAffineTransform)initialTransform
{
    CGFloat pValue = (self.value - self.min) / (self.max - self.min);
    pValue = (pValue * kDCKnobRatio * 2) - kDCKnobRatio;
    return CGAffineTransformMakeRotation(pValue);
}

- (CGFloat)newValueFromTransform:(CGAffineTransform)transform
{
    CGFloat newValue = atan2(transform.b, transform.a);
    newValue = (newValue + kDCKnobRatio) / (kDCKnobRatio * 2);
    newValue = self.min + (newValue * (self.max - self.min));
    return newValue;
}

#pragma mark -
#pragma mark Drawing

- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGRect boundsRect = self.bounds;
    CGFloat maxHalf = self.min + (self.max - self.min) / 2;
    float x = boundsRect.size.width / 2;
    float y = boundsRect.size.height / 2;
    
    CGContextSaveGState(context);
    CGContextSetLineWidth(context, self.valueArcWidth);
    
    if (self.backgroundColorAlpha > 0.02)
    {
        // outline semi circle
        //		const CGFloat *colorComponents = CGColorGetComponents(self.color.CGColor);
        //		UIColor *backgroundColor = [UIColor colorWithRed:colorComponents[0]
        //												   green:colorComponents[1]
        //													blue:colorComponents[2]
        //												   alpha:self.backgroundColorAlpha];
        // ape's code
        UIColor *backgroundColor = [BACKGROUND_WAVE_UICOLOR colorWithAlphaComponent:self.backgroundColorAlpha];
        
        [backgroundColor set];
        
        CGContextAddArc(context,
                        x,
                        y,
                        (boundsRect.size.width / 2) - self.valueArcWidth / 2,
                        kDCControlDegreesToRadians(self.arcStartAngle + self.cutoutSize / 2),
                        kDCControlDegreesToRadians(self.arcStartAngle + 360 - self.cutoutSize / 2),
                        0);
        CGContextStrokePath(context);
    }
    
    // draw the value semi circle
    [self.color set];
    CGFloat valueAdjusted = (self.value - self.min) / (self.max - self.min);
    if (self.biDirectional)
    {
        CGFloat apeDegreeOffSet;
        //        CGFloat range = fabsf(self.min) + self.max;
        //        CGFloat negativeRange = fabsf(self.min) / range;
        //        CGFloat positiveRange = self.max / range;
        //
        //        if (fabsf(self.min) < self.max)
        //            apeDegreeOffSet = 180.f - (negativeRange * 180.f);
        //        else
        //            apeDegreeOffSet = 180.f + (positiveRange * 180.f);
        //        if(fabsf(self.min) == self.max)
        //            apeDegreeOffSet = 180.f;
        //        maxHalf = 0.f;
        
        apeDegreeOffSet = 180; //comment this if intend to use above code (does not work properly yet!) ape 03.02.2012
        
        CGContextAddArc(context,
                        x,
                        y,
                        (boundsRect.size.width / 2.f) - self.valueArcWidth / 2.f,
                        kDCControlDegreesToRadians(self.arcStartAngle + apeDegreeOffSet),
                        kDCControlDegreesToRadians(self.arcStartAngle + self.cutoutSize / 2.f + (360.f - self.cutoutSize) * valueAdjusted),
                        self.value <= maxHalf);
        
    }
    else
    {
        CGContextAddArc(context,
                        x,
                        y,
                        (boundsRect.size.width / 2) - self.valueArcWidth / 2,
                        kDCControlDegreesToRadians(self.arcStartAngle + self.cutoutSize / 2),
                        kDCControlDegreesToRadians(self.arcStartAngle + self.cutoutSize / 2 + (360 - self.cutoutSize) * valueAdjusted),
                        0);
    }
    CGContextStrokePath(context);
    
    // draw the value string as needed
    if (self.displaysValue)
    {
        if (self.labelColor)
            [self.labelColor set];
        else
            [self.color set];
        NSString *valueString = nil;
        if (self.biDirectional) {
            //			valueString = [NSString stringWithFormat:@"%02.0f%%", ((self.value - self.min - (self.max - self.min) / 2) / (self.max - self.min)) * 100]; //ape comment
            valueString = [NSString stringWithFormat:@"%4.3f", self.value];
        } else {
            //			valueString = [NSString stringWithFormat:@"%03.0f%%", ((self.value - self.min) / (self.max - self.min)) * 100];  //ape comment
            valueString = [NSString stringWithFormat:@"%4.3f", self.value];
        }
        
        CGSize valueStringSize = [valueString sizeWithAttributes:@{NSFontAttributeName:self.labelFont}];
                
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle defaultParagraphStyle] mutableCopy];
        paragraphStyle.lineBreakMode = NSLineBreakByTruncatingTail;
        [valueString drawInRect:CGRectMake(floorf((boundsRect.size.width - valueStringSize.width) / 2.0 + self.labelOffset.x),
                                           floorf((boundsRect.size.height - valueStringSize.height) / 2.0 + self.labelOffset.y),
                                           valueStringSize.width,
                                           valueStringSize.height)
                 withAttributes: @{NSFontAttributeName:self.labelFont, NSParagraphStyleAttributeName: paragraphStyle }];
    }
    
    CGContextRestoreGState(context);
}
@end
