//
//  MainView.h
//  apeSoft
//
//  Created by Alessandro Petrolati on 15/02/12.
//  Copyright (c) 2012 apeSoft. All rights reserved.
//

#define BACKGROUND_WAVE_UICOLOR [UIColor colorWithRed:227.f/255.f green:227.f/255.f blue:227.f/255.f alpha:1.f]
#define BACKGROUND_PARAMS_UICOLOR [UIColor colorWithRed:28.f/255.f green:28.f/255.f blue:28.f/255.f alpha:1.f]
#define BACKGROUND_MINIMUMTRACK_UICOLOR [UIColor colorWithRed:58.f/255.f green:58.f/255.f blue:58.f/255.f alpha:1.f]

#import <UIKit/UIKit.h>

@interface MainView : UIView

@end
