//
//  ViewController.m
//  apeSoft
//
//  Created by Alessandro Petrolati on 16/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ViewController.h"
#import "APESlider.h"
#import "CsoundUI.h"

@implementation ViewController
@synthesize currentMessage = mCurrentMessage;
@synthesize csound = mCsound;


-(void)viewDidLoad {
    
    widgetsManager = [[MidiWidgetsManager alloc] init];
    
    [widgetsManager openMidiIn];
    
    /* Auto start when open app */
    [self toggleOnOff:YES];
    
    [super viewDidLoad];
}


-(void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    [widgetsManager closeMidiIn];
    
    [mCsound stop];
}

-(void)toggleOnOff:(BOOL)state {
    
    if(state) {
        
        NSString *csdFile = [[NSBundle mainBundle] pathForResource:@"jmc.hammond" ofType:@"csd"];
        NSLog(@"FILE PATH: %@", csdFile);
        
        [self.csound stop];
        
        self.csound = [[CsoundObj alloc] init];
        [self.csound addListener:self];
        
        //Console Out
        [self.csound setMessageCallback:@selector(messageCallback:) withListener:self];
        
        [self.csound addBinding:drawbar_1_low];
        [self.csound addBinding:drawbar_2_low];
        [self.csound addBinding:drawbar_3_low];
        [self.csound addBinding:drawbar_4_low];
        [self.csound addBinding:drawbar_5_low];
        [self.csound addBinding:drawbar_6_low];
        [self.csound addBinding:drawbar_7_low];
        [self.csound addBinding:drawbar_8_low];
        [self.csound addBinding:drawbar_9_low];
        
        [self.csound addBinding:drawbar_1_hi];
        [self.csound addBinding:drawbar_2_hi];
        [self.csound addBinding:drawbar_3_hi];
        [self.csound addBinding:drawbar_4_hi];
        [self.csound addBinding:drawbar_5_hi];
        [self.csound addBinding:drawbar_6_hi];
        [self.csound addBinding:drawbar_7_hi];
        [self.csound addBinding:drawbar_8_hi];
        [self.csound addBinding:drawbar_9_hi];
        
        //Effects returns
        [self.csound addBinding:PreEff];
        [self.csound addBinding:Scan];
        [self.csound addBinding:Dist];
        [self.csound addBinding:Rot];
        [self.csound addBinding:Del];
        [self.csound addBinding:Rev];
        
        [self.csound addBinding:SlowFastRot];
        [self.csound addBinding:BootRev];
        
        //Generators
        [self.csound addBinding:MasterTune];
        [self.csound addBinding:DetL];
        [self.csound addBinding:Mix];
        
        //Main instrument controls
        [self.csound addBinding:MainVol];
        
        //Percussion Low
        [self.csound addBinding:AttackL];
        [self.csound addBinding:LeakL];
        [self.csound addBinding:LevelL];
        [self.csound addBinding:DecL];
        [self.csound addBinding:PercL];
        
        //Percussion High
        [self.csound addBinding:AttackH];
        [self.csound addBinding:LeakH];
        [self.csound addBinding:LevelH];
        [self.csound addBinding:DecH];
        [self.csound addBinding:PercH];
        
        CsoundUI *csoundUI = [[CsoundUI alloc] initWithCsoundObj:self.csound];
        
        [csoundUI addSlider:orderL forChannelName:@"OrderL"];
        [csoundUI addSlider:orderH forChannelName:@"OrderH"];
        
        [self.csound setMidiInEnabled:YES];
        [self.csound play:csdFile];
    }
    else {
        [self.csound stop];
        //remove Binding here
    }
}


#pragma mark CsoundObjCompletionListener

-(void)csoundObjDidStart:(CsoundObj *)csoundObj {
    
    [drawbar_1_low refreshValue];
    [drawbar_2_low refreshValue];
    [drawbar_3_low refreshValue];
    [drawbar_4_low refreshValue];
    [drawbar_5_low refreshValue];
    [drawbar_6_low refreshValue];
    [drawbar_7_low refreshValue];
    [drawbar_8_low refreshValue];
    [drawbar_9_low refreshValue];
    
    [drawbar_1_hi refreshValue];
    [drawbar_2_hi refreshValue];
    [drawbar_3_hi refreshValue];
    [drawbar_4_hi refreshValue];
    [drawbar_5_hi refreshValue];
    [drawbar_6_hi refreshValue];
    [drawbar_7_hi refreshValue];
    [drawbar_8_hi refreshValue];
    [drawbar_9_hi refreshValue];
}

-(void)csoundObjComplete:(CsoundObj *)csoundObj {
    
}

#pragma mark CsoundVirtualKeyboardDelegate

-(void)keyUp:(CsoundVirtualKeyboard*)keybd keyNum:(int)keyNum {
    int midikey = 48 + keyNum;
    [mCsound sendScore:[NSString stringWithFormat:@"i-1.%003d 0 0 1", midikey]];
}

-(void)keyDown:(CsoundVirtualKeyboard*)keybd keyNum:(int)keyNum {
    
    int midikey = 48 + keyNum;
    [mCsound sendScore:[NSString stringWithFormat:@"i1.%003d 0 -2 %d 0 1", midikey, midikey]];
}

#pragma mark Console methods
- (void)updateUIWithNewMessage:(NSString *)newMessage
{
    NSLog(@"%@", newMessage);
}

- (void)messageCallback:(NSValue *)infoObj
{
    @autoreleasepool {
        Message info;
        [infoObj getValue:&info];
        char message[1024];
        vsnprintf(message, 1024, info.format, info.valist);
        NSString *messageStr = [NSString stringWithFormat:@"%s", message];
        [self performSelectorOnMainThread:@selector(updateUIWithNewMessage:)
                               withObject:messageStr
                            waitUntilDone:NO];
    }
}

#pragma mark Show About
-(IBAction) showAbout:(id)sender {
    
    UIViewController* popoverController = [[UIViewController alloc] init];
    popoverController.view = aboutView;
    popoverController.preferredContentSize = aboutView.frame.size;
    
    [self presentViewController:popoverController animated:YES completion:nil];
}

-(IBAction) closeAbout:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
