//
//  ViewController.h
//  apeSoft
//
//  Created by Alessandro Petrolati on 16/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MIDIWidgetsManager.h"
#import "CsoundObj.h"
#import "CsoundVirtualKeyboard.h"
#import "APEKnob.h"
#import "APESlider.h"
#import "UICustomSliderSwitch.h"


@interface ViewController : UIViewController <CsoundObjListener,
CsoundVirtualKeyboardDelegate> {
    
    CsoundObj* mCsound;
    MidiWidgetsManager* widgetsManager;
    
    //DrawBars low
    IBOutlet APESlider* drawbar_1_low;
    IBOutlet APESlider* drawbar_2_low;
    IBOutlet APESlider* drawbar_3_low;
    IBOutlet APESlider* drawbar_4_low;
    IBOutlet APESlider* drawbar_5_low;
    IBOutlet APESlider* drawbar_6_low;
    IBOutlet APESlider* drawbar_7_low;
    IBOutlet APESlider* drawbar_8_low;
    IBOutlet APESlider* drawbar_9_low;
    
    //DrawBars hi
    IBOutlet APESlider* drawbar_1_hi;
    IBOutlet APESlider* drawbar_2_hi;
    IBOutlet APESlider* drawbar_3_hi;
    IBOutlet APESlider* drawbar_4_hi;
    IBOutlet APESlider* drawbar_5_hi;
    IBOutlet APESlider* drawbar_6_hi;
    IBOutlet APESlider* drawbar_7_hi;
    IBOutlet APESlider* drawbar_8_hi;
    IBOutlet APESlider* drawbar_9_hi;
    
    //Effects returns
    IBOutlet APEKnob *PreEff;
    IBOutlet APEKnob *Scan;
    IBOutlet APEKnob *Dist;
    IBOutlet APEKnob *Rot;
    IBOutlet APEKnob *Del;
    IBOutlet APEKnob *Rev;
    
    IBOutlet APEKnob *SlowFastRot;
    IBOutlet APEKnob *BootRev;
    
    //Generators
    IBOutlet APEKnob *MasterTune;
    IBOutlet APEKnob *DetL;
    IBOutlet APEKnob *Mix;
    
    //Main instrument controls
    IBOutlet APEKnob *MainVol;
    
    
    //Percussion Low
    IBOutlet APEKnob *AttackL;
    IBOutlet APEKnob *LeakL;
    IBOutlet APEKnob *LevelL;
    IBOutlet APEKnob *DecL;
    IBOutlet APEKnob *PercL;
    
    //Percussion High
    IBOutlet APEKnob *AttackH;
    IBOutlet APEKnob *LeakH;
    IBOutlet APEKnob *LevelH;
    IBOutlet APEKnob *DecH;
    IBOutlet APEKnob *PercH;
    
    //console
    //    IBOutlet UITextView *mTextView;
    
    IBOutlet UICustomSliderSwitch* orderH;
    IBOutlet UICustomSliderSwitch *orderL;
    
    // UIviews
    IBOutlet UIView *aboutView;
}
//console
@property (nonatomic, strong) NSString *currentMessage;
@property (nonatomic, strong) CsoundObj* csound;

@end
