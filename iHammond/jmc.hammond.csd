<CsoundSynthesizer>
<CsOptions>
-o dac -+rtmidi=null -+rtaudio=null -d -+msg_color=0 -M0 -m0
</CsOptions>
<CsInstruments>
/*
DirectHammond v2 by Josep M Comajuncosas
Barcelona, May/June 2001
This is an optimized version of DirectHammond
(it doubles the polyphony in realtime,
about 8 notes on my P3/1000, same sound quality)

Alessandro Petrolati iOs integration 2012
*/

	sr 	= 44100
	ksmps 	= 64
	nchnls 	= 2
;do not uncomment if you want save the loudspeaker
;0dbfs = 1


opcode cpsmid, k, k

kmid 	xin	

#define	MIDI2CPS(xmidi) # (440.0*exp(log(2.0)*(($xmidi)-69.0)/12.0)) # 
kcps 	=	$MIDI2CPS(kmid) 

xout	kcps 

endop


;this is optional (to make sure DirecHammond plays everything...)
massign	1,1 
massign	2,1 
massign	3,1 
massign	4,1 
massign	5,1 
massign	6,1 
massign	7,1 
massign	8,1 
massign	9,1 
massign	10,1 
massign	11,1 
massign	12,1 
massign	13,1 
massign	14,1 
massign	15,1 
massign	16,1 

maxalloc	1,0 ;maximum polyphony (0 means unlimited) 
prealloc	1,16 ;preallocate 16 voices (your expected max. polyphony) 
maxalloc	2,1 ;global effects : only 1 instance of them 
maxalloc	8,1 ;autobalance : if you press the button MAX AMP twice only the last remains active 


;		gkL_16ft_lvl init 0.5 ;,   ih00 FLslider "16'"    ,0,1,0,2,-1,17,150,15,30
;		gkL_5_13ft_lvl init 0.5 ;, ih02 FLslider "5 1/3'" ,0,1,0,2,-1,17,150,35,30
;		gkL_8ft_lvl init 0.5 ;,    ih01 FLslider "8'"     ,0,1,0,2,-1,17,150,55,30
;		gkL_4ft_lvl init 0.5 ;,    ih03 FLslider "4'"     ,0,1,0,2,-1,17,150,75,30
;		gkL_2_23ft_lvl init 0.5 ;, ih04 FLslider "2 2/3'" ,0,1,0,2,-1,17,150,95,30
;		gkL_2ft_lvl init 0.5 ;,    ih05 FLslider "2'"     ,0,1,0,2,-1,17,150,115,30
;		gkL_1_35ft_lvl init 0.5 ;, ih06 FLslider "1 3/5'" ,0,1,0,2,-1,17,150,135,30
;		gkL_1_23ft_lvl init 0.5 ;, ih07 FLslider "1 1/3'" ,0,1,0,2,-1,17,150,155,30
;		gkL_1ft_lvl init 0.5 ;,    ih08 FLslider "1'"     ,0,1,0,2,-1,17,150,175,30



gkOct_Lo 	init	1 ;, ih09 FLtext "Oct", 0, 3, 1, 1, 20,20, 15,192 
;gkL_perc_Harm  init 2 ;, ih14 FLtext  "2nd/3rd", 2, 3, 1, 1, 24,24, 268,95

;gk_L_Att init 0.1 ;, ih10 FLknob  "Attack", 0.001, 10, -1, 1, -1, 40, 210, 30
;gkL_leak init 0.05 ;, ih11 FLknob  "Leak"  , 0,    0.1,  0, 1, -1, 40, 210, 85
;gkL_lvl init 0.5 ;,  ih12 FLknob  "Level" , 0,    1,    0, 1, -1, 40, 210, 140
;gk_L_perc_Dec init 0.5 ;, ih13 FLknob  "Dec" , 0.01, 10, -1, 1, -1, 40, 210+50, 30
;gkL_perc_lvl init 0.5 ;,  ih15 FLknob  "Perc L" , 0,    1,    0, 1, -1, 40, 210+50, 140


	
;		gkH_16ft_lvl init 0.5 ;,   ih20 FLslider "16'"    ,0,1,0,2,-1,17,150,15+320,30
;		gkH_5_13ft_lvl init 0.5 ;, ih22 FLslider "5 1/3'" ,0,1,0,2,-1,17,150,35+320,30
;		gkH_8ft_lvl init 0.5 ;,    ih21 FLslider "8'"     ,0,1,0,2,-1,17,150,55+320,30
;		gkH_4ft_lvl init 0.5 ;,    ih23 FLslider "4'"     ,0,1,0,2,-1,17,150,75+320,30
;		gkH_2_23ft_lvl init 0.5 ;, ih24 FLslider "2 2/3'" ,0,1,0,2,-1,17,150,95+320,30
;		gkH_2ft_lvl init 0.5 ;,    ih25 FLslider "2'"     ,0,1,0,2,-1,17,150,115+320,30
;		gkH_1_35ft_lvl init 0.5 ;, ih26 FLslider "1 3/5'" ,0,1,0,2,-1,17,150,135+320,30
;		gkH_1_23ft_lvl init 0.5 ;, ih27 FLslider "1 1/3'" ,0,1,0,2,-1,17,150,155+320,30
;		gkH_1ft_lvl init 0.5 ;,    ih28 FLslider "1'"     ,0,1,0,2,-1,17,150,175+320,30


gkOct_Hi 	init	1 ;, ih29 FLtext "Oct", 0, 3, 1, 1, 20,20, 172+320,192 
;gkH_perc_Harm init 2 ;, ih34 FLtext  "2nd/3rd", 2, 3, 1, 1, 24,24, 268+320,95

;gk_H_Att init 0.1 ;, ih30 FLknob  "Attack", 0.001, 10, -1, 1, -1, 40, 210+320, 30
;gkH_leak init 0.05 ;, ih31 FLknob  "Leak"  , 0,    0.1,  0, 1, -1, 40, 210+320, 85
;gkH_lvl init 0.5 ;,  ih32 FLknob  "Level" , 0,    1,    0, 1, -1, 40, 210+320, 140
;gk_H_perc_Dec init 0.5 ;, ih33 FLknob  "Dec" , 0.01, 10, -1, 1, -1, 40, 210+320+50, 30
;gkH_perc_lvl init 0.5 ;,  ih35 FLknob  "Perc L" , 0,    1,    0, 1, -1, 40, 210+320+50, 140


;Generators
;	gkTune init 1 ;, ihgen3   FLknob  "Master Tune",   0.944, 1.059, 0, 1, -1, 40, 20, 230;+/- 1 ET semitone
;	gkDetl init 0.995 ;, ihgen1   FLknob  "Det L",   0.99, 1.01, 0, 1, -1, 40, 70, 230
;	gkDet_L1 init 0.5 ;, ihgen2 FLknob  "Mix", 0,    1,    0, 1, -1, 40, 120, 230


;Allocated voices
;feature removed
;giVoices  FLvalue "Voices", 30, 20, 220+320, 220


;Autoadjust volume

gkrms 	init	0 ;,ihrms FLbutton "Auto max", 1,0, 11,50, 20, 230+320+40, 280, 0,8,0,3 

;Main instrument controls
gkSplit 	init	60 ;, ihsplit FLtext "Split", 0, 128, 1, 1, 35, 20, 170+320, 260 
;	gkMain_Vol init 0.75 ;,    ihmain2 FLknob  "Main Vol",     0, 1,  0, 1, -1, 50, 230+320+40, 210


;Effect returns

;	gkMain_PreEff init 0.995 ;, ihmain1   FLknob  "PreEff", 0.001, 1, -1, 1, -1, 40, 190, 220
;	gkfx_Scan init 0.3 ;, ihfx0   FLknob  "Scan",   0, 1, 0, 1, -1, 40, 240, 220
;	gkfx_Rot init 0.5 ;,  ihfx1   FLknob  "Rot",    0, 1, 0, 1, -1, 40, 330 , 220
;	gkfx_Rev init 0.2 ;,  ihfx2   FLknob  "Rev",    0, 1, 0, 1, -1, 40, 420 , 220
;	gkfx_Dist init 0.65 ;, ihfx3   FLknob  "Dist",   0, 1, 0, 1, -1, 40, 285 , 220
;	gkfx_Del init 0.15 ;,  ihfx4   FLknob  "Del",    0, 1, 0, 1, -1, 40, 375, 220


;	gkSlowFast init 0.52 ;, ih130 FLslider  "Slow/Fast Rot", 0, 1, 0, 1, -1, 110,15, 200, 275
;	gkFlanger_dlt init 0.52 ;, ihldlt FLslider  "Boot rev (move fast!)", 0.1, 1, -1, 1, -1, 110,15, 340, 275



gkDist_Drive 	init	0.1 ;, ihdist1 FLknob "Drive", .01, 20, -1, 1, -1, 40, 216, 47 
gkDist_Cutoff 	init	1000 ;, ihdist2 FLknob "Cutoff", 250, 4000, -1, 1, -1, 40, 261, 47 
gkSoft_Drive 	init	0.125 ;, ihdist3 FLknob "Soft", 0.0625, 0.25, -1, 1, -1, 40, 172, 47 
gkAsymm 	init	0.1 ;, ihdist4 FLknob "Asymm", 0.01, 0.99, 0, 1, -1, 40, 192, 102 
gkDist_postGain 	init	1 ;,ihdist6 FLknob "postGain", .125, 8, -1, 1, -1, 40, 241, 102 


gkFlanger_dlt 	init	0.3 ;, ihflang1 FLknob "Delay", 0.1, 1, -1, 1, -1, 40, 321, 47 
gkFlanger_fdbk 	init	-0.5 ;, ihflang2 FLknob "Fdbk", -.999, .999,0, 1, -1, 40, 321, 102 


gkRvb_time 	init	1.5 ;, ihrv0 FLknob "Size", 0.05, 10, 0, 1, -1, 40, 371, 47 
gkRvb_Attn 	init	0.2 ;, ihrv1 FLknob "frq Att", 0, 1, 0, 1, -1, 40, 371, 102 


gkSc_C 	init	0.9 ;, ihsc1 FLknob "C", -1, 1, -1, 1, -1, 40, 22, 47 
gkSc_Acc 	init	1.2 ;, ihsc2 FLknob "Acc", 0.01, 10, -1, 1, -1,40 , 67, 47 
gkSc_Fst 	init	2.3 ;, ihsc3 FLknob "Fst", 0.1, 5, -1, 1, -1, 40, 112, 47 
gkSc_Slw 	init	0.995 ;, ihsc4 FLknob "Slw", 0.1, 5, -1, 1, -1, 40, 22, 102 
gkSc_Depth 	init	0.05 ;, ihsc5 FLknob "Depth", 0, 0.1, 0, 1, -1,40 , 67, 102 
gkSc_Delay 	init	25 ;, ihsc6 FLknob "Delay", 20, 30, -1, 1, -1, 40, 112, 102 


;Autoadjust volume

gkrms 	init	1 ;,ihrms FLbutton "Auto max", 1,0, 11,50, 20, 440, 130, 0,8,0,3 

;Leslie
gkL_Fst 	init	7.43 ;, ih131 FLknob "L Fast" , 0.1, 15, 0, 1, -1, 40, 67, 236 
gkL_Slw 	init	0.16 ;, ih132 FLknob "L Slow" , 0.1, 15, 0, 1, -1, 40, 22, 236 
gkH_Fst 	init	9.73 ;, ih133 FLknob "H Fast" , 0.2, 16, 0, 1, -1, 40, 67, 181 
gkH_Slw 	init	0.33 ;, ih134 FLknob "H Slow" , 0.2, 16, 0, 1, -1, 40, 22, 181 
gkL_Acc 	init	0.11 ;, ih135 FLknob "L Acc" , 0.01, 10, 0, 1, -1, 40, 112, 236 
gkH_Acc 	init	0.10 ;, ih136 FLknob "H Acc" , 0.01, 10, 0, 1, -1, 40, 112, 181 

gkFq_Sep 	init	1638 ;, ih137 FLknob "Fq Sep" , 100, 2000, -1, 1, -1, 40, 177, 181 
gkaM 	init	1.72 ;, ih138 FLknob "aM" , 0, 2, 0, 1, -1, 40, 177, 236 
gkL_Att2 	init	0.27 ;, ih139 FLknob "L Att" , 0, 1, 0, 1, -1, 40, 232, 236 
gkH_Att2 	init	0.21 ;, ih140 FLknob "H Att" , 0, 1, 0, 1, -1, 40, 232, 181 

gkHCut 	init	5153 ;, ih141 FLknob "HCut" , 20, 6000, -1, 1, -1, 40, 297, 181 
gkHQ 	init	0.86 ;, ih142 FLknob "HQ" , 0.001, 1, -1, 1, -1, 40, 342, 181 
gkLCut 	init	2599 ;, ih143 FLknob "LCut" , 20, 6000, -1, 1, -1, 40, 297, 236 
gkLQ 	init	0.87 ;, ih144 FLknob "LQ" , 0.001, 1, -1, 1, -1, 40, 342, 236 
gkLow 	init	0.92 ;, ih145 FLknob "Low" , 0, 1, 0, 1, -1, 40, 387, 236 
gkHigh 	init	1 ;, ih146 FLknob "High" , 0, 1, 0, 1, -1, 40, 387, 181 

gkL_Doppler 	init	0.17 ;, ih147 FLknob "LDoppler" , 0, 1,0, 1, -1, 40, 452, 236 
gkH_Doppler 	init	0.23 ;, ih148 FLknob "HDoppler" , 0, 1,0, 1, -1, 40, 452, 181 



instr 99 ;sampling from iOs

; -------------------------------------------------------
;NB Input channels for the ape.knobs must be lowcase!!!
; Sampling UI parameters

;Drawbars low
	gkL_16ft_lvl chnget "drawbar_1_low"
	gkL_5_13ft_lvl chnget "drawbar_2_low"
	gkL_8ft_lvl chnget "drawbar_3_low"
	gkL_4ft_lvl chnget "drawbar_4_low"
	gkL_2_23ft_lvl chnget "drawbar_5_low"
	gkL_2ft_lvl chnget "drawbar_6_low"
	gkL_1_35ft_lvl chnget "drawbar_7_low"
	gkL_1_23ft_lvl chnget "drawbar_8_low"
	gkL_1ft_lvl chnget "drawbar_9_low"

;Drawbars hight
	gkH_16ft_lvl chnget "drawbar_1_hi"
	gkH_5_13ft_lvl chnget "drawbar_2_hi"
	gkH_8ft_lvl chnget "drawbar_3_hi"
	gkH_4ft_lvl chnget "drawbar_4_hi"
	gkH_2_23ft_lvl chnget "drawbar_5_hi"
	gkH_2ft_lvl chnget "drawbar_6_hi"
	gkH_1_35ft_lvl chnget "drawbar_7_hi"
	gkH_1_23ft_lvl chnget "drawbar_8_hi"
	gkH_1ft_lvl chnget "drawbar_9_hi"


;Percussion L
;gkOct_Lo init 1 ;, ih09 FLtext  "Oct", 0, 3, 1, 1, 20,20, 15,192

	korderL chnget "OrderL"
	gkL_perc_Harm = korderL + 2

	gk_L_Att chnget "attackl"
	gkL_leak chnget "leakl"
	gkL_lvl chnget "levell"
	gk_L_perc_Dec chnget "decl"
	gkL_perc_lvl chnget "percl"

;Percussion H
;gkOct_Hi init 1 ;, ih29 FLtext  "Oct", 0, 3, 1, 1, 20,20, 172+320,192

	korderH chnget "OrderH"
	gkH_perc_Harm = korderH + 2


	gk_H_Att chnget "attackh"
	gkH_leak chnget "leakh"
	gkH_lvl chnget "levelh"
	gk_H_perc_Dec chnget "dech"
	gkH_perc_lvl chnget "perch"


;Effects Returns
	gkMain_PreEff chnget "preeff"
	gkfx_Scan chnget "scan"
	gkfx_Dist chnget "dist"
	gkfx_Rot chnget "rot"
	gkfx_Rev chnget "rev"
	gkfx_Del chnget "del"

	gkSlowFast chnget "SlowFastRot"
	gkFlanger_dlt chnget "BootRev"

;Generators
	gkTune chnget "mastertune"
	gkDetl chnget "detl"
	gkDet_L1 chnget "mix"


;Main instrument controls
;gkSplit init 0 ;, ihsplit FLtext  "Split", 0, 128, 1, 1, 35, 20, 170+320, 260
	gkMain_Vol chnget "mainvol"

; --------------------------------------------------------------------

endin

instr 1
;Get and display current number of allocated voices
;inum	active	1

	gasig init 0


; MIDI In hardware
if(p6 == 0) then
	ikey notnum
	ifreq cpsmidi

else
	; MIDI From iPad UI because called on p6 TRUE
	ikey = p4
	ifreq = cpsmidinn(p4)

endif


p6 = 0

; MIDI test example does't work properly
;kfreq_ cpsmid p4 ;ape
;ifreq = i(kfreq_)


	kOct_Hi = 2^(gkOct_Hi-1)
	kOct_Lo = 2^(gkOct_Lo-1)

	kfreq0 = ifreq*gkTune
	kfreq1 = kfreq0*kOct_Hi
	kfreq2 = kfreq0*gkDetl*kOct_Lo

;GENERATORS

;tonewheels
	a01 oscil 1,kfreq1,1
	a02 oscil 1,2*kfreq1,1
	a03 oscil 1,3*kfreq1,1
	a04 oscil 1,4*kfreq1,1
	a06 oscil 1,6*kfreq1,1
	a08 oscil 1,8*kfreq1,1
	a10 oscil 1,10*kfreq1,1
	a12 oscil 1,12*kfreq1,1
	a16 oscil 1,16*kfreq1,1

	ad01 oscil 1,kfreq2,1
	ad02 oscil 1,2*kfreq2,1
	ad03 oscil 1,3*kfreq2,1
	ad04 oscil 1,4*kfreq2,1
	ad06 oscil 1,6*kfreq2,1
	ad08 oscil 1,8*kfreq2,1
	ad10 oscil 1,10*kfreq2,1
	ad12 oscil 1,12*kfreq2,1
	ad16 oscil 1,16*kfreq2,1

;add both tonewheel arrays
	a16ft sum a01,ad01
	a8ft sum a02,ad02
	a5_13ft sum a03,ad03
	a4ft sum a04,ad04
	a2_23ft sum a06,ad06
	a2ft sum a08,ad08
	a1_35ft sum a10,ad10
	a1_23ft sum a12,ad12
	a1ft sum a16,ad16

	aftSum sum a16ft,a8ft,a5_13ft,a4ft,a2_23ft,a2ft,a1_35ft,a1_23ft,a1ft

	aNull init 0;I know ...but it seems a necessary trick in conditionals

;DRAWBARS HIGH

	aH_16ft mac gkH_16ft_lvl ,(a16ft)
	aH_8ft mac gkH_8ft_lvl ,(a8ft)
	aH_5_13ft mac gkH_5_13ft_lvl ,(a5_13ft)
	aH_4ft mac gkH_4ft_lvl ,(a4ft)
	aH_2_23ft mac gkH_2_23ft_lvl ,(a2_23ft)
	aH_2ft mac gkH_2ft_lvl ,(a2ft)
	aH_1_35ft mac gkH_1_35ft_lvl ,(a1_35ft)
	aH_1_23ft mac gkH_1_23ft_lvl ,(a1_23ft)
	aH_1ft mac gkH_1ft_lvl ,(a1ft)

	aH_ftSum sum aH_16ft,aH_8ft,aH_5_13ft,aH_4ft,aH_2_23ft,aH_2ft,aH_1_35ft,aH_1_23ft,aH_1ft
	aH_env madsr i(gk_H_Att),0,1,0.01
	aH_env = (gkSplit > ikey ? aNull : aH_env)
	kH_perc_Harm = gkH_perc_Harm

	aH_perc = (kH_perc_Harm > 2 ? a2_23ft : a4ft)
	aH_perc_env madsr 0.01,i(gk_H_perc_Dec),0,0.01
	aH_perc_env = (gkSplit > ikey ? aNull : aH_perc_env)
	aH_ftSumOut = (aH_ftSum + gkH_leak*aftSum)*aH_env*gkH_lvl + (gkH_perc_lvl*aH_perc)*aH_perc_env


;DRAWBARS LOW

	aL_16ft mac gkL_16ft_lvl ,(a16ft)
	aL_8ft mac gkL_8ft_lvl ,(a8ft)
	aL_5_13ft mac gkL_5_13ft_lvl ,(a5_13ft)
	aL_4ft mac gkL_4ft_lvl ,(a4ft)
	aL_2_23ft mac gkL_2_23ft_lvl ,(a2_23ft)
	aL_2ft mac gkL_2ft_lvl ,(a2ft)
	aL_1_35ft mac gkL_1_35ft_lvl ,(a1_35ft)
	aL_1_23ft mac gkL_1_23ft_lvl ,(a1_23ft)
	aL_1ft mac gkL_1ft_lvl ,(a1ft)


	aL_ftSum sum aL_16ft,aL_8ft,aL_5_13ft,aL_4ft,aL_2_23ft,aL_2ft,aL_1_35ft,aL_1_23ft,aL_1ft
	aL_env madsr i(gk_L_Att),0,1,0.01
	aL_env = (gkSplit>ikey?aL_env:aNull)
	kL_perc_Harm = gkL_perc_Harm

	aL_perc = (kL_perc_Harm>2?a2_23ft:a4ft)
	aL_perc_env madsr 0.01,i(gk_L_perc_Dec),0,0.01
	aL_percEnv = (gkSplit>ikey?aL_perc_env:aNull)
	aL_ftSumOut = (aL_ftSum + gkL_leak*aftSum)*aL_env*gkL_lvl + (gkL_perc_lvl*aL_perc)*aL_perc_env


;mix&output
	aout sum aL_ftSumOut,aH_ftSumOut
	gasig = gkMain_PreEff*aout + gasig
endin


instr 2
;Get and display preset number
;FLprintk2  gkGet, giPreset_n1
;FLprintk2  gkGet, giPreset_n2
	aSignal = gasig


;Scanner

	kSc_LpOut tonek gkSc_C,gkSc_Acc
	kSc_Fst2 = kSc_LpOut*gkSc_Fst
	kSc_Freq = (kSc_Fst2>gkSc_Slw?kSc_Fst2:gkSc_Slw)
	kSc_sine oscil gkSc_Depth,kSc_Freq,1
	aSc_dlt1 interp (1-kSc_sine)*gkSc_Delay
	aSc_dlt2 interp (1+kSc_sine)*gkSc_Delay
	aSc_Dl1 vdelay aSignal,aSc_dlt1,60
	aSc_Dl2 vdelay aSignal,aSc_dlt2,60
;aSc_Out dcblock gkfx_Scan*(aSc_Dl1+aSc_Dl2)+(1-gkfx_Scan)*aSignal
	aSc_Out = gkfx_Scan*(aSc_Dl1+aSc_Dl2)+(1-gkfx_Scan)*aSignal


;Distortion
	aSat_In butterlp aSc_Out*gkDist_Drive,gkDist_Cutoff

kLp=2.5*(1-gkAsymm)
kLn=-2.5*(1+gkAsymm)


;aLp interp kLp
;aLn interp kLn
	aLp upsamp kLp
	aLn upsamp kLn


;now some a-rate conditionals with tables
;Positive

	aSat_In = 4*tanh(gkSoft_Drive*aSat_In);trick to avoid overflow of the original Sync Modular module (-4,4)
	arel1 table aSat_In,10,1,.5,0
aSat1=(arel1)*(((((-4*kLp)+aSat_In)*aSat_In)))/(-4*kLp)+(1-arel1)*aSat_In

	arel2 table aSat_In-2*kLp,10,1,.5,0
aSat2=(arel2)*aLp+(1-arel2)*aSat1


;Negative
	arel3 table aSat2,10,1,.5,0
aSat3=(1-arel1)*(((((-4*kLn)+aSat2)*aSat2)))/(-4*kLn)+(arel3)*aSat2

	arel4 table aSat2-2*kLn,10,1,.5,0
aSat4=(1-arel4)*aLp+(arel4)*aSat3

	aDist_Out mac gkDist_postGain*gkfx_Dist,aSat4,(1-gkfx_Dist),aSc_Out


;Feedback delay

;aFlanger_dlt interp gkFlanger_dlt
	aFlanger_dlt upsamp gkFlanger_dlt

	aFlanger flanger aDist_Out, aFlanger_dlt, gkFlanger_fdbk ,1
	aFlanger_Out dcblock gkfx_Del*aFlanger+(1-gkfx_Del)*aDist_Out
	aFlanger_Out mac gkfx_Del,aFlanger,(1-gkfx_Del),aDist_Out


;Freverb reverb

	aRvb_Free nreverb aFlanger_Out, gkRvb_time, gkRvb_Attn, 0, 8, 71, 4, 72
	aRvb_Out mac gkfx_Rev,aRvb_Free,(1-gkfx_Rev),aFlanger_Out


;Rotary Speaker Effect
;SPEED/ACCEL

	kL_Horn tonek gkL_Fst*gkSlowFast+gkL_Slw*(1-gkSlowFast),gkL_Acc
	kH_Horn tonek gkH_Fst*gkSlowFast+gkH_Slw*(1-gkSlowFast),gkH_Acc

	kH oscil 1, kH_Horn,1
	kL oscil 1, kL_Horn,1


;ATTEN
	aLP, aHP, aBP1 svfilter aDist_Out, gkFq_Sep, .7,1
	aBP1 = .3*aBP1
	aO1 = (kH+gkaM)*(aLP+aBP1)
	aO2 = (kL+gkaM)*(aHP+aBP1)
	kCL = gkL_Att2
	kCH = gkH_Att2


;HP FILTERS
kHQ=1+500*gkHQ;0.8667*500;(0~1?500?)
kLQ=1+500*gkLQ;0.8733*500;(")
 
	aLP2, aHP2, aBP2 svfilter aO2, gkHCut, kHQ,1
	aLP3, aHP3, aBP3 svfilter aO1, gkLCut, kLQ,1

	aDS = aO1 + (gkLow*(kL-1)*aHP3)
	aDD = aO1 + (gkLow*(-1-kL)*aHP3)
	aHS = aO2 + (gkHigh*(-1-kH)*aHP2)
	aHD = aO2 + (gkHigh*(kH-1)*aHP2)


;DOPPLER
	kH_Horn = (1/kH_Horn+1)*gkH_Doppler
	kL_Horn = (1/kL_Horn+1)*gkL_Doppler


;SEPARATOR1

	kLCL1 = kL*kCL
	kDL1 = (1-kLCL1)
	kSL1 = (kLCL1+1)
	kHCH1 = kH*kCH
	kDH1 = (1-kHCH1)
	kSH1 = (kHCH1+1)


;SEPARATOR2

	kLCL2 = kL*kL_Horn
	kDL2 = (1-kLCL2)
	kSL2 = (kLCL2+1)
	kHCH2 = kH*kH_Horn
	kDH2 = (1-kHCH2)
	kSH2 = (kHCH2+1)


;DELAYS
;interpolating upsamplers
;(works faster but adds noise)

	aDL2 interp kDL2
	aDH2 interp kDH2
	aSL2 interp kSL2
	aSH2 interp kSH2

	aD1 vdelay aDD*kDL1, 1.5*aSL2, 3
	aD2 vdelay aHD*kDH1, 1.5*aSH2, 3
	aD3 vdelay aDS*kSL1, 1.5*aDL2, 3
	aD4 vdelay aHS*kSH1, 1.5*aDH2, 3

	aLeslie_Dry = (1-gkfx_Rot)*aDist_Out
	aLeslie_L mac gkfx_Rot,(aD1+aD2),1,aLeslie_Dry
	aLeslie_R mac gkfx_Rot,(aD3+aD4),1,aLeslie_Dry

	gkirms init 1000
aout_L =gkirms*(aLeslie_L + aRvb_Out)*gkMain_Vol
aout_R =gkirms*(aLeslie_R + aRvb_Out)*gkMain_Vol
	galevel = aLeslie_L + aRvb_Out;take left channel as reference

	outs aout_L,aout_R
	gasig = 0
endin


instr 8
	gkrms rms galevel
	gkirms = 5000/gkrms
endin


instr allNotesOff
	turnoff2 1, 0, 1
	turnoff
endin

</CsInstruments>
<CsScore>
;f1 0 16384 10 1
;
;i2 0 360000
 
f1	0	8193	10	1
f2	0	8193	10	2	4	0	3	5	2

;distortion table
f6	0	8193	7	-.8	934	-.79	934	-.77	934	-.64	1034	-.48	520	.47	2300	.48	1536	.48
f7	0	4097	4	6	1
;relay table
f10	0	32	7	0	16	0	0	1	16	1
; freeverb time constants, as direct (negative) sample, with arbitrary gains
f71	0	16	-2	-1116	-1188	-1277	-1356	-1422	-1491	-1557	-1617	0.8	0.79	0.78	0.77	0.76	0.75	0.74	0.73
f72	0	16	-2	-556	-441	-341	-225	0.7	0.72	0.74	0.76

i2	0	360000
i99	0	360000
e
</CsScore>
</CsoundSynthesizer>
