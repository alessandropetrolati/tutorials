//
//  APEKnob.h
//  apeSoft
//
//  Created by Alessandro Petrolati on 01/02/12.
//  Copyright (c) 2012 apeSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DCSlider.h"
#import "CsoundObj.h"
#import "MainView.h"

@class DCSlider;

@interface APESlider : UIControl  <CsoundBinding> {
 
    NSString *min;
    NSString *max;
    NSString *def;
    Boolean bipolar;
    NSString *label;
    UIButton *increaseButton;
    UIButton *decreaseButton;
    NSTimer* _timerIncr;
    NSTimer* _timerDecr;
    
    float fineTuneAmount;
    
#pragma mark - Value Cacheable
	BOOL mCacheDirty;
	float cachedValue;
    float* channelPtr;
}

-(void) setValue:(float)value;
-(float) getValue;
-(float) getMaximumValue;
-(void) setMaximumValue:(float)value;
-(float) getMinimumValue;
-(void) setMinimumValue:(float)value;
-(float) getDefaultValue;
-(void) setDefaultValue:(float)value;
-(void) setEnabled:(BOOL)enable;

-(void)refreshValue;

@property (nonatomic, strong) DCSlider *slider;

#pragma mark - Value Cacheable
@property (assign) BOOL cacheDirty;
@end
