//
//  MainView.m
//  apeSoft
//
//  Created by Alessandro Petrolati on 15/02/12.
//  Copyright (c) 2012 apeSoft. All rights reserved.
//

#import "MainView.h"

@implementation MainView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)drawRect:(CGRect)rect {
    
    // Set the fill color
    [BACKGROUND_PARAMS_UICOLOR setFill];
    
    // Create the path for the first rounded rectangle
    CGRect roundedRect_up = CGRectMake(self.bounds.origin.x, self.bounds.origin.y, self.bounds.size.width, self.bounds.size.height/2);
    
    // Create the path for the second rounded rectangle
    UIBezierPath *roundedRectPath_up = [UIBezierPath bezierPathWithRect:roundedRect_up];
    [roundedRectPath_up fill];
    
    [BACKGROUND_WAVE_UICOLOR setFill];
    
    // Create the path for the rounded rectangle
    CGRect roundedRect_down = CGRectMake(self.bounds.origin.x, self.bounds.size.height/2, self.bounds.size.width, self.bounds.size.height/2);
    
    UIBezierPath *roundedRectPath_down = [UIBezierPath bezierPathWithRect:roundedRect_down];
    [roundedRectPath_down fill];
}

@end
