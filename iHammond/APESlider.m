//
//  APEKnob.m
//  apeSoft
//
//  Created by Alessandro Petrolati on 01/02/12.
//  Copyright (c) 2012 apeSoft. All rights reserved.
//

#import "APESlider.h"
#import <QuartzCore/QuartzCore.h>

@implementation APESlider

@synthesize slider;
@synthesize cacheDirty = mCacheDirty;

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    /* Shadows */
	self.layer.shadowColor = BACKGROUND_PARAMS_UICOLOR.CGColor;
	self.layer.shadowOffset = CGSizeZero;
	self.layer.shadowRadius = 9.0;
	self.layer.shadowOpacity = 0.7;
    
	self.slider = [[DCSlider alloc] initWithDelegate:self];

    /* Slider Initialize MUST BE AFTER slider allocation */
    slider.biDirectional = NO;
    BOOL fineButtons = NO;
    BOOL showLabel = NO;
    [self.slider setDrawBarMode:YES];
    
    
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, self.frame.size.height+20);    
	self.slider.frame = CGRectMake(floorf((self.frame.size.width - self.frame.size.width) / 2),
                                   floorf(((self.frame.size.height - self.frame.size.height)-0) / 2),
                                   self.frame.size.width,
                                   self.frame.size.height-20);
    
    
	self.slider.labelFont = [UIFont fontWithName:@"Helvetica" size:12.0];
    self.slider.labelOffset = CGPointMake(0, 0); //text X offset
    
    /* Color */
	self.backgroundColor = [UIColor clearColor];    
    self.slider.backgroundColor = [UIColor clearColor];    
    
    int ndx = 4; //black, red, green, blue
	switch (ndx)
	{
		case 0: self.slider.color = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.7]; break;
		case 1: self.slider.color = [UIColor colorWithRed:1.0 green:0.0 blue:0.0 alpha:1.0]; break;
		case 2: self.slider.color = [UIColor colorWithRed:0.0 green:1.0 blue:0.0 alpha:1.0]; break;
		case 3: self.slider.color = [UIColor colorWithRed:0.0 green:0.0 blue:1.0 alpha:1.0]; break;
		case 4: self.slider.color = BACKGROUND_PARAMS_UICOLOR; break;            
        case 5: self.slider.color = BACKGROUND_WAVE_UICOLOR; break;            
	}
    
    self.slider.labelColor = [UIColor blackColor];
    
    self.slider.biDirectional = bipolar;
    
    if (bipolar)
        self.slider.min = -[max floatValue]; //min
    else
        self.slider.min = [min floatValue]; //min
    
	self.slider.max = [max floatValue]; //max
    
    if ([self.slider getDrawBarMode]){
        self.slider.value = [max floatValue] - [def floatValue]; //default value
        self.slider.doubleTapValue = [max floatValue] - [def floatValue]; //double tap value
        self.slider.tripleTapValue = [max floatValue] - [def floatValue]; //triple tap value
    }
    else {

        self.slider.value = [def floatValue]; //default value
        self.slider.doubleTapValue = [def floatValue]; //double tap value
        self.slider.tripleTapValue = [def floatValue]; //triple tap value
    }
    
    [self addSubview:slider];
    
    if (fineButtons) {
        // Fine Step
        Float32 stepsNumbers = 350.f;
        Float32 range = self.slider.max - self.slider.min;    
        fineTuneAmount = range / stepsNumbers;
        
        decreaseButton = [UIButton buttonWithType:UIButtonTypeInfoLight];
        [decreaseButton setImage:[UIImage imageNamed:@"chevron-rev~iphone"] forState:UIControlStateNormal];
        decreaseButton.frame = CGRectMake(0, self.frame.size.height-20, 20, 20);
        decreaseButton.layer.shadowColor = self.layer.shadowColor;
        decreaseButton.layer.shadowOffset = self.layer.shadowOffset;
        decreaseButton.layer.shadowRadius = self.layer.shadowRadius;
        decreaseButton.layer.shadowOpacity = self.layer.shadowOpacity;
        [self addSubview:decreaseButton];
        [decreaseButton addTarget:self action:@selector(fineTuneButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        increaseButton = [UIButton buttonWithType:UIButtonTypeInfoLight];
        [increaseButton setImage:[UIImage imageNamed:@"chevron~iphone"] forState:UIControlStateNormal];
        increaseButton.frame = CGRectMake(self.frame.size.width-20 , self.frame.size.height-20, 20, 20);
        increaseButton.layer.shadowColor = self.layer.shadowColor;
        increaseButton.layer.shadowOffset = self.layer.shadowOffset;
        increaseButton.layer.shadowRadius = self.layer.shadowRadius;
        increaseButton.layer.shadowOpacity = self.layer.shadowOpacity;
        [self addSubview:increaseButton];
        [increaseButton addTarget:self action:@selector(fineTuneButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        // add the gesture recognizers for the UILongPressGestureRecognizer 
        UILongPressGestureRecognizer *longPressIncrease = [[UILongPressGestureRecognizer alloc]
                                                            initWithTarget:self 
                                                            action:@selector(handleLongPressIncrease:)];
        longPressIncrease.minimumPressDuration = 0.5;
        [increaseButton addGestureRecognizer:longPressIncrease];
        
        UILongPressGestureRecognizer *longPressDecrease = [[UILongPressGestureRecognizer alloc]
                                                            initWithTarget:self 
                                                            action:@selector(handleLongPressDecrease:)];
        longPressDecrease.minimumPressDuration = 0.5;
        [decreaseButton addGestureRecognizer:longPressDecrease];
        
    }
    
    if(showLabel) {
        /* Label inherit value from display number */
        UILabel *myLabel = [[UILabel alloc] init];
        myLabel.text = label;
        myLabel.frame = CGRectMake(0 , -20, self.frame.size.width, 15);
        myLabel.backgroundColor = self.backgroundColor;
        myLabel.minimumScaleFactor = 2.0;
        myLabel.adjustsFontSizeToFitWidth = YES;
        myLabel.textAlignment = NSTextAlignmentCenter;
        
        //  myLabel.layer.shadowColor = self.layer.shadowColor;
        //	myLabel.layer.shadowOffset = self.layer.shadowOffset;
        //	myLabel.layer.shadowRadius = self.layer.shadowRadius;
        //	myLabel.layer.shadowOpacity = self.layer.shadowOpacity;
        
        myLabel.font = 	self.slider.labelFont;
        myLabel.textColor = self.slider.labelColor;
        
        [self addSubview:myLabel];
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {

	return YES;
}


- (void)fineTuneButtonPressed:(id)sender
{
	if (sender == increaseButton)
        slider.value += fineTuneAmount;
    
	else if (sender == decreaseButton) {
        slider.value -= fineTuneAmount;
    }
}

#pragma mark -
#pragma mark Delegate Methods

- (void)controlValueDidChange:(float)value sender:(id)sender
{
    [super sendActionsForControlEvents:UIControlEventValueChanged];
}

- (void)controlValueDidUp:(float)value sender:(id)sender 
{
    [super sendActionsForControlEvents:UIControlEventTouchUpInside];
}

- (void)controlValueDidDown:(float)value sender:(id)sender
{
    [super sendActionsForControlEvents:UIControlEventTouchDown];
}

#pragma mark -
#pragma mark Accessor Methods
-(void) setValue:(float)value {

    if ([slider getDrawBarMode])
        [slider setValue:(slider.max + slider.min) - value];
    else
        [slider setValue:value];
}

-(float) getValue {

    if ([self.slider getDrawBarMode])
        return  [max floatValue] - slider.value;
    else
        return  slider.value;
}

-(void) setMaximumValue:(float)value {
    
    slider.max = value;
    [slider setNeedsDisplay];
}

-(float) getMaximumValue {
    
    return slider.max;
}

-(void) setMinimumValue:(float)value {
    
    slider.min = value;
    [slider setNeedsDisplay];
}

-(float) getMinimumValue {
    return slider.min;
}

-(float) getDefaultValue {
    
    return self.slider.doubleTapValue;
}

-(void) setDefaultValue:(float)value {
    
    slider.doubleTapValue = value;
}

#pragma mark -
#pragma handleLongPress Gesture

- (void)handleLongPressIncrease:(UILongPressGestureRecognizer *)gesture {
    
    if(UIGestureRecognizerStateBegan == gesture.state) {
        // Do initial work here
        if(!_timerIncr.isValid)
            _timerIncr = [NSTimer scheduledTimerWithTimeInterval:0.1
                                                          target:self
                                                        selector:@selector(INCREASEfineTuneButtonPressed)
                                                        userInfo:nil
                                                         repeats:YES];
        
    }
    
    if(UIGestureRecognizerStateEnded == gesture.state) {
        // Do end work here when finger is lifted
        [_timerIncr invalidate];
        _timerIncr = nil;
    }
}

- (void)handleLongPressDecrease:(UILongPressGestureRecognizer *)gesture {
    
    
    if(UIGestureRecognizerStateBegan == gesture.state) {
        // Do initial work here
        
        if(!_timerDecr.isValid)
            _timerDecr = [NSTimer scheduledTimerWithTimeInterval:0.1
                                                          target:self
                                                        selector:@selector(DECREASEfineTuneButtonPressed)
                                                        userInfo:nil
                                                         repeats:YES];
        
    }
    
    if(UIGestureRecognizerStateChanged == gesture.state) {
        // Do repeated work here (repeats continuously) while finger is down
    }
    
    if(UIGestureRecognizerStateEnded == gesture.state) {
        // Do end work here when finger is lifted
        [_timerDecr invalidate];
        _timerDecr = nil;
    }
}

- (void)INCREASEfineTuneButtonPressed
{
    [self performSelector:@selector(fineTuneButtonPressed:) withObject:increaseButton];
    
}

- (void)DECREASEfineTuneButtonPressed
{
    [self performSelector:@selector(fineTuneButtonPressed:) withObject:decreaseButton];   
}

-(void)setEnabled:(BOOL)enable {
    
    [super setEnabled:enable];
    
    if(enable) {

        self.alpha = 1.; 
    }
    else {

        self.alpha = 0.4;
    }
}


#pragma mark --------------------------------------------------------------------------------------------------------------
#pragma mark --------------------------------------------------------------------------------------------------------------
#pragma mark --------------------------------------------------------------------------------------------------------------
#pragma mark - Value Cacheable

- (void)setup:(CsoundObj *)csoundObj
{
	channelPtr = [csoundObj getInputChannelPtr:[label lowercaseString] channelType:CSOUND_CONTROL_CHANNEL];
    cachedValue = slider.value;
    self.cacheDirty = YES;
    [self addTarget:self action:@selector(updateValueCache:) forControlEvents:UIControlEventValueChanged];
}

- (void)updateValueCache:(id)sender
{
	cachedValue = ((APESlider*)sender).getValue;
    self.cacheDirty = YES;
}

- (void)updateValuesToCsound
{
	if (self.cacheDirty) {
        *channelPtr = cachedValue;
        self.cacheDirty = NO;
    }
}

- (void)updateValuesFromCsound
{
	
}

- (void)cleanup
{
	[self removeTarget:self action:@selector(updateValueCache:) forControlEvents:UIControlEventValueChanged];
}

-(void)refreshValue {
    
    self.slider.value = self.slider.value;
}

@end
