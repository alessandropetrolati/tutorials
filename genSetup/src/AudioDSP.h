//
//  AudioDSP.h
//  genSetup
//
//  Created by Alessandro Petrolati on 28/02/2017
//  Copyright (c) 2017 apeSoft. All rights reserved


#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioServices.h>
#import <AudioUnit/AudioUnit.h>
#import <AVFoundation/AVFoundation.h>
#import "oscillatore.h"
#import <vector>

#define IAA
//#define AB

//#define ENABLE_BLUETOOTH
#define ENABLE_MIDI

@interface AudioDSP : NSObject {
    
    CommonState* oscil;
    std::vector<t_sample *> inputBuffers;
    std::vector<t_sample *> outputBuffers;

    /* Audio IO */
    AudioUnit csAUHAL;
    
    BOOL inputIsMono;
    Float64 CURRENT_SAMPLING_RATE;
    Float64 CURRENT_BUFFER_FRAME;

    
    /* User Interface */
    IBOutlet UISlider* freq;
    IBOutlet UILabel* hzLabel;
    
    IBOutlet UISlider* amp;
    IBOutlet UILabel* ampLabel;

}
#ifdef AB
//AUDIOBUS
@property (strong, nonatomic) ABAudiobusController* AB_Controller;
@property (strong, nonatomic) ABSenderPort* output;
@property (strong, nonatomic) ABFilterPort* filter;
#endif

#ifdef IAA
@property (nonatomic, readonly) BOOL connected;
#endif

@end
