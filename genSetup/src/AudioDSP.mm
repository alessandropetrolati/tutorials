//
//  AudioDSP.m
//  genSetup
//
//  Created by Alessandro Petrolati on 28/02/2017
//  Copyright (c) 2017 apeSoft. All rights reserved

#define Check(expr) do { OSStatus err = (expr); if (err) { NSLog(@"error %d from %s", (int)err, #expr); /*exit(1);*/ } } while (0)


#import "AudioDSP.h"
#import "CAStreamBasicDescription.h"

#define SAMPLE_RATE_DEF 44100.0
#define BUFFER_FRAME 256.0
#define EXTREME_BUFFER_SIZE 4096

@implementation AudioDSP

#ifdef IAA
@synthesize connected = _connected;
#endif

#ifdef AB
@synthesize AB_Controller = _AB_Controller;
@synthesize output = _output;
@synthesize filter = _filter;
#endif

- (instancetype)init {
    
    self = [super init];
    if (self) {
        
        NSLog(@"%s", __FUNCTION__);
    
        /* Allocate DSP gen~ */
        oscil = (CommonState *)oscillatore::create(SAMPLE_RATE_DEF, BUFFER_FRAME);

        /* Allocate Buffers */
        int i;
        for(i=0; i < oscillatore::num_inputs(); ++i) {
            inputBuffers.push_back(new t_sample[EXTREME_BUFFER_SIZE]);
        }
        for(i=0; i < oscillatore::num_outputs(); ++i) {
            outputBuffers.push_back(new t_sample[EXTREME_BUFFER_SIZE]);
        }
        
        /* Clear Buffer */
        for(int c = 0; c < oscillatore::num_inputs(); ++c)
            memset(inputBuffers[c], 0.0, EXTREME_BUFFER_SIZE);
        
        for(int c = 0; c < oscillatore::num_outputs(); ++c)
            memset(outputBuffers[c], 0.0, EXTREME_BUFFER_SIZE);


        
        // Setup CoreAudio
        [self initializeAudio];
    }
    return self;
}

-(void)dealloc {
 
    oscillatore::destroy((CommonState *)oscil);
    oscil = NULL;

    int i;
    for(i=0; i < inputBuffers.size(); ++i) {
        delete[] inputBuffers[i];
    }
    for(i=0; i < outputBuffers.size(); ++i) {
        delete[] outputBuffers[i];
    }
}

-(void)awakeFromNib {
    
    [super awakeFromNib];
    
    NSLog(@"%s", __FUNCTION__);
    
    // Configure UI Widgets
    [self performSelectorOnMainThread:@selector(refreshParameters) withObject:nil waitUntilDone:NO];
}

-(void)refreshParameters {

    //Refresh parameters
    [freq sendActionsForControlEvents:UIControlEventValueChanged];
    [amp sendActionsForControlEvents:UIControlEventValueChanged];

}

-(void)initializeAudio {
    
    /* Audio Session handler */
    AVAudioSession* session = [AVAudioSession sharedInstance];
    
    NSError* error = nil;
    BOOL success = NO;
    
    success = [session setCategory:AVAudioSessionCategoryPlayAndRecord
                       withOptions:(AVAudioSessionCategoryOptionMixWithOthers |
                                    AVAudioSessionCategoryOptionDefaultToSpeaker
#ifdef ENABLE_AUDIO_BLUETOOTH
                                    | AVAudioSessionCategoryOptionAllowBluetoothA2DP
#endif
                                    )
                             error:&error];
    
    if (!success) {
        NSLog(@"%@ Error setting category: %@",
              NSStringFromSelector(_cmd), [error localizedDescription]);
    }
    
    
    success = [session setActive:YES error:&error];
    
    if (!success) {
        NSLog(@"%@", [error localizedDescription]);
    }
    
    
    inputIsMono = (([AVAudioSession sharedInstance].inputNumberOfChannels) < 2);
    
    /* Sets Interruption Listner */
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(InterruptionListener:)
                                                 name:AVAudioSessionInterruptionNotification
                                               object:session];
    
    [self CONFIGURE_SR_AND_BUFFER:SAMPLE_RATE_DEF withBuffer:BUFFER_FRAME];

    /* Publish Inter-App when app is off */
    AudioComponentDescription defaultOutputDescription;
    defaultOutputDescription.componentType = kAudioUnitType_Output;
    defaultOutputDescription.componentSubType = kAudioUnitSubType_RemoteIO;
    defaultOutputDescription.componentManufacturer = kAudioUnitManufacturer_Apple;
    defaultOutputDescription.componentFlags = 0;
    defaultOutputDescription.componentFlagsMask = 0;
    
    // Get the default playback output unit
    AudioComponent HALOutput = AudioComponentFindNext(NULL, &defaultOutputDescription);
    NSAssert(HALOutput, @"Can't find default output");
    
    // Create a new unit based on this that we'll use for output
    Check(AudioComponentInstanceNew(HALOutput, &csAUHAL));
    
    /*
     | i                   o |
     -- BUS 1 -- from mic --> | n    REMOTE I/O     u | -- BUS 1 -- to app -->
     | p      AUDIO        t |
     -- BUS 0 -- from app --> | u       UNIT        p | -- BUS 0 -- to speaker -->
     | t                   u |
     |                     t |
     -------------------------
     */
    
    // Enable IO for recording
    UInt32 flag = 1;
    Check(AudioUnitSetProperty(csAUHAL, kAudioOutputUnitProperty_EnableIO,  kAudioUnitScope_Input, 1, &flag, sizeof(UInt32)));
    // Enable IO for playback
    Check(AudioUnitSetProperty(csAUHAL, kAudioOutputUnitProperty_EnableIO, kAudioUnitScope_Output, 0, &flag, sizeof(UInt32)));
    
    CAStreamBasicDescription ioFormat = CAStreamBasicDescription(SAMPLE_RATE_DEF, 2, CAStreamBasicDescription::kPCMFormatFloat32, false);
    Check(AudioUnitSetProperty(csAUHAL, kAudioUnitProperty_StreamFormat, kAudioUnitScope_Input, 0, &ioFormat, sizeof(CAStreamBasicDescription)));
    
    UInt32 maxFramesPerSlice = 4096;
    Check(AudioUnitSetProperty(csAUHAL, kAudioUnitProperty_MaximumFramesPerSlice, kAudioUnitScope_Global, 0, &maxFramesPerSlice, sizeof(UInt32)));
    Check(AudioUnitSetProperty(csAUHAL, kAudioUnitProperty_MaximumFramesPerSlice, kAudioUnitScope_Global, 1, &maxFramesPerSlice, sizeof(UInt32)));
    
    // Set input callback
    AURenderCallbackStruct callbackStruct;
    // Set output callback
    callbackStruct.inputProc = PROCESSING;
    callbackStruct.inputProcRefCon = (__bridge void*) self;
    Check(AudioUnitSetProperty(csAUHAL,
                               kAudioUnitProperty_SetRenderCallback,
                               kAudioUnitScope_Input,
                               0,
                               &callbackStruct,
                               sizeof(callbackStruct)));
    
    Check(AudioUnitInitialize(csAUHAL));
    
    AudioComponentDescription dynamics_desc;
    memset(&dynamics_desc, 0, sizeof(dynamics_desc));
    dynamics_desc.componentManufacturer = kAudioUnitManufacturer_Apple;
    dynamics_desc.componentType = kAudioUnitType_Effect;
    dynamics_desc.componentSubType = kAudioUnitSubType_DynamicsProcessor;
    

    /* AUDIOBUS and IAA  */
    [self initializeAB_IAA];
    
    Check(AudioOutputUnitStart(csAUHAL));
}


-(void)initializeAB_IAA {
    
#warning: in the AudioComponents key of Info.plist you must declare the same informations
    
    /* geni, youc */
    
    /* Create Sender and Filter ports */
    AudioComponentDescription desc_instr = {
        kAudioUnitType_RemoteInstrument,
        'geni',
        'youc', 0, 0 };
    
    AudioComponentDescription desc_fx = {
        kAudioUnitType_RemoteEffect,
        'genx',
        'youc', 0, 0 };
    
    /* ===================== Inter-App Audio Setup ========================== */
#ifdef IAA
    
    [self addAudioUnitPropertyListener];
    [self setupMidiCallBacks:&csAUHAL userData:(__bridge void *)(self)];
    
    OSStatus result;
    
    result = AudioOutputUnitPublish(&desc_instr,
                                    (CFStringRef)@"genSetup (instr)",
                                    1, csAUHAL);
    
    result = AudioOutputUnitPublish(&desc_fx,
                                    (CFStringRef)@"genSetup (fx)",
                                    1, csAUHAL);
    
#endif
    
    
    /* ===================== Audiobus Setup ========================== */
#ifdef AB
    
    //    MTQ4OTI0MjIyOSoqKjAyX0FCX0lBQSoqKjAyLUFCLUlBQS5hdWRpb2J1czovLyoqKlthdXJpLml5b3UuaWNzby4xXVthdXJ4Lnh5b3UueGNzby4xXQ==:Nvv08/8XaZ6RyJZwI3KGn4Ze+3ReW33XxTm5X5mRN/vMO66Wfsztdqlou6/2aai/jUho4NkKc5CUmtKEfEuKa1FAKzSOrF2JsqnZv5pzjAiivcjWyWmfRLEaM376QVtn
    
    _AB_Controller = [[ABAudiobusController alloc] initWithApiKey:@"YOUR_AB_KEY"];
    _AB_Controller.allowsConnectionsToSelf = NO;
    _AB_Controller.connectionPanelPosition = ABConnectionPanelPositionRight;
    
    _output = [[ABSenderPort alloc] initWithName:@"genSetup (instr)"
                                           title:@"genSetup (instr)"
                       audioComponentDescription:desc_instr
                                       audioUnit:csAUHAL];
    
    _output.derivedFromLiveAudioSource = YES;
    
    [_AB_Controller addSenderPort:_output];
    
    _filter = [[ABFilterPort alloc] initWithName:@"genSetup (fx)"
                                           title:@"genSetup (fx)"
                       audioComponentDescription:desc_fx
                                       audioUnit:csAUHAL];
    
    //_filter.latency = _currentKsmps;
    
    [_AB_Controller addFilterPort:_filter];
    
#endif
    
}

-(void)CONFIGURE_SR_AND_BUFFER:(Float64)sr withBuffer:(Float64)ksmps {
    
    AVAudioSession *session = [AVAudioSession sharedInstance];
    NSError* err = nil;
    
    [session setPreferredSampleRate:sr error:&err];
    
    CURRENT_SAMPLING_RATE =  session.sampleRate;
    
    // preferred buffer size
    Float32 preferredBufferSize = ksmps / CURRENT_SAMPLING_RATE;
    [session setPreferredIOBufferDuration:preferredBufferSize error:&err];
    
    /* Verify is user setting are valid for current session */
    Float32 srVerbose = [AVAudioSession sharedInstance].sampleRate;
    
    NSTimeInterval bufferDurationVerbose;
    bufferDurationVerbose = session.IOBufferDuration;
    
    CURRENT_SAMPLING_RATE = srVerbose;
    CURRENT_BUFFER_FRAME = roundf(bufferDurationVerbose * srVerbose);
    
    Float64 latency = (CURRENT_BUFFER_FRAME / srVerbose) * 1000.f;
    
    
    NSLog(@"%@", [NSString stringWithFormat:@"Sampling Rate %.0f; Buffer Size %d; Latency %.3f msec. (large buffer increase latency but reduce CPU)",
                  srVerbose, (int)CURRENT_BUFFER_FRAME, latency]);
    
    

    /* Apply SR and BUFFER to DSPs */
    oscil->vs = CURRENT_BUFFER_FRAME;
    oscil->sr = CURRENT_SAMPLING_RATE;
    
    oscillatore::reset(oscil);
}

#pragma mark Gen Code

//Called when inNumberFrames >= ksmps
OSStatus  PROCESSING(void *inRefCon,
                         AudioUnitRenderActionFlags *ioActionFlags,
                         const AudioTimeStamp *inTimeStamp,
                         UInt32 dump,
                         UInt32 inNumberFrames,
                         AudioBufferList *ioData
                         )
{
    
    AudioDSP *_self = (__bridge AudioDSP*) inRefCon;
    
    //Pull audio from AudioUnit
    AudioUnitRender(_self->csAUHAL, ioActionFlags, inTimeStamp, 1, inNumberFrames, ioData);

    // convert input channels into a Gen-ready format
    for(int i = 0; i < MIN(ioData->mNumberBuffers, _self->inputBuffers.size()); ++i) {
        FloatToSample(_self->inputBuffers[i], (Float32*)ioData->mBuffers[i].mData, inNumberFrames);
    }

    // Perform gen~
    oscillatore::perform((CommonState *)_self->oscil, &(_self->inputBuffers[0]), _self->inputBuffers.size(), &(_self->outputBuffers[0]), _self->outputBuffers.size(), inNumberFrames);

    
    // convert Gen-ready back into audio channels
    for(int i = 0; i < ioData->mNumberBuffers; ++i) {
        // wrap channels for case where more inputs are handed in than the gen filter outputs
        int gen_idx = i % _self->outputBuffers.size();
        FloatFromSample((Float32*)ioData->mBuffers[i].mData, _self->outputBuffers[gen_idx], inNumberFrames);
    }

    
    return noErr;
}

#pragma mark -------------------------------------

void FloatToSample(t_sample *dst, const float *src, long n) {
    while (n--) *dst++ = *src++;
}

void FloatFromSample(float *dst, const t_sample *src, long n) {
    while (n--) *dst++ = *src++;
}

void zeroBuffer(t_sample *dst, int n) {
    while (n--) *dst++ = 0;
}

#pragma mark - Inter-app Audio Change Dispather delegate
void AudioUnitPropertyChangeDispatcher(void *inRefCon, AudioUnit inUnit, AudioUnitPropertyID inID, AudioUnitScope inScope, AudioUnitElement inElement) {
    
    AudioDSP *audio = (__bridge AudioDSP *)inRefCon;
    
    if (inID==kAudioUnitProperty_IsInterAppConnected)
    {
        UInt32 connect;
        UInt32 dataSize = sizeof(UInt32);
        AudioUnitGetProperty(inUnit, kAudioUnitProperty_IsInterAppConnected, kAudioUnitScope_Global, 0, &connect, &dataSize);
        if (connect)
        {
            NSLog(@"%s -> %@", __FUNCTION__, @"CONNECTED");
            
            audio->_connected = YES;
            [AudioDSP setAudioSessionActive];
            AudioOutputUnitStart(audio->csAUHAL);
            AudioOutputUnitStart(inUnit);
            
        }
        else
        {
            NSLog(@"%s -> %@", __FUNCTION__, @"UN-CONNECTED");
            audio->_connected = NO;
            [AudioDSP setAudioSessionActive];
            /* Important, we don't wont audio stops when disconnecting */
            AudioOutputUnitStart(audio->csAUHAL);
            
            // update and post IAA transport state
        }
    }
    if (inID==kAudioOutputUnitProperty_HostTransportState)
    {
        NSLog(@"%s -> %@", __FUNCTION__, @"kAudioOutputUnitProperty_HostTransportState");
        // update and post IAA transport state
    }
}

#pragma mark - Inter-App Audio
+ (void) setAudioSessionActive {
    
    AVAudioSession *session = [AVAudioSession sharedInstance];
    NSError *err;
    [session setCategory: AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionMixWithOthers
     
#if ENABLE_BLUETOOTH
     | AVAudioSessionCategoryOptionAllowBluetooth
#endif
                   error:  &err];
    
    [session setActive: YES error:  &err];
}

+ (void) setAudioSessionInActive {
    
    AVAudioSession *session = [AVAudioSession sharedInstance];
    NSError *err;
    [session setActive: NO error:  &err];
}

-(void)addAudioUnitPropertyListener {
    
    AudioUnitAddPropertyListener(csAUHAL,
                                 kAudioUnitProperty_IsInterAppConnected,
                                 AudioUnitPropertyChangeDispatcher,
                                 (__bridge void *)(self));
    AudioUnitAddPropertyListener(csAUHAL,
                                 kAudioOutputUnitProperty_HostTransportState,
                                 AudioUnitPropertyChangeDispatcher,
                                 (__bridge void *)(self));
}

-(void) setupMidiCallBacks:(AudioUnit*)output userData:(void*)inUserData
{
    AudioOutputUnitMIDICallbacks callBackStruct;
    callBackStruct.userData = inUserData;
    callBackStruct.MIDIEventProc = MIDIEventProcCallBack;
    callBackStruct.MIDISysExProc = NULL;
    AudioUnitSetProperty (*output,
                          kAudioOutputUnitProperty_MIDICallbacks,
                          kAudioUnitScope_Global,
                          0,
                          &callBackStruct,
                          sizeof(callBackStruct));
}

#pragma mark IAA MIDI Receive Data
void MIDIEventProcCallBack(void *userData, UInt32 inStatus, UInt32 inData1, UInt32 inData2, UInt32 inOffsetSampleFrame)
{
    fprintf(stderr,"midi received from host\n");
    
    //parse note on/off
    if (inStatus == 144 || inStatus == 128) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            int noteNum = inData1;
            int noteVel = inData2;
            
            //Receive MIDI Note On
            if (inStatus == 144) {
                NSLog(@"%s -> NOTE_ON_NUM -> NOTE_VELOC %d %d", __FUNCTION__, noteNum, noteVel);
            }
            
            //Receive MIDI Note Off
            if(inStatus == 128) {
                NSLog(@"%s -> NOTE_OFF_NUM -> NOTE_VELOC %d %d", __FUNCTION__, noteNum, noteVel);
            }
        });
    }
    
    // Parse CC and others MIDI events
    //...
}

#pragma mark AVAudioSessionInterruptionNotification

-(void)InterruptionListener: (NSNotification*) aNotification
{
    NSDictionary *interuptionDict = aNotification.userInfo;
    NSNumber* interuptionType = (NSNumber*)[interuptionDict valueForKey:AVAudioSessionInterruptionTypeKey];
    
    if([interuptionType intValue] == AVAudioSessionInterruptionTypeBegan) {
        
        NSLog(@"_____________________________%s Begin Interruption", __FUNCTION__);
        
        AudioOutputUnitStop(csAUHAL);
    }
    
    else if ([interuptionType intValue] == AVAudioSessionInterruptionTypeEnded) {
        
        NSLog(@"_____________________________%s End Interruption", __FUNCTION__);
        AudioOutputUnitStart(csAUHAL);
    }
}

#pragma  mark - User Interface Actions

-(IBAction)sliderAction:(id)sender {
    
    UISlider* sld = sender;
    
    switch (sld.tag) {
        case 0:
            NSLog(@"%s -> FREQ. = %f", __FUNCTION__, sld.value);
            [hzLabel setText:[NSString stringWithFormat:@"Frequency = %f", sld.value]];
            
            oscillatore::setparameter((CommonState *)self->oscil, 1, sld.value, NULL);
            
            break;

        case 1:
            NSLog(@"%s -> AMP. = %f", __FUNCTION__, sld.value);
            [ampLabel setText:[NSString stringWithFormat:@"Amplitude = %f", sld.value]];
            
            oscillatore::setparameter((CommonState *)self->oscil, 0, sld.value, NULL);
            
            break;

        default:
            break;
    }
    
    
}
@end
