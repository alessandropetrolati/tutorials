//
//  AppDelegate.h
//  genSetup
//
//  Created by Alessandro Petrolati on 28/02/2017
//  Copyright (c) 2017 apeSoft. All rights reserved

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

