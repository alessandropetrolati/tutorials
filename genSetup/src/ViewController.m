//
//  ViewController.m
//  genSetup
//
//  Created by Alessandro Petrolati on 28/02/2017
//  Copyright (c) 2017 apeSoft. All rights reserved

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
