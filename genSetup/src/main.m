//
//  main.m
//  genSetup
//
//  Created by Alessandro Petrolati on 28/02/2017
//  Copyright (c) 2017 apeSoft. All rights reserved

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
