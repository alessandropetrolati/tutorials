//
//  AudioDSP.h
//  06_customOpcode
//
//  Created by Alessandro Petrolati on 29/05/15.
//  Copyright (c) 2015 apeSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioServices.h>
#import <AudioUnit/AudioUnit.h>
#import <AVFoundation/AVFoundation.h>

#import "csound.h"
#import "csdl.h"

typedef struct {
    OPDS    h;
    MYFLT   *ar, *asig, *kcutoff, *kresonance;
    
    double az1;
    double az2;
    double az3;
    double az4;
    double az5;
    double ay4;
    double amf;
    double sr;
    double inv_sr;
    double i2v;
    double inv_i2v;
    int ksmps;
    
} MOOGLADDER_OPCODE;

#if! TARGET_OS_MACCATALYST
#define IAA
//#define AB
#endif

//#define ENABLE_BLUETOOTH
#define ENABLE_MIDI

#ifdef AB
#import "Audiobus.h"
#import "ABAudiobusController.h"
#endif

@interface AudioDSP : NSObject {
    
    /* Audio IO */
    AudioUnit csAUHAL;
    OSStatus err;
    ExtAudioFileRef recFile;
    
    /* Csound data */
    long bufframes;
    int ret;
    int nchnls;
    int counter;
    bool running;
    bool shouldRecord;
    
    /* User Interface */
    IBOutlet UISwitch* rec;
    
    IBOutlet UILabel* hzLabel;    
    IBOutlet UISlider* cutoff;
    IBOutlet UISlider* resonance;
    IBOutlet UISlider* inputGain;
    IBOutlet UISlider* inputAmpNoise;
}
@property(nonatomic, readwrite) CSOUND* cs;

#ifdef AB
//AUDIOBUS
@property (strong, nonatomic) ABAudiobusController* AB_Controller;
@property (strong, nonatomic) ABSenderPort* output;
@property (strong, nonatomic) ABFilterPort* filter;
#endif

#ifdef IAA
@property (nonatomic, readonly) BOOL connected;
#endif
@end
