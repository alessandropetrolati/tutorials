/*
cs4dev tutorial 06
by Alessandro Petrolati
www.apesoft.it
*/

<CsoundSynthesizer>
<CsOptions>

-o dac
-+rtmidi=null
-+rtaudio=null
-d
-+msg_color=0
--expression-opt
-M0
-m0
-i adc

</CsOptions>
<CsInstruments>

sr = 44100
ksmps = 64
nchnls = 2
0dbfs = 1

instr 1

kinputgain chnget "in_gain"
kinputgain scale kinputgain, 127, 0
kinputgain	gainslider	kinputgain

knoisegain chnget "noise_gain"
knoisegain scale knoisegain, 127, 0
knoisegain	gainslider	knoisegain


kcutoff chnget "cutoff"
kcutoff	limit kcutoff, 20, sr / 2 ;(Nyquist/2 when use moogvcf)
kcutoff tonek kcutoff, 10
kres chnget "resonance"

ain1, ain2 ins
ain = (ain1 + ain2) * 0.5
ain *= kinputgain

anoise noise knoisegain, 0

ain = ain + anoise
aOut MOOGLADDER ain, kcutoff, kres

aOut balance aOut, ain

outs aOut, aOut

endin

</CsInstruments>
<CsScore>

i 1 0 10000

</CsScore>
</CsoundSynthesizer>
