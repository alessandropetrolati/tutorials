/* 
 
 APE_MATRIX.h
 
 This file is part of iVCS3
 Copyright (C) 2013 Alessandro Petrolati
 
 Created by Alessandro Petrolati on 05/13
 
 */

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>


//8 x 8  matrix
#define MATRIX_SIZE 4

#define MATRIX_SCALE_FACTOR 1.8

@protocol APE_MATRIX_Delegate;

@interface APE_MATRIX : UIView {

    
	CGRect matrixRects[(MATRIX_SIZE*MATRIX_SIZE)];
	BOOL connected[(MATRIX_SIZE*MATRIX_SIZE)];
    int lastConnection;
    
    UIView* lineW;
    UIView* lineH;
    
    id _matrixDelegate;
}

-(void)clear;
-(void)refresh;
-(void)saveAsDefaults;
-(void)loadAsDefaults;
-(BOOL)getPinState:(int)ndx;
-(void)setDelegate:(id)obj;
-(NSMutableArray*)getState;
-(void)restoreState:(NSMutableArray*)keyState;
@end

@interface APE_MATRIX ()
-(void)updatePinsRects;
-(void)initialize;
@end

@protocol APE_MATRIX_Delegate

-(void)matrixConnected:(APE_MATRIX*)matrix pinNum:(int)keyNum;
-(void)matrixUnconnect:(APE_MATRIX*)matrix pinNum:(int)keyNum;

@end
