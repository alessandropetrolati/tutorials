/*
cs4dev tutorial 05
by Alessandro Petrolati
www.apesoft.it
*/
<CsoundSynthesizer>
<CsOptions>

-o dac
-+rtmidi=null
-+rtaudio=null
-d
-+msg_color=0
--expression-opt
-M0
-m0
-i adc

</CsOptions>
<CsInstruments>

sr = 44100
ksmps = 64
nchnls = 2
0dbfs = 1

;define 4 x 4 matrix according to APE_MATRIX.h
#define MATRIX_SIZE #4#

/*
if USE_ZAK is definded, the Matrix connections are lazy, suitable for control (k) signals.
if USE_ZAK is un-defined the Matrix connections are awake, suitable for audio (a) signals but more CPU expensive.

N.B.The AudioDSP.m class must declare/undeclare USE_ZAK according to this csd
*/

;#define USE_ZAK ##	;use zak system for GET_CHANNEL (more fast but less accurate)


;-------------------------------------------------------------------------------
;	Output channels

;0	Oscillator sine
;1 	Oscillator saw
;2 	Reverb
;3 	Flanger


#ifdef USE_ZAK

	zakinit  $MATRIX_SIZE*2, 1
#else

	gaMixer[] init $MATRIX_SIZE
	gkMatrix[][] init $MATRIX_SIZE, $MATRIX_SIZE
#end



;------------------------------------------------------------
;to make sure the matrix does not receive MIDI 
;------------------------------------------------------------
massign 1,0

;------------------------------------------------------------
;max allocation for matrix connections $MATRIX_SIZE^2 instances
;------------------------------------------------------------
maxalloc 1, ($MATRIX_SIZE*$MATRIX_SIZE)



;-------------------------------------------------
	opcode GET_MIXER_MASTER, a, i
;-------------------------------------------------

iMixerNumber  xin

#ifdef USE_ZAK
iMixerNumber += $MATRIX_SIZE
	aSumOfChannels	zar iMixerNumber
	zacl iMixerNumber, iMixerNumber
#else
aSumOfChannels = 0
kndx = 0

loop:
    
	if (gkMatrix[iMixerNumber][kndx] > 0) then
		aSumOfChannels += gaMixer[kndx]; * gkMatrix[ichannel][kndx]
	endif

loop_lt kndx, 1, $MATRIX_SIZE, loop
#end

xout aSumOfChannels

	endop
	
	
	
;------------------------------------------------------------
instr 1 ; MATRIX PATCHBOARD
;------------------------------------------------------------

; Y = p4 Channel
; X = p5 Mixer
; p6 = 1 Matrix connected
; p6 = 0 Matrix un-connected

iChannelNumber init p4
iMixerNumber init p5
iMatrixState init p6

#ifdef USE_ZAK
	ain	 zar  iChannelNumber ; from 0 to $MATRIX_SIZE-1
	
	zawm ain, iMixerNumber + $MATRIX_SIZE, 1  ; from $MATRIX_SIZE to ($MATRIX_SIZE*2)-1
#else

	gkMatrix[iMixerNumber][iChannelNumber] = iMatrixState
	turnoff
#end	

endin



;------------------------------------------------------------
instr 10 ; OSC 1 Sine
;------------------------------------------------------------

kcps	chnget "sine_freq"
asig oscili 0.5, kcps

;write Signal to Output Channel 0
ioutputChannel init 0

#ifdef USE_ZAK
	zaw asig, ioutputChannel
#else
	gaMixer[ioutputChannel] = asig
#end

endin



;------------------------------------------------------------
instr 11 ; OSC 2 Saw
;------------------------------------------------------------
 
kcps	chnget "saw_freq"
asig vco 0.5, kcps, 1, 0.5, 1

;write Signal to Output Channel 1
ioutputChannel init 1

#ifdef USE_ZAK
	zaw asig, ioutputChannel
#else
	gaMixer[ioutputChannel] = asig	; write Signal in zak 0 (VCO_1 output 1)
#end
endin



;------------------------------------------------------------
instr 12; Reverb
;------------------------------------------------------------

;receive signal from mixer 2
iMixerNumber init 2
aInputSignal = GET_MIXER_MASTER(iMixerNumber)

a1, a2 reverbsc    aInputSignal * 1.8, aInputSignal * -1.8, 0.85, 9000
arev = a1 + a2

; write Signal to Output Channel 2
ioutputChannel init 2

#ifdef USE_ZAK
	zaw arev, ioutputChannel
#else
	gaMixer[ioutputChannel] = arev
#end

endin



;------------------------------------------------------------
instr 13; Flanger
;------------------------------------------------------------

;receive signal from mixer 3
iMixerNumber init 3
aInputSignal = GET_MIXER_MASTER(iMixerNumber)

kcps	chnget "lfo_hz"
adel oscil 0.001, kcps
adel += 0.001
aflg flanger aInputSignal, adel, 0.8
asig clip aflg, 1, 1

;mix flanger with original
asig += aInputSignal     
     
;write Signal to Output Channel 3
ioutputChannel init 3

#ifdef USE_ZAK
	zaw asig, ioutputChannel
#else
	gaMixer[ioutputChannel] = asig
#end
     
endin



;------------------------------------------------------------
instr 14; OUTPUT
;------------------------------------------------------------

;receive signal from mixer 0
inputSignal init 0
aIn_L = GET_MIXER_MASTER(inputSignal)

;receive signal from mixer 1
inputSignal init 1
aIn_R = GET_MIXER_MASTER(inputSignal)

outs aIn_L, aIn_R
endin

</CsInstruments>
<CsScore>
; Table #1, a sine wave.
f 1 0 65536 10 1

i10 0 999999999
i11 0 999999999
i12 0 999999999
i13 0 999999999
i14 0 999999999

</CsScore>
</CsoundSynthesizer>
