/*
 
 APE_MATRIX.m
 
 This file is part of iVCS3
 Copyright (C) 2013 Alessandro Petrolati
 
 Created by Alessandro Petrolati on 05/13
 
 */


#import "APE_MATRIX.h"
#import "ViewController.h"

@implementation APE_MATRIX


-(void)setDelegate:(id)obj {
    
    _matrixDelegate = obj;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    self.contentScaleFactor = MATRIX_SCALE_FACTOR;
    self.layer.cornerRadius = 10;
    self.layer.masksToBounds = YES;
}

- (void)initialize {
    
    [self setBackgroundColor:[UIColor grayColor]];
    [self setMultipleTouchEnabled:NO];
    [self setAlpha:1.f];
    
    for (int i = 0; i < (MATRIX_SIZE*MATRIX_SIZE); i++) {
        connected[i] = NO;
    }
    
    [self setClipsToBounds:NO];
    
    /* Raw column show indicators */
    lineH = [[UIView alloc] initWithFrame:CGRectZero];
    [self setRoundedView:lineH toDiameter:20];
    
    [lineH setBackgroundColor:[[UIColor redColor] colorWithAlphaComponent:0.7]];
    [self addSubview:lineH];
    [lineH setHidden:YES];
    
    lineW = [[UIView alloc] initWithFrame:CGRectZero];
    [self setRoundedView:lineW toDiameter:20];
    [lineW setBackgroundColor:[[UIColor redColor] colorWithAlphaComponent:0.7]];
    [self addSubview:lineW];
    [lineW setHidden:YES];
    
    
    //    unconnectImage = nil;//[[UIImage imageNamed:@"matrix_bk.png"] retain];
    //    connectImage = [[UIImage imageNamed:@"pin_center.png"] retain];
    
    [self updatePinsRects];
    [self performSelectorOnMainThread:@selector(setNeedsDisplay) withObject:nil waitUntilDone:NO];
    
    
    // Pinch for reset default values
    UIPinchGestureRecognizer* swipeRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self
                                                                                          action:@selector(gestureMethod:)];
    
    
    // SwipeRecognizer.direction = UISwipeGestureRecognizerDirectionRight | UISwipeGestureRecognizerDirectionLeft;
    swipeRecognizer.cancelsTouchesInView = YES;
    swipeRecognizer.delaysTouchesBegan = NO;
    swipeRecognizer.delaysTouchesEnded = NO;
    
    [self addGestureRecognizer:swipeRecognizer];
}

-(void)gestureMethod: (UIPinchGestureRecognizer *) sender {
    for (int i = 0; i < (MATRIX_SIZE*MATRIX_SIZE); i++)
        
        if (connected[i] && [sender state] == UIGestureRecognizerStateEnded) {
            
            [lineH setHidden:YES];
            [lineW setHidden:YES];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertController *alertController = [UIAlertController
                                                      alertControllerWithTitle:@"Matrix"
                                                      message:@"Do you want remove all connections?"
                                                      preferredStyle:UIAlertControllerStyleAlert];
                
                
                [alertController addAction:[UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
                    [self->lineH setHidden:YES];
                    [self->lineW setHidden:YES];
                    
                    [self clear];
                }]];
                
                [alertController addAction:[UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                    
                }]];
                
                [[ViewController topMostController] presentViewController:alertController animated:YES completion:nil];
            });
            
            return;
        }
    
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self) {
        [self initialize];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        [self initialize];
    }
    return self;
}


//-(void)updatePinsRects {
//
//    CGFloat rowAndColumns = MATRIX_SIZE;
//	CGFloat pinWidth = self.bounds.size.width / rowAndColumns;
//	CGFloat pinHeight = self.bounds.size.height / rowAndColumns;
//
//	int lastHorizontalPin = 0;
//    int lastVerticalPin = 0;
//
//	matrixRects[0] = CGRectMake(0, 0, pinWidth, pinHeight);
//
//	for (int i = 1; i < (MATRIX_SIZE*MATRIX_SIZE); i++) {
//
//        lastHorizontalPin++;
//        if (lastHorizontalPin > (rowAndColumns - 1)) {
//            lastHorizontalPin = 0;
//            lastVerticalPin++;
//        }
//        matrixRects[i] = CGRectMake(lastHorizontalPin * pinWidth, lastVerticalPin * pinHeight, pinWidth, pinHeight);
//
//	}
//}

-(void)updatePinsRects {
    
    CGFloat rowAndColumns = MATRIX_SIZE;
    
    CGFloat pinWidth = self.bounds.size.width / rowAndColumns;
    CGFloat pinHeight = self.bounds.size.height / rowAndColumns;
    
    int lastHorizontalPin = 0;
    int lastVerticalPin = 0;
    
    //	matrixRects[0] = CGRectMake(0, 0, pinWidth, pinHeight);
    
    for (int i = 0; i < (MATRIX_SIZE*MATRIX_SIZE); i++) {
        
        matrixRects[i] = CGRectMake(lastHorizontalPin * pinWidth, lastVerticalPin * pinHeight, pinWidth, pinHeight);
        
        lastHorizontalPin++;
        
        if (lastHorizontalPin > MATRIX_SIZE-1) {
            lastHorizontalPin = 0;
            lastVerticalPin++;
        }
        
    }
}

-(int)getMatrixPin:(CGPoint)point {
    
    int pinNum = -1;
    
    for(int i = 0; i < (MATRIX_SIZE*MATRIX_SIZE); i++) {
        if (CGRectContainsPoint(matrixRects[i], point)) {
            pinNum = i;
        }
    }
    
    return pinNum;
}

#pragma mark Touch Handling Code


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [self performSelector:@selector(drawCoordinates:) withObject:event afterDelay:0.1];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [self performSelector:@selector(drawCoordinates:) withObject:event afterDelay:0.1];
}

-(void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [lineH setHidden:YES];
    [lineW setHidden:YES];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [lineH setHidden:YES];
    [lineW setHidden:YES];
    
    int index = -1;
    NSSet *allTouches = [event touchesForView:self];
    
    for (int i = 0; i < [allTouches count]; i++) {
        UITouch *touch = [[allTouches allObjects] objectAtIndex:i];
        //    for (UITouch* touch in touches) {
        CGPoint pt = [touch locationInView:self];
        
        index = [self getMatrixPin:pt];
        
        if (index == -1)
            return;
        
        if (!connected[index] /*&& (index !=  -1)*/) {
            connected[index] = YES;
            [_matrixDelegate matrixConnected:self pinNum:index];
            [self setNeedsDisplayInRect:matrixRects[index]];
            lastConnection = index;
        }
        else {
            if ([allTouches count] >= 1) { //da modificare!!!
                connected[index] = NO;
                
                [_matrixDelegate matrixUnconnect:self pinNum:index];
                [self setNeedsDisplayInRect:matrixRects[index]];
            }
        }
    }
    
    [self syncronizeRecovery];
}
#pragma mark Drawing Code

- (void)drawRect:(CGRect)rect
{
    // Get the context
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetAllowsAntialiasing(context, YES);
    CGContextSetFillColorWithColor(context, [UIColor blackColor].CGColor);
    CGContextSetStrokeColorWithColor(context, [UIColor blackColor].CGColor);
    
    CGFloat gap = self.frame.size.width / MATRIX_SIZE;
    
    //#define DRAW_AS_GRID
    
#ifdef DRAW_AS_GRID
    //  Draw as Grid
    for (int i = 0; i < MATRIX_SIZE; ++i) {
        
        CGFloat vert = (i * gap) + (gap/2);
        CGContextMoveToPoint(context, vert, 0);
        CGContextAddLineToPoint(context, vert, self.frame.size.height);
        CGContextStrokePath(context);
        
        CGFloat hor = (i * gap) + (gap/2);
        CGContextMoveToPoint(context, 0, hor);
        CGContextAddLineToPoint(context, self.frame.size.width, hor);
        CGContextStrokePath(context);
    }
#else
    // Draw a Circles
    for (int i = 0; i < (MATRIX_SIZE*MATRIX_SIZE); i++) {
        CGRect nRect = CGRectInset(matrixRects[i], gap/16.0, gap/16.0);
        CGContextAddEllipseInRect(context, nRect);
        CGContextStrokePath(context);
    }
#endif
    
    CGContextSetFillColorWithColor(context, [UIColor redColor].CGColor);
    
    // Draw Matrix Pins
    for (int i = 0; i < (MATRIX_SIZE*MATRIX_SIZE); i++) {
        if (connected[i]) {
            
            int x, y;
            
            x = i % MATRIX_SIZE;
            y = i / MATRIX_SIZE;
            
            CGRect nRect = CGRectInset(matrixRects[i], gap/8.0, gap/8.0);
            CGContextAddEllipseInRect(context, nRect);
            CGContextFillPath(context);
            //or use image for pin
            //            [[UIImage imageNamed:@"myPinImage.png"] drawInRect:matrixRects[i]];
        }
    }
}

#pragma mark save and restore keyboard state and various

-(NSMutableArray*)getState {
    
    NSMutableArray* connectionState = [[NSMutableArray alloc] initWithCapacity:(MATRIX_SIZE*MATRIX_SIZE)*2];
    
    for (int i = 0; i < (MATRIX_SIZE*MATRIX_SIZE); i++)
        [connectionState insertObject:[NSNumber numberWithBool:connected[i]] atIndex:i];
    
    return connectionState;
}

-(void)restoreState:(NSMutableArray*)keyState {
    
    [self clear];
    
    for (int i = 0; i < (MATRIX_SIZE*MATRIX_SIZE); i++) {
        connected[i] = [[keyState objectAtIndex:i] boolValue];
    }
    
    for (int i = 0; i < (MATRIX_SIZE*MATRIX_SIZE); i++) {
        
        if (connected[i]) {
            [_matrixDelegate matrixConnected:self pinNum:i];
            [self setNeedsDisplayInRect:matrixRects[i]];
        }
        
        else
        {
            if(connected[i]) {
                [_matrixDelegate matrixUnconnect:self pinNum:i];
                [self setNeedsDisplayInRect:matrixRects[i]];
            }
        }
    }
    
    [self syncronizeRecovery];
}


-(void)saveAsDefaults {
    
    NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
    [preferences setObject:[self getState] forKey:@"MATRIX_STATE"];
}

-(void)loadAsDefaults {
    
    NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
    
    NSArray *retrieved = [preferences arrayForKey:@"MATRIX_STATE"];
    if (retrieved) {
        NSMutableArray* array = [NSMutableArray arrayWithArray:retrieved];
        
        if(array)
            [self restoreState:array];
    }
}

-(void)refresh {
    
    for (int i = 0; i < (MATRIX_SIZE*MATRIX_SIZE); i++) {
        if (_matrixDelegate != nil) {
            if (connected[i]) {
                [_matrixDelegate matrixConnected:self pinNum:i];
                [self setNeedsDisplayInRect:matrixRects[i]];
            }
            else {
                [_matrixDelegate matrixUnconnect:self pinNum:i];
                [self setNeedsDisplayInRect:matrixRects[i]];
            }
        }
    }
    
    [self syncronizeRecovery];}

-(void)clear {
    
    for (int i = 0; i < (MATRIX_SIZE*MATRIX_SIZE); i++) {
        if (connected[i]){
            [_matrixDelegate matrixUnconnect:self pinNum:i];
            connected[i] = NO;
        }
    }
    
    [self performSelectorOnMainThread:@selector(setNeedsDisplay) withObject:nil waitUntilDone:NO];
    
    [self syncronizeRecovery];
}


-(void)syncronizeRecovery {
    
    //    [_recovery syncronize:[self getConnectionsWhite] andRed:[self getConnectionsRed] andGreen:[self getConnectionsGreen]];
    
}

-(int)getConnectionsWhite {
    
    int num = 0;
    for (int i = 0; i < (MATRIX_SIZE*MATRIX_SIZE); i++) {
        if(connected[i])
            num++;
    }
    return num;
}

-(BOOL)getPinState:(int)ndx {
    
    if (ndx < (MATRIX_SIZE*MATRIX_SIZE) && ndx >= 0)
        return connected[ndx];
    else
        return NO;
}

#pragma mark - Coordinates APE method
-(void)drawCoordinates:(UIEvent *)event {
    
    NSSet *allTouches = [event touchesForView:self];
    
    for (int i = 0; i < [allTouches count]; i++) {
        UITouch *touch = [[allTouches allObjects] objectAtIndex:i];
        CGPoint pt = [touch locationInView:self];
        
        int index = [self getMatrixPin:pt];
        //        float Vindex = (float) index * (self.frame.size.width/MATRIX_SIZE);
        
        if (index == -1)
            return;
        
        //        float size = 10;
        float offset = (self.frame.size.width/MATRIX_SIZE);
        int x, y;
        
        x = index % MATRIX_SIZE;
        y = index / MATRIX_SIZE;
        
        x *= offset;
        x += offset/2;
        
        y *= offset;
        y += offset/2;
        
        
        [lineH setHidden:NO];
        //        [lineH setFrame:CGRectMake(-size*1.5, y, size, size)];
        [lineH setFrame:CGRectMake(-5, y, self.frame.size.width+10, 2)];
        
        [lineW setHidden:NO];
        //        [lineW setFrame:CGRectMake(x, -size*1.5, size, size)];
        [lineW setFrame:CGRectMake(x, -5, 2, self.frame.size.height+10)];
    }
}

#pragma mark - Various Graphics Methods
-(void)setRoundedView:(UIView *)roundedView toDiameter:(float)newSize {
    CGPoint saveCenter = roundedView.center;
    CGRect newFrame = CGRectMake(roundedView.frame.origin.x, roundedView.frame.origin.y, newSize, newSize);
    roundedView.frame = newFrame;
    roundedView.layer.cornerRadius = newSize / 2.0;
    roundedView.center = saveCenter;
}

@end
