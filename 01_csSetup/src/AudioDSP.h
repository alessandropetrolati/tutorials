//
//  AudioDSP.h
//  01_csSetup
//
//  Created by Alessandro Petrolati on 24/05/15.
//  Copyright (c) 2015 apeSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioServices.h>
#import <AudioUnit/AudioUnit.h>
#import <AVFoundation/AVFoundation.h>

#import "csound.h"

//#define ENABLE_BLUETOOTH
#define ENABLE_MIDI

@interface AudioDSP : NSObject {
    
    /* Audio IO */
    AudioUnit csAUHAL;
    OSStatus err;
    ExtAudioFileRef recFile;
    
    /* Csound data */
    long bufframes;
    int ret;
    int nchnls;
    int counter;
    bool running;
    bool shouldRecord;
    
    /* User Interface */
    IBOutlet UISlider* freq;
    IBOutlet UISwitch* rec;
    IBOutlet UILabel* hzLabel;
}
@property(nonatomic, readwrite) CSOUND* cs;

@end
