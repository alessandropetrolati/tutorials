/*
cs4dev tutorial 01
by Alessandro Petrolati
www.apesoft.it
*/

<CsoundSynthesizer>
<CsOptions>

-o dac
-+rtmidi=null
-+rtaudio=null
-d
-+msg_color=0
--expression-opt
-M0
-m0
-i adc

</CsOptions>
<CsInstruments>

sr = 44100
ksmps = 64
nchnls = 2
0dbfs = 1

instr 1
kfr chnget "freq"
;kam chnget "amp"
kam = 0.7

a1 oscili kam, kfr
outs a1,a1
endin

</CsInstruments>
<CsScore>

i 1 0 10000

</CsScore>
</CsoundSynthesizer>
