//
//  main.m
//  MainAppAUv3
//
//  Created by Alessandro Petrolati on 06/10/2019.
//  Copyright © 2019 Alessandro Petrolati. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
