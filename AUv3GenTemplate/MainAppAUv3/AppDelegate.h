//
//  AppDelegate.h
//  MainAppAUv3
//
//  Created by Alessandro Petrolati on 06/10/2019.
//  Copyright © 2019 Alessandro Petrolati. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

