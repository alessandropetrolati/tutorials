//
//  AudioUnitViewController.h
//  AUExtension
//
//  Created by Alessandro Petrolati on 10/08/16
//  Copyright (c) 2016 apeSoft. All rights reserved.
//

#import <CoreAudioKit/AUViewController.h>

@class MyAudioUnit;

@interface AudioUnitViewController : AUViewController
@property (nonatomic) MyAudioUnit *audioUnit;
@property (nonatomic, readonly) NSArray* paramChildrens;

-(NSDictionary*)getState;
-(void)restoreState:(NSDictionary*)state;
-(void)refreshParameters;
-(void)midiNoteOn:(int)note :(int)veloc;
@end
