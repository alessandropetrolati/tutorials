//
//  MyAudioUnit.h
//  AUExtension
//
//  Created by Alessandro Petrolati on 13/08/16.
//  Copyright © 2016 apeSoft. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import <Accelerate/Accelerate.h>
#import "oscillatore.h"
#import "volumeAndGain.h"
#import "drywet.h"
#import <vector>

#define SAMPLE_RATE_DEF 44100.0
#define BUFFER_FRAME 256
#define CHANNELS_NUM 2
#define EXTREME_BUFFER_SIZE 4096

enum {
    
    freq_INDEX = 0,
    volume_INDEX = 1,
    drywet_INDEX = 2,
    micInput_INDEX = 3,
    // APE_ADD_PARAMETER 0
};

@class AudioUnitViewController;

struct audio_struct
{
    double freqValue;
    double volumeValue;
    double drywetValue;
    bool micInputValue;
    // APE_ADD_PARAMETER 10
    
    CommonState* oscil[2];
    CommonState* volAndGain;
    CommonState* drywet;
    
    std::vector<t_sample *> inputBuffers;
    std::vector<t_sample *> outputBuffers;
    std::vector<t_sample *> tmpBuffers;
};

@interface MyAudioUnit : AUAudioUnit /*AUAudioUnitV2Bridge*/ {
    
    struct audio_struct data;
    
}
-(void)setController:(AudioUnitViewController*)ctrl andParameters:(NSArray*)childrens;
-(void)createParametersTree:(NSArray*)childrens;
@end
