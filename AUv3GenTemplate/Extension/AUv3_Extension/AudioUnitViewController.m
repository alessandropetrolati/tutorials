//
//  AudioUnitViewController.m
//  AUExtension
//
//  Created by Alessandro Petrolati on 10/08/16
//  Copyright (c) 2016 apeSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

#import "AudioUnitViewController.h"
#import "MyAudioUnit.h"

@interface AudioUnitViewController ()
{
    
    /* UI Widgets */
    IBOutlet UISlider *freqSlider;
    IBOutlet UISlider *volumeSlider;
    IBOutlet UISlider *drywetSlider;
    IBOutlet UISwitch *inputFromMicSwitch;
    // APE_ADD_PARAMETER 1
    
    IBOutlet UILabel* hzLabel;
    IBOutlet UILabel *volumeValueLabel;
    IBOutlet UILabel *drywetValueLabel;
    
    /* AUParameters */
    AUParameter *freq_Parameter;
    AUParameter *volumeSlider_Parameter;
    AUParameter *drywetSlider_Parameter;
    AUParameter *inputFromMic_Parameter;
    // APE_ADD_PARAMETER 2
}

@end

@implementation AudioUnitViewController

@synthesize paramChildrens = _paramChildrens;

-(void)setAudioUnit:(MyAudioUnit *)audioUnit
{
    _audioUnit = audioUnit;
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if ([self isViewLoaded]) {
            [self connectViewWithAU];
        }
    });
}

-(MyAudioUnit *)getAudioUnit
{
    return _audioUnit;
}

#pragma mark - AUAudioUnit owerride, suitable for saving as a user preset from/to Host
-(NSDictionary*)getState {
    
    NSMutableDictionary* connectionState = [[NSMutableDictionary alloc] init];

    /* SAVE WIDGETS VALUE */
    [connectionState setObject:[NSNumber numberWithFloat:freqSlider.value]  forKey:@"freqSlider_channel"];
    [connectionState setObject:[NSNumber numberWithFloat:volumeSlider.value]  forKey:@"volumeSlider_channel"];
    [connectionState setObject:[NSNumber numberWithFloat:drywetSlider.value]  forKey:@"drywetSlider_channel"];
    [connectionState setObject:[NSNumber numberWithBool:inputFromMicSwitch.on]  forKey:@"inputFromMicSwitch_channel"];
    // APE_ADD_PARAMETER 3
    
    return (NSDictionary*)connectionState;
}

-(void)restoreState:(NSDictionary*)state {
    
    if (!state) {
        return;
    }
    [self resizeWidgets];
    
    dispatch_sync(dispatch_get_main_queue(), ^{
        
        NSNumber *val = nil;
        
        /* RESTORE WIDGETS VALUE */
        if((val = [state objectForKey:@"freqSlider_channel"])) { freqSlider.value = [val floatValue]; }
        if((val = [state objectForKey:@"volumeSlider_channel"])) { volumeSlider.value = [val floatValue]; }
        if((val = [state objectForKey:@"drywetSlider_channel"])) { drywetSlider.value = [val floatValue]; }
        if((val = [state objectForKey:@"inputFromMicSwitch_channel"])) { inputFromMicSwitch.on = [val boolValue]; }
        // APE_ADD_PARAMETER 4
        
        [self refreshParameters];
        
    });
}

-(void)refreshParameters {
    
    // Warning Don't use NSNotifications here!
    
    [freqSlider sendActionsForControlEvents:UIControlEventValueChanged];
    [volumeSlider sendActionsForControlEvents:UIControlEventValueChanged];
    [drywetSlider sendActionsForControlEvents:UIControlEventValueChanged];
    [inputFromMicSwitch sendActionsForControlEvents:UIControlEventAllEvents];
    // APE_ADD_PARAMETER 5
}

- (void)connectViewWithAU
{
    AUParameterTree *parameterTree = self.audioUnit.parameterTree;
    
    if (parameterTree) {
        
        __block AudioUnitViewController* _self = self;
        
        [parameterTree tokenByAddingParameterObserver:^(AUParameterAddress address, AUValue value) {
            dispatch_sync(dispatch_get_main_queue(), ^{
                
                //                NSLog(@"tokenByAddingParameterObserver -> %llu -> %f", address, value);
                
                /* Update DSP Parameter */
                switch (address) {
                        
                    case freq_INDEX:
                        _self->freqSlider.value = value;
                        break;
                        
                    case volume_INDEX:
                        _self->volumeSlider.value = value;
                        break;
                                                
                    case drywet_INDEX:
                        _self->drywetSlider.value = value;
                        break;
                        
                    case micInput_INDEX:
                        
                        BOOL state = (BOOL)value;
                        
                        if(_self->inputFromMicSwitch.on != state) {
                            _self->inputFromMicSwitch.on = state;
                        }
                        
                        break;
                        // APE_ADD_PARAMETER 6
                        
                }
            });
        }];
    }
}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    
    /* Resize View According to Host View*/
    CGFloat scale = self.view.bounds.size.width / 1024.0;
    
    self.view.transform = CGAffineTransformMakeScale(scale, scale);
    
    [self.view setFrame:self.view.bounds];
    
    [self resizeWidgets];
    [self refreshParameters];
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    [self addObserver:self forKeyPath:@"view.bounds" options:NSKeyValueObservingOptionOld context:NULL];
    
    
    // Create the parameter tree.
    
    AudioUnitParameterOptions flags = kAudioUnitParameterFlag_IsWritable | kAudioUnitParameterFlag_IsReadable | kAudioUnitParameterFlag_IsGlobalMeta;
    
    freq_Parameter = [AUParameterTree createParameterWithIdentifier:@"freqSlider_channel" name:@"Oscillator Frequency"
                                                            address:freq_INDEX
                                                                min:freqSlider.minimumValue
                                                                max:freqSlider.maximumValue
                                                               unit:kAudioUnitParameterUnit_Generic
                                                           unitName:nil
                                                              flags:flags
                                                       valueStrings:nil
                                                dependentParameters:nil];
    
    
    volumeSlider_Parameter = [AUParameterTree createParameterWithIdentifier:@"volumeSlider_channel" name:@"Input Gain"
                                                                    address:volume_INDEX
                                                                        min:volumeSlider.minimumValue
                                                                        max:volumeSlider.maximumValue
                                                                       unit:kAudioUnitParameterUnit_Generic
                                                                   unitName:nil
                                                                      flags:flags
                                                               valueStrings:nil
                                                        dependentParameters:nil];
    
    
    
    drywetSlider_Parameter = [AUParameterTree createParameterWithIdentifier:@"drywetSlider_channel" name:@"Dry/Wet"
                                                                    address:drywet_INDEX
                                                                        min:drywetSlider.minimumValue
                                                                        max:drywetSlider.maximumValue
                                                                       unit:kAudioUnitParameterUnit_Generic
                                                                   unitName:nil
                                                                      flags:flags
                                                               valueStrings:nil
                                                        dependentParameters:nil];
    
    inputFromMic_Parameter = [AUParameterTree createParameterWithIdentifier:@"inputFromMicSwitch_channel" name:@"Input From Mic/IAA/AB"
                                                                    address:micInput_INDEX
                                                                        min:0
                                                                        max:1
                                                                       unit:kAudioUnitParameterUnit_Boolean
                                                                   unitName:nil
                                                                      flags:flags
                                                               valueStrings:nil
                                                        dependentParameters:nil];
    // APE_ADD_PARAMETER 7
    
    // Create the parameter tree.
    _paramChildrens = @[freq_Parameter, volumeSlider_Parameter, drywetSlider_Parameter, inputFromMic_Parameter]; // APE_ADD_PARAMETER 8
    
    
    if (_audioUnit)
    {
        [self connectViewWithAU];
    }
    
    
    [self refreshParameters];
}


- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext>  _Nonnull context) {
        
        [self resizeWidgets];
        
    } completion:^(id<UIViewControllerTransitionCoordinatorContext>  _Nonnull context) {
        
        [self resizeWidgets];
    }];
}

- (void)dealloc {
    
    [self removeObserver:self forKeyPath:@"view.bounds"];
}

-(void)resizeWidgets {
    
    [self.view setNeedsDisplay];
    
    for (id w in self.view.subviews) {
        
        if ([w isKindOfClass:[UIView class]]) {
            
            UIView* r = w;
            for (UIView* r_ in r.subviews) {
                if ([r_ respondsToSelector:@selector(setNeedsDisplay)]) {
                    [r_ setNeedsDisplay];
                }
            }
        }
        
        if ([w respondsToSelector:@selector(setNeedsDisplay)])
            [w setNeedsDisplay];
    }
}


#pragma MIDI Route
-(void)midiNoteOn:(int)note :(int)veloc {
    
    //    float valNote = (float) note;
    //    float valVeloc = (float) veloc;
    
    //parse incoming MIDI events here!
}

#pragma  mark - User Interface Widgets -> DSP

-(IBAction)frequencySliderSlided:(UISlider*)sender {
    
    [hzLabel setText:[NSString stringWithFormat:@"Frequency = %f", sender.value]];
    
    freq_Parameter.value = sender.value;
}

-(IBAction)volumeSliderSlided:(UISlider *)sender {
    
    [volumeValueLabel setText:[NSString stringWithFormat:@"Gain = %.3f", sender.value]];
    
    volumeSlider_Parameter.value = sender.value;
}

-(IBAction)drywetSliderSlided:(UISlider *)sender {
    
    [drywetValueLabel setText:[NSString stringWithFormat:@"Dry/Wet = %.3f", sender.value]];
    
    drywetSlider_Parameter.value = sender.value;
}

-(IBAction)inputFromMicSwitch:(UISwitch *)sender {
    
    [freqSlider setAlpha:(sender.on ? 0.33 : 1.0)];
    
    inputFromMic_Parameter.value = (double) sender.on;
}
// APE_ADD_PARAMETER 9
@end
