//
//  MyAudioUnit.mm
//  AUExtension
//
//  Created by Alessandro Petrolati on 13/08/16.
//  Copyright © 2016 apeSoft. All rights reserved.
//

#import "MyAudioUnit.h"

#import "BufferedAudioBus.hpp"
#import "AudioUnitViewController.h"

static const NSInteger kDefaultFactoryPreset = 0;

static AUAudioUnitPreset* NewAUPreset(NSInteger number, NSString *name)
{
    AUAudioUnitPreset *aPreset = [AUAudioUnitPreset new];
    aPreset.number = number;
    aPreset.name = name;
    return aPreset;
}

@interface MyAudioUnit ()
@property AUAudioUnitBus *outputBus;
@property AUAudioUnitBusArray *inputBusArray;
@property AUAudioUnitBusArray *outputBusArray;
@property AUParameterTree *parameterTree;

@end

@implementation MyAudioUnit
{
    // C++ members need to be ivars; they would be copied on access if they were properties.
    BufferedInputBus _inputBus;
    AUHostMusicalContextBlock _musicalContext;
    
    __block AudioUnitViewController* controller;
    
    Float64 CURRENT_SAMPLING_RATE;
    Float64 CURRENT_BUFFER_FRAME;
    
    // Factory Presets
    AUAudioUnitPreset   *_currentPreset;
    NSInteger           _currentFactoryPresetIndex;
    NSArray<AUAudioUnitPreset *> *_presets;
    NSMutableArray* presetParameters;
}
@synthesize parameterTree = _parameterTree;
@synthesize factoryPresets = _presets;

- (void)setController:(AudioUnitViewController*)ctrl andParameters:(NSArray*)childrens {
    
    if (!ctrl) {
        return;
    }
    
    controller = ctrl;
    
    [self createParametersTree:childrens];
    
    // set default preset as current
    // must be called with non null 'controller' ptr
    //    self.currentPreset = _presets.firstObject;
    
    [ctrl refreshParameters];
}

#pragma mark - AUAudioUnit owerride, suitable for saving as a user preset from/to Host

//#define MAYBE_BUG_?

#ifdef MAYBE_BUG_?
- (NSDictionary<NSString *, id> *)getFullState {
    NSDictionary* pluginState = [controller getState];
    NSDictionary* superState = [super fullState];
    
    NSMutableDictionary* stateRes = [NSMutableDictionary dictionaryWithDictionary:pluginState];
    [stateRes addEntriesFromDictionary:superState];
    
    return stateRes;
}
#else
- (NSDictionary<NSString *,id> *)fullState {
    return [controller getState];
}
#endif

- (void)setFullState:(NSDictionary<NSString *,id> *)state {

    [controller restoreState:state];
}

- (NSDictionary<NSString *,id> *)fullStateForDocument {

    return [controller getState];
}

- (void)setFullStateForDocument:(NSDictionary<NSString *,id> *)state {

    [controller restoreState:state];
}

- (instancetype)initWithComponentDescription:(AudioComponentDescription)componentDescription
                                     options:(AudioComponentInstantiationOptions)options
                                       error:(NSError **)outError
{
    self = [super initWithComponentDescription:componentDescription options:options error:outError];
    
    if (self == nil)
    {
        return nil;
    }
    
    [self allocateLocalResources];
    
    // Initialize a default format for the busses.
    AVAudioFormat *defaultFormat = [[AVAudioFormat alloc] initStandardFormatWithSampleRate:SAMPLE_RATE_DEF channels:CHANNELS_NUM];
    
    // Create the input and output busses.
    _inputBus.init(defaultFormat, 8);
//    _inputBus = [[AUAudioUnitBus alloc] initWithFormat:defaultFormat error:nil];
    _outputBus = [[AUAudioUnitBus alloc] initWithFormat:defaultFormat error:nil];
    
    // Create the input and output bus arrays.
    _inputBusArray  = [[AUAudioUnitBusArray alloc] initWithAudioUnit:self busType:AUAudioUnitBusTypeInput busses: @[_inputBus.bus]];
    _outputBusArray = [[AUAudioUnitBusArray alloc] initWithAudioUnit:self busType:AUAudioUnitBusTypeOutput busses: @[_outputBus]];
    
    self.maximumFramesToRender = 512;
    
    // Create factory preset array.
    [self makeFactoryPresets];
    
    return self;
}

- (void)createParametersTree:(NSArray*)childrens {
    
    // Create the parameter tree.
    _parameterTree = [AUParameterTree createTreeWithChildren:childrens];
    //    __parameterTree__ = _parameterTree;
    
    __block struct audio_struct* data_ = &data;
    __block MyAudioUnit* self_ = self;
    
    // implementorValueObserver is called when a parameter changes value.
    _parameterTree.implementorValueObserver = ^(AUParameter *param, AUValue value) {
        
        switch (param.address) {
                
            case freq_INDEX:
                [self_ setFrequencyValue:value];
                break;
                
            case volume_INDEX:
                [self_ setVolumeValue:value];
                break;

            case drywet_INDEX:
                [self_ setDrywetValue:value];
                break;
                
            case micInput_INDEX:
                [self_ setMicInputValue:value > 0.5 ? true : false];
                break;
                
                // APE_ADD_PARAMETER 11
                
        }
    };
    
    
    // implementorValueProvider is called when the value needs to be refreshed.
    _parameterTree.implementorValueProvider = ^(AUParameter *param) {
        
        AUValue value = 0.0;
        
        switch (param.address) {
                
            case freq_INDEX:
                value = data_->freqValue;
                break;
                
            case volume_INDEX:
                value = data_->volumeValue;
                break;
                
            case drywet_INDEX:
                value = data_->drywetValue;
                break;
                
            case micInput_INDEX:
                value = data_->micInputValue;
                break;
                
                // APE_ADD_PARAMETER 12
                
        }
        return value;
    };
}

- (void)allocateLocalResources {
    
    data.freqValue = 0.0;
    data.volumeValue = 0.0;
    data.drywetValue = 0.0;
    
    /* Allocate DSP gen~ */
    for (int i = 0; i < 2; ++i) {
        data.oscil[i] = (CommonState *)oscillatore::create(SAMPLE_RATE_DEF, BUFFER_FRAME);
    }
    
    data.volAndGain = (CommonState *)volumeAndGain::create(SAMPLE_RATE_DEF, BUFFER_FRAME);
    data.drywet = (CommonState *)drywet::create(SAMPLE_RATE_DEF, BUFFER_FRAME);
    
    /* Allocate Buffers */
    int i;
    for(i=0; i < oscillatore::num_inputs(); ++i) {
        data.inputBuffers.push_back(new t_sample[EXTREME_BUFFER_SIZE]);
    }
    for(i=0; i < oscillatore::num_outputs(); ++i) {
        data.outputBuffers.push_back(new t_sample[EXTREME_BUFFER_SIZE]);
    }
    for(i=0; i < drywet::num_inputs(); ++i) {
        data.tmpBuffers.push_back(new t_sample[EXTREME_BUFFER_SIZE]);
    }
    
    /* Clear Buffer */
    for(int c = 0; c < oscillatore::num_inputs(); ++c)
        memset(data.inputBuffers[c], 0.0, EXTREME_BUFFER_SIZE);
    
    for(int c = 0; c < oscillatore::num_outputs(); ++c)
        memset(data.outputBuffers[c], 0.0, EXTREME_BUFFER_SIZE);
    
    for(int c = 0; c < drywet::num_outputs(); ++c)
        memset(data.tmpBuffers[c], 0.0, EXTREME_BUFFER_SIZE);
}

- (AUAudioUnitBusArray *)inputBusses {
    return _inputBusArray;
}

- (AUAudioUnitBusArray *)outputBusses {
    return _outputBusArray;
}

- (BOOL)allocateRenderResourcesAndReturnError:(NSError **)outError {
    
    if (![super allocateRenderResourcesAndReturnError:outError]) {
        return NO;
    }
    
    if (self.outputBus.format.channelCount != _inputBus.bus.format.channelCount) {
        if (outError) {
            *outError = [NSError errorWithDomain:NSOSStatusErrorDomain code:kAudioUnitErr_FailedInitialization userInfo:nil];
        }
        // Notify superclass that initialization was not successful
        self.renderResourcesAllocated = NO;
        
        return NO;
    }
    
    _inputBus.allocateRenderResources(self.maximumFramesToRender);
    
    //Tempo provided by host?
    if (self.musicalContextBlock) { _musicalContext = self.musicalContextBlock; } else _musicalContext = nil;
    
    // Initialize a default format for the busses.
    AVAudioSession *session = [AVAudioSession sharedInstance];
    NSTimeInterval bufferDurationVerbose = session.IOBufferDuration;
    
    Float32 srVerbose = [AVAudioSession sharedInstance].sampleRate;
    Float64 bufferFrameVerbose = roundf(bufferDurationVerbose * srVerbose);
    //    Float64 latency = (bufferFrameVerbose / srVerbose) * 1000.f;
    
    CURRENT_SAMPLING_RATE = srVerbose;
    CURRENT_BUFFER_FRAME = bufferFrameVerbose;
    
    //    NSLog(@"%@", [NSString stringWithFormat:@"Sampling Rate %.0f; Buffer Size %d; Latency %.3f msec. (large buffer increase latency but reduce CPU)",
    //                  CURRENT_SAMPLING_RATE, (int)CURRENT_BUFFER_FRAME, latency]);
    
    /* Apply SR and BUFFER to DSPs */
    for (int i = 0; i < 2; ++i) {
        data.oscil[i]->vs = CURRENT_BUFFER_FRAME;
        data.oscil[i]->sr = CURRENT_SAMPLING_RATE;
    }
    
    data.volAndGain->vs = CURRENT_BUFFER_FRAME;
    data.volAndGain->sr = CURRENT_SAMPLING_RATE;
    
    data.drywet->vs = CURRENT_BUFFER_FRAME;
    data.drywet->sr = CURRENT_SAMPLING_RATE;
    
    for (int i = 0; i < 2; ++i) {
        oscillatore::reset(data.oscil[i]);
    }
    
    volumeAndGain::reset(data.volAndGain);
    drywet::reset(data.drywet);
    
    if (controller) {
        
        [controller refreshParameters];
    }
    
    return YES;
}

- (void)deallocateRenderResources {
    
    _inputBus.deallocateRenderResources();
    
    [super deallocateRenderResources];
}

-(void)dealloc {
    
    for (int i = 0; i < 2; ++i) {
        oscillatore::destroy((CommonState *)data.oscil[i]);
        data.oscil[i] = NULL;
    }
    
    volumeAndGain::destroy((CommonState *)data.volAndGain);
    data.volAndGain = NULL;

    drywet::destroy((CommonState *)data.drywet);
    
    int i;
    for(i=0; i < data.inputBuffers.size(); ++i) {
        delete[] data.inputBuffers[i];
    }
    for(i=0; i < data.outputBuffers.size(); ++i) {
        delete[] data.outputBuffers[i];
    }
    for(i=0; i < data.tmpBuffers.size(); ++i) {
        delete[] data.tmpBuffers[i];
    }
    
    _presets = nil;
    presetParameters = nil;
}

- (AUInternalRenderBlock)internalRenderBlock {
    
    //    NSLog(@"-------------------------------------------------------------%s", __func__);
    
    /*
     Capture in locals to avoid ObjC member lookups. If "self" is captured in
     render, we're doing it wrong.
     */
    
    __block MyAudioUnit *self_ = self;
    __block BufferedInputBus *input = &_inputBus;
    __block struct audio_struct* data_ = &data;
    
    return ^AUAudioUnitStatus(
                              AudioUnitRenderActionFlags *actionFlags,
                              const AudioTimeStamp       *timestamp,
                              AVAudioFrameCount           frameCount,
                              NSInteger                   outputBusNumber,
                              AudioBufferList            *outputData,
                              const AURenderEvent        *realtimeEventListHead,
                              AURenderPullInputBlock      pullInputBlock) {
        
        AudioUnitRenderActionFlags pullFlags = 0;
        
        AUAudioUnitStatus err = input->pullInput(&pullFlags, timestamp, frameCount, 0, pullInputBlock);
        
        if (err != 0) { return err; }
        
        /* Get tempo from host */
        double currentTempo;
        static double lastTempoValue = -1;
        
        if ( self_->_musicalContext ) { // only use this if the host supports it...
            if (self_->_musicalContext( &currentTempo, NULL, NULL, NULL, NULL, NULL ) ) {
                //            plugin->handleTempoSetting(currentTempo);
                
                if (currentTempo != lastTempoValue) {
                    lastTempoValue = currentTempo;
                    //dispatch_async(dispatch_get_main_queue(), ^{
                    
                    //...
                    //warning evitare chiamata sul thread della UI
                    
                    //});
                }
            }
        }
        
        AUEventSampleTime now = AUEventSampleTime(timestamp->mSampleTime);
        AURenderEvent const *event = realtimeEventListHead;
        
        if (realtimeEventListHead)
        {
            
            do {
                
                switch (event->head.eventType) {
                    case AURenderEventParameter:
                    case AURenderEventParameterRamp:
                    {
                        AUParameterEvent const& paramEvent = event->parameter;
                        //                        AUParameter * param = [__parameterTree__ parameterWithAddress:paramEvent.parameterAddress];
                        //                        param.value = paramEvent.value;
                        
                        switch (paramEvent.parameterAddress) {
                                
                            case freq_INDEX:
                                [self_ setFrequencyValue:paramEvent.value];
                                break;
                                
                            case volume_INDEX:
                                [self_ setVolumeValue:paramEvent.value];
                                break;
                                
                            case drywet_INDEX:
                                [self_ setDrywetValue:paramEvent.value];
                                break;
                                
                            case micInput_INDEX:
                                [self_ setMicInputValue:paramEvent.value > 0.5 ? true : false];
                                break;
                                
                                // APE_ADD_PARAMETER 13
                                
                        }
                        
                        break;
                    }
                        
                    case AURenderEventMIDI:
                    {
                        
                        /* Get MIDI Event from Keyboard */
                        break;
                    }
                        
                    default:
                        break;
                }
                
                // Go to next event.
                event = event->parameter.next;
                
                // While event is not null and is simultaneous.
            } while (event && event->head.eventSampleTime == now);
            
        }
        
        
        AudioBufferList *inAudioBufferList = input->mutableAudioBufferList;
        
        /*
         If the caller passed non-nil output pointers, use those. Otherwise,
         process in-place in the input buffer. If your algorithm cannot process
         in-place, then you will need to preallocate an output buffer and use
         it here.
         */
        AudioBufferList *outAudioBufferList = outputData;
        if (outAudioBufferList->mBuffers[0].mData == nullptr)
        {
            for (UInt32 i = 0; i < outAudioBufferList->mNumberBuffers; ++i) {
                outAudioBufferList->mBuffers[i].mData = inAudioBufferList->mBuffers[i].mData;
            }
        }
        
        //Buffer pointers
        /*
         Float32* sampleL_IN = (Float32*) inAudioBufferList->mBuffers[0].mData;
         Float32* sampleR_IN = (Float32*) inAudioBufferList->mBuffers[1].mData;
         
         Float32* sampleL_OUT = (Float32*) outputData->mBuffers[0].mData;
         Float32* sampleR_OUT = (Float32*) outputData->mBuffers[1].mData;
         */
        
        // convert input channels into a Gen-ready format
        for(int i = 0; i < MIN(inAudioBufferList->mNumberBuffers, data_->inputBuffers.size()); ++i) {
            FloatToSample(data_->inputBuffers[i], (Float32*)inAudioBufferList->mBuffers[i].mData, frameCount);
        }
        
        
        // Uncomment macro for dry/wet test: low frequency is dry signal and hi frequency is wet
        //#define DRYWET_TEST
        
        if (!data_->micInputValue) {
            // Perform gen~
            oscillatore::perform((CommonState *)data_->oscil[0], &(data_->inputBuffers[0]), data_->inputBuffers.size(), &(data_->inputBuffers[0]), data_->inputBuffers.size(), frameCount);
            
#ifdef DRYWET_TEST
            // Perform gen~
            oscillatore::perform((CommonState *)data_->oscil[1], &(data_->inputBuffers[0]), data_->inputBuffers.size(), &(data_->outputBuffers[0]), data_->outputBuffers.size(), frameCount);
#endif
        }
        
#ifndef DRYWET_TEST
        // Perform gen~
        volumeAndGain::perform((CommonState *)data_->volAndGain, &(data_->inputBuffers[0]), data_->inputBuffers.size(), &(data_->outputBuffers[0]), data_->outputBuffers.size(), frameCount);
#endif
        
        t_sample* dry = data_->inputBuffers[0];
        t_sample* wet = data_->outputBuffers[0];
        
        
        // Optimized copy: TODO
        //cblas_scopy(inNumberFrames, dry, 1, _self->inputBuffers[0], 1);
        //cblas_scopy(inNumberFrames, dry, 1, _self->outputBuffers[0], 1);
        
        for (int i = 0; i < frameCount; ++i) {
            
            data_->tmpBuffers[0][i] = wet[i];
            data_->tmpBuffers[1][i] = dry[i];
        }
        
        // Perform gen~
        drywet::perform((CommonState *)data_->drywet, &(data_->tmpBuffers[0]), data_->tmpBuffers.size(), &(data_->outputBuffers[0]), data_->outputBuffers.size(), frameCount);
        
        
        // convert Gen-ready back into audio channels
        for(int i = 0; i < outputData->mNumberBuffers; ++i) {
            // wrap channels for case where more inputs are handed in than the gen filter outputs
            int gen_idx = i % data_->outputBuffers.size();
            FloatFromSample((Float32*)outputData->mBuffers[i].mData, data_->outputBuffers[gen_idx], frameCount);
        }
        
        
        //size_t size = sizeof(Float32) * frameCount;
        //memcpy(sampleL_OUT, sampleL_IN, size);
        //memcpy(sampleR_OUT, sampleR_IN, size);
        
        return noErr;
    };
}

#pragma mark -------------------------------------

void FloatToSample(t_sample *dst, const float *src, long n) {
    while (n--) *dst++ = *src++;
}

void FloatFromSample(float *dst, const t_sample *src, long n) {
    while (n--) *dst++ = *src++;
}

void zeroBuffer(t_sample *dst, int n) {
    while (n--) *dst++ = 0;
}

#pragma - Mark Factory Presets
-(void)makeFactoryPresets
{
    if (!presetParameters) {
        presetParameters = [[NSMutableArray alloc] init];
    }
    else {
        
        [presetParameters removeAllObjects];
    }
    
    NSMutableArray* presetsTMP = [[NSMutableArray alloc] init];
    NSMutableArray *keysSorted = [[NSMutableArray alloc] init];
    
    int i = 0;
    
    /* Read Built-In Preset Banks */
    for (NSString* name in [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[[NSBundle mainBundle] resourcePath]  error:nil]) {
        BOOL subfix = [[name pathExtension] isEqualToString:@"your_suffix"];
        
        if(subfix && name) {
            
            NSString* bankName = [name stringByDeletingPathExtension];
            
            /* Retrive Keys for Presets */
            NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:bankName ofType:@"your_suffix"]];
            NSArray *keys = [dictionary allKeys];
            keysSorted = [[keys sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)] mutableCopy];
            
            /* Iterate through rough every preset and filter apeFilter AUv3 Informations, according to AUv3 Implemantation  */
            for (NSString *key in keysSorted) {
                
                NSDictionary *preset = [dictionary objectForKey:key];
                
                
                /* Copy all keys for the factory presets parameters */
                if (preset) {
                    NSString* name = [preset objectForKey:@"your_key"];
                    [presetsTMP addObject:NewAUPreset(i++, [NSString stringWithFormat:@"%@ (%@)", name, bankName])];
                    [presetParameters addObject:preset];
                }
            }
        }
    }
    
    // Create factory preset array.
    _currentFactoryPresetIndex = kDefaultFactoryPreset;
    _presets = presetsTMP;
    
    presetsTMP = nil;
    keysSorted = nil;
}

- (AUAudioUnitPreset *)currentPreset
{
#warning Logic auval fixed add to all apps
    if (_currentPreset.number >= 0 && _currentFactoryPresetIndex < _presets.count) {
        NSLog(@"Returning Current Factory Preset: %ld\n", (long)_currentFactoryPresetIndex);
        return [_presets objectAtIndex:_currentFactoryPresetIndex];
    }
    else {
        NSLog(@"Returning Current Custom Preset: %ld, %@\n", (long)_currentPreset.number, _currentPreset.name);
        return _currentPreset;
    }
}

- (void)setCurrentPreset:(AUAudioUnitPreset *)currentPreset
{
    if (nil == currentPreset) { NSLog(@"nil passed to setCurrentPreset!"); return; }
    
    if (currentPreset.number >= 0) {
        // factory preset
        for (AUAudioUnitPreset *factoryPreset in _presets) {
            if (currentPreset.number == factoryPreset.number) {
                
                [controller restoreState:presetParameters[factoryPreset.number]];
                
                // set factory preset as current
                _currentPreset = currentPreset;
                _currentFactoryPresetIndex = factoryPreset.number;
                NSLog(@"currentPreset Factory: %ld, %@\n", (long)_currentFactoryPresetIndex, factoryPreset.name);
                
                break;
            }
        }
    } else if (nil != currentPreset.name) {
        // set custom preset as current
        _currentPreset = currentPreset;
        NSLog(@"currentPreset Custom: %ld, %@\n", (long)_currentPreset.number, _currentPreset.name);
    } else {
        NSLog(@"setCurrentPreset not set! - invalid AUAudioUnitPreset\n");
    }
}

#pragma mark - PARAM -> DSP

-(void)setFrequencyValue:(double)value {
    
    data.freqValue = value;
    
    oscillatore::setparameter((CommonState *)data.oscil[0], 1, value, NULL);
    
    oscillatore::setparameter((CommonState *)data.oscil[1], 1, value + 352.997856, NULL);
    
    for (int i = 0; i < 2; ++i) {
        oscillatore::setparameter((CommonState *)data.oscil[i], 0, 0.5, NULL);
    }
}

-(void)setVolumeValue:(double)value {
    
    data.volumeValue = value;
    
    volumeAndGain::setparameter((CommonState *)data.volAndGain, 0, value, NULL);
}

-(void)setDrywetValue:(double)value {
    
    data.drywetValue = value;
    
    drywet::setparameter((CommonState *)data.drywet, 0, value, NULL);
}

-(void)setMicInputValue:(bool)value {
    
    data.micInputValue = (bool) value;
}

// APE_ADD_PARAMETER 14
@end
