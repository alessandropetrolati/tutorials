/*
cs4dev tutorial 05
by Alessandro Petrolati
www.apesoft.it
*/
<CsoundSynthesizer>
<CsOptions>

-o dac
-+rtmidi=null
-+rtaudio=null
-d
-+msg_color=0
--expression-opt
-M0
-m0
-i adc

</CsOptions>
<CsInstruments>


sr = 44100
ksmps = 64
nchnls = 2
0dbfs = 1

maxalloc 53,1  ;iPad UI Waveforms morphing only 1 instance

giWaveSize		init	2048

/* Prototypes Waveform */
giSine		ftgen	0, 0, giWaveSize, 10, 1										; sine wave
giTri			ftgen	0, 0, giWaveSize, 7, 0, giWaveSize/4, 1, giWaveSize/2, -1, giWaveSize/4, 0	; triangle wave 
giSquare		ftgen	0, 0, giWaveSize, 7, 1, giWaveSize/2, 1, 0, -1, giWaveSize/2, -1			; square wave 
giSawDown		ftgen	0, 0, giWaveSize, 7, 1, giWaveSize, -1								; sawtooth wave, downward slope 
giSawSmooth		ftgen	0, 0, giWaveSize, 30, giSawDown, 1, 40, sr 
giSquareSmooth	ftgen	0, 0, giWaveSize, 30, giSquare, 1, 40, sr 


/* Morphing pad */
giWaveTMP1		ftgen	0, 0, giWaveSize, 10, 1						; result wave
giWaveTMP2		ftgen	0, 0, giWaveSize, 10, 1						; result wave
giWaveMORPH		ftgen	0, 0, giWaveSize, 10, 1						; result wave


/* Harmonic generation */
giWaveHARM		ftgen	0, 0, giWaveSize, 10, 1						; result wave


;--------------------------------------------------------------------------------------------------------------------------------

instr 1 ; Play Morph Wave

;--------------------------------------------------------------------------------------------------------------------------------

	chnset giWaveMORPH, "wave_func_table"

kamp1 chnget "amp_one"	
kamp1 portk kamp1, 0.1
amp1 interp kamp1

kfreq1 chnget "freq_one"	
asig oscili amp1, kfreq1, giWaveMORPH

anil = 0
outs asig, anil


endin

;--------------------------------------------------------------------------------------------------------------------------------

instr 2 ; Play Harmonic Wave

;--------------------------------------------------------------------------------------------------------------------------------

	chnset giWaveHARM , "harm_func_table"
	
kamp2 chnget "amp_two"	
kamp2 portk kamp2, 0.1
amp2 interp kamp2

kfreq2 chnget "freq_two"	
asig oscili amp2, kfreq2, giWaveHARM

anil = 0
outs anil, asig


endin


;--------------------------------------------------------------------------------------------------------------------------------

instr 53 ; Waveforms morphing

;--------------------------------------------------------------------------------------------------------------------------------

	tableimix giWaveTMP1, 0, giWaveSize, giSine, 0, 1.-p4, giTri, 0, p4
	tableimix giWaveTMP2, 0, giWaveSize, giSawSmooth, 0, 1.-p4, giSquareSmooth, 0, p4
	tableimix giWaveMORPH, 0, giWaveSize, giWaveTMP2, 0, 1.-p5, giWaveTMP1, 0, p5

	chnset giWaveMORPH , "wave_func_table"

	;turnoff ;not need if UPDATE_RES > 0
endin

</CsInstruments>
<CsScore>

i1 0 999999999
i2 0 999999999

</CsScore>
</CsoundSynthesizer>
<bsbPanel>
 <label>Widgets</label>
 <objectName/>
 <x>100</x>
 <y>100</y>
 <width>320</width>
 <height>240</height>
 <visible>true</visible>
 <uuid/>
 <bgcolor mode="nobackground">
  <r>255</r>
  <g>255</g>
  <b>255</b>
 </bgcolor>
</bsbPanel>
<bsbPresets>
</bsbPresets>
