//
//  AudioDSP.h
//  05_writeGen
//
//  Created by Alessandro Petrolati on 29/05/15.
//  Copyright (c) 2015 apeSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioServices.h>
#import <AudioUnit/AudioUnit.h>
#import <AVFoundation/AVFoundation.h>

#import "csound.h"

#if! TARGET_OS_MACCATALYST
#define IAA
//#define AB
#endif

//#define ENABLE_BLUETOOTH
#define ENABLE_MIDI

#ifdef AB
#import "Audiobus.h"
#import "ABAudiobusController.h"
#endif

#define UPDATE_RES 0.1

@class UIControlXY;
@class Waveview;
@class APE_MULTISLIDER;

@interface AudioDSP : NSObject {
    
    /* Audio IO */
    AudioUnit csAUHAL;
    OSStatus err;
    ExtAudioFileRef recFile;
    
    /* Csound data */
    long bufframes;
    int ret;
    int nchnls;
    int counter;
    bool running;
    bool shouldRecord;
    
    MYFLT srcHarmonic[2048]; //must be conform to giWaveSize in the .csd

    /* User Interface */
    IBOutlet UISwitch* rec;
    
    /* WaveForm */
    IBOutlet UIControlXY *pad;
    IBOutlet Waveview *waveView_morph;
 
    IBOutlet APE_MULTISLIDER* msliders;
    IBOutlet Waveview *waveView_harm;
    
    IBOutlet UISlider* osc1Amp;
    IBOutlet UISlider* osc2Amp;
    IBOutlet UISlider* osc1Freq;
    IBOutlet UISlider* osc2Freq;
}
@property(nonatomic, readwrite) CSOUND* cs;

#ifdef AB
//AUDIOBUS
@property (strong, nonatomic) ABAudiobusController* AB_Controller;
@property (strong, nonatomic) ABSenderPort* output;
@property (strong, nonatomic) ABFilterPort* filter;
#endif

#ifdef IAA
@property (nonatomic, readonly) BOOL connected;
#endif
@end
