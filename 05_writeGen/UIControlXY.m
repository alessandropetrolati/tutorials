/* 
 
 UIControlXY.m:
 
 Copyright (C) 2011 Thomas Hass
 Extended for cs4dev tutorial by Alessandro Petrolati (apeSoft)
 
 This file is part of Csound iOS Examples.
 
 The Csound for iOS Library is free software; you can redistribute it
 and/or modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.   
 
 Csound is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with Csound; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
 02111-1307 USA
 
 */

#import "UIControlXY.h"
#import <QuartzCore/QuartzCore.h>

@interface UIControlXY ()
{
    CGFloat borderWidth;
    BOOL shouldTrack;
}
@end

@implementation UIControlXY

@synthesize cacheDirty = mCacheDirty;
@synthesize xValue;
@synthesize yValue;
@synthesize circleDiameter;
@synthesize channel;

- (void)resetDefaultValue {
    
    self.xValue = 0.0;
    self.yValue = 1.0;
    shouldTrack = NO;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self commonInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

-(void)commonInit {
    
    borderWidth = 1.0f;
    circleRect = CGRectMake(borderWidth,
                            self.frame.size.height - 30.0f - borderWidth,
                            30.0f,
                            30.0f);
    
    [self setCircleDiameter:15];
    [self resetDefaultValue];
        
    self.layer.cornerRadius = 10;
    self.layer.masksToBounds = YES;
}

#pragma mark - Accessor Methods

-(void) setValue:(float)value {
    if (self.tag == 1)
        [self setYValue:value];
    else
        [self setXValue:value];
}

-(float) value {
    
    if (self.tag == 1)
        return yValue;
    else
        return xValue;
}

-(void) maximumValue:(float)value {
    
}

-(float) maximumValue {
    
    return 1.f;
}

-(void) minimumValue:(float)value {

}

-(float) minimumValue {
    return 0.f;
}

- (void)setXValue:(Float32)xValue_
{
    xValue = xValue_;
        
    // Limit it
    CGFloat minX = borderWidth;
    CGFloat maxX = self.frame.size.width - borderWidth - circleRect.size.width;
    
    // Redraw
    CGFloat xPosition = xValue * (maxX - minX);
    circleRect.origin.x = xPosition + borderWidth;

    dispatch_async(dispatch_get_main_queue(), ^{
        [self setNeedsDisplay];
//    [self sendActionsForControlEvents:UIControlEventValueChanged];
    });
}

- (void)setYValue:(Float32)yValue_
{
    yValue = yValue_;
    
    CGFloat minY = borderWidth;
    CGFloat maxY = self.frame.size.height - borderWidth - circleRect.size.height;
    
    CGFloat yPosition = yValue * (maxY - minY);
    yPosition = maxY - yPosition;
    circleRect.origin.y = yPosition;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self setNeedsDisplay];
        //    [self sendActionsForControlEvents:UIControlEventValueChanged];
    });
}

- (void)setCircleDiameter:(CGFloat)circleDiameter_
{
    circleRect = CGRectMake(borderWidth, 
                            self.frame.size.height - circleDiameter_ - borderWidth, 
                            circleDiameter_, 
                            circleDiameter_);
    [self setNeedsDisplay];
}

#pragma mark - UIControl Overrides

- (BOOL)beginTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event
{
    CGPoint location = [touch locationInView:self];
    //if (CGRectContainsPoint(circleRect, location)) {
        
        // Reposition the touch (origin is top left)
        location.x -= circleRect.size.width/2.0f;
        location.y -= circleRect.size.height/2.0f;
        
        // Limit it
        CGFloat minX = 0.;//borderWidth;
        CGFloat minY = 0.;//borderWidth;
        CGFloat maxX = self.frame.size.width - borderWidth - circleRect.size.width;
        CGFloat maxY = self.frame.size.height - borderWidth - circleRect.size.height;
        location.x = location.x < minX ? minX : location.x;
        location.y = location.y < minY ? minY : location.y;
        location.x = location.x > maxX ? maxX : location.x;
        location.y = location.y > maxY ? maxY : location.y;
        
        // Redraw
        circleRect.origin.x = location.x;
        circleRect.origin.y = location.y;
        
        // Update values
        xValue = location.x / maxX;
        yValue = 1.0f - location.y / maxY;
        
        shouldTrack = YES;
    //}
    
    [self setNeedsDisplay];
    [self sendActionsForControlEvents:UIControlEventValueChanged];
    return YES;
}

- (BOOL)continueTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event
{
    CGPoint location = [touch locationInView:self];
    if (shouldTrack) {
        
        // Reposition the touch (origin is top left)
        location.x -= circleRect.size.width/2.0f;
        location.y -= circleRect.size.height/2.0f;
        
        // Limit it
        CGFloat minX = 0.; //borderWidth;
        CGFloat minY = 0.; //borderWidth;
        CGFloat maxX = self.frame.size.width - borderWidth - circleRect.size.width;
        CGFloat maxY = self.frame.size.height - borderWidth - circleRect.size.height;
        location.x = location.x < minX ? minX : location.x;
        location.y = location.y < minY ? minY : location.y;
        location.x = location.x > maxX ? maxX : location.x;
        location.y = location.y > maxY ? maxY : location.y;
        
        // Redraw
        circleRect.origin.x = location.x;
        circleRect.origin.y = location.y;
        
        // Update values
        xValue = location.x / maxX;
        yValue = 1.0f - location.y / maxY;
    }
    
    [self setNeedsDisplay];
    [self sendActionsForControlEvents:UIControlEventValueChanged];
    return YES;
}

- (void)cancelTrackingWithEvent:(UIEvent *)event
{
    
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    shouldTrack = NO;
    [self sendActionsForControlEvents:UIControlEventTouchUpInside];
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self touchesEnded:touches withEvent:event];
}

- (void)drawRect:(CGRect)rect
{
    /* Draw grid */
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextSetAllowsAntialiasing(ctx, YES);
    
    [[UIColor grayColor] set];
    
    //    UIColor* _currentColor = [UIColor grayColor];
    //    CGContextSetStrokeColorWithColor(ctx, _currentColor.CGColor);
    
    float x1 = self.frame.size.width / 4.f;
    float x2 = self.frame.size.width / 2.f;
    float x3 = self.frame.size.width - x1;;
    
    float y1 = self.frame.size.height / 4.f;
    float y2 = self.frame.size.height / 2.f;
    float y3 = self.frame.size.height - y1;;
    
    
    // Draw them with a 3.0 stroke width so they are a bit more visible.
    CGContextSetLineWidth(ctx, 1);
    
    CGContextMoveToPoint(ctx, x2, 0);
    CGContextAddLineToPoint(ctx, x2, self.frame.size.height);
    
    CGContextMoveToPoint(ctx, 0, y2);
    CGContextAddLineToPoint(ctx, self.frame.size.width, y2);
    
    CGContextStrokePath(ctx);
    
    // Draw them with a 1.0 stroke width so they are a bit more visible.
    CGContextSetLineWidth(ctx, 1);
    CGFloat dash1[] = {1.0, 1.0};
    CGContextSetLineDash(ctx, 0.0, dash1, 2);
    
    CGContextMoveToPoint(ctx, x1, 0);
    CGContextAddLineToPoint(ctx, x1, self.frame.size.height);
    
    CGContextMoveToPoint(ctx, x3, 0);
    CGContextAddLineToPoint(ctx, x3, self.frame.size.height);
    
    CGContextMoveToPoint(ctx, 0, y1);
    CGContextAddLineToPoint(ctx, self.frame.size.width, y1);
    
    CGContextMoveToPoint(ctx, 0, y3);
    CGContextAddLineToPoint(ctx, self.frame.size.width, y3);
    
    CGContextStrokePath(ctx);
    
    /* draw circle */
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    // Draw border lines.
    //    [[UIColor grayColor] set];
    CGContextSetLineWidth(ctx, borderWidth * 2.0f);
    CGContextSetLineJoin(ctx, kCGLineJoinRound);
    CGContextSetLineWidth(ctx, borderWidth * 2.0f);
    
    // Line 1
    //    CGMutablePathRef borderPath = CGPathCreateMutable();
    //    CGPathMoveToPoint(borderPath, NULL, 0.0f, 0.0f);
    //    CGPathAddLineToPoint(borderPath, NULL, 0.0f, rect.size.height);
    //    CGPathAddLineToPoint(borderPath, NULL, rect.size.width, rect.size.height);
    //    CGPathAddLineToPoint(borderPath, NULL, rect.size.width, 0.0f);
    //    CGPathAddLineToPoint(borderPath, NULL, 0.0f, 0.0f);
    //    CGContextAddPath(context, borderPath);
    //    CGPathRelease(borderPath);
    //    CGContextDrawPath(context, kCGPathStroke);
    
    //Circle Color
    [[UIColor orangeColor] set];
    CGContextAddEllipseInRect(ctx, circleRect);
    CGContextFillEllipseInRect(ctx, circleRect);
    
    CGColorSpaceRelease(colorSpace);
}

#pragma mark - Value Cacheable

//- (void)setup:(CsoundObj *)csoundObj
//{
//	channelPtrX = [csoundObj getInputChannelPtr:@"mix" channelType:CSOUND_CONTROL_CHANNEL];
//	channelPtrY = [csoundObj getInputChannelPtr:@"pitch" channelType:CSOUND_CONTROL_CHANNEL];
//    cachedValueX = xValue;
//	cachedValueY = yValue;
//    self.cacheDirty = YES;
//    [self addTarget:self action:@selector(updateValueCache:) forControlEvents:UIControlEventValueChanged];
//}
//
//- (void)updateValueCache:(id)sender
//{
//	cachedValueX = ((UIControlXY*)sender).xValue;
//	cachedValueY = ((UIControlXY*)sender).yValue;
//    self.cacheDirty = YES;
//}
//
//- (void)updateValuesToCsound
//{
//	if (self.cacheDirty) {
//        *channelPtrX = cachedValueX;
//		*channelPtrY = cachedValueY;
//        self.cacheDirty = NO;
//    }
//}
//
//- (void)updateValuesFromCsound
//{
//	
//}
//
//- (void)cleanup
//{
//	[self removeTarget:self action:@selector(updateValueCache:) forControlEvents:UIControlEventValueChanged];
//}

@end
