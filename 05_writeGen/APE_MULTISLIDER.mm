//
//  APE_MULTISLIDER.h
//  iOs Common SRC
//
//  Created by Alessandro Petrolati on 15/05/14.
//  Copyright (c) 2014 Alessandro Petrolati. All rights reserved.
//

#import "APE_MULTISLIDER.h"
#import <QuartzCore/QuartzCore.h>
#import <Accelerate/Accelerate.h>
#import <cmath>

@implementation APE_MULTISLIDER
@synthesize channel;
@synthesize delegate = _delegate;
@synthesize slidersNumber = _slidersNumber;
@synthesize styleLine = _styleLine;
@synthesize hitTouch = _hitTouch;
@synthesize bipolar;
@synthesize slidersAlpha;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self commonInit];
    }
    return self;
}


-(id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Initialization code
        [self commonInit];
    }
    return self;
}

-(void)awakeFromNib {
    
    [super awakeFromNib];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(saveAsDefault:)
                                                 name:@"SaveAsDefault"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadAsDefault:)
                                                 name:@"LoadAsDefault"
                                               object:nil];
    if(isRandomizable)
        [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(genRandomic:)
                                                 name:@"GenRandomValue"
                                               object:nil];
    
    if(isDefaultResettable)
        [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(resetsDefault:)
                                                 name:@"ResetToDefault"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRotate)
                                                 name:UIDeviceOrientationDidChangeNotification
                                               object:nil];
}

-(void)dealloc {
    
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"SaveAsDefault"
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"LoadAsDefault"
                                                  object:nil];
    if(isRandomizable)
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"GenRandomValue"
                                                  object:nil];
    if(isDefaultResettable)
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"ResetToDefault"
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIDeviceOrientationDidChangeNotification
                                                  object:nil];
}

-(void)commonInit {
    
    slidersAlpha = 1.0;
    _slidersNumber = 16;
    _styleLine = NO; //default is Bar Style
    rescale = 1.0;
    _hitTouch = 50;
    self.multipleTouchEnabled = YES;
    
    /* Setup Graph Skin Colors */
    //is always clear, see drawRect
//    self.backgroundColor = [UIColor clearColor];
    
    memset(value, 0, sizeof(float) * MAX_SLIDERS_NUMBER);
    
    [self makeRectsForSliders];
    

 // border radius
    [self.layer setCornerRadius:10];
    
    // border
    [self.layer setBorderColor:[UIColor grayColor].CGColor];
    [self.layer setBorderWidth:1];

}

-(void)setSlidersAlpha:(CGFloat)slidersAlpha_ {

    slidersAlpha = slidersAlpha_;
    [self setNeedsDisplay];
}

-(void)didRotate {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self makeRectsForSliders];
        [self updateAllSliders];
    });
}

-(void)makeRectsForSliders {
    
    CGFloat w, h, x;
    h = self.frame.size.height;
    w = self.frame.size.width / _slidersNumber;
    
    for (int i = 0; i < _slidersNumber; ++i) {
        x = i * w;
        bandRect[i] = CGRectMake(x, 0, w, 0);
    }
    
    [self performSelectorOnMainThread:@selector(setNeedsDisplay) withObject:nil waitUntilDone:NO];
}

-(void)setValue:(float)val forIndex:(int)ndx {
    
    if (ndx < MAX_SLIDERS_NUMBER)
        value[ndx] = val * rescale;
}

-(float)getValueAtIndex:(int)ndx {
    if (ndx < MAX_SLIDERS_NUMBER)
        return value[ndx];
    else
        return value[MAX_SLIDERS_NUMBER-1];
}

-(void)setSlidersNumber:(int)slidersNumber {
    
    int oldNum = _slidersNumber;
    
    _slidersNumber = slidersNumber > MAX_SLIDERS_NUMBER ? MAX_SLIDERS_NUMBER : slidersNumber;
    
    [self makeRectsForSliders];
    
    [self scaleContents:oldNum];
}


-(int)getSlidersNumber {

    return _slidersNumber;
}

-(void)randomize {
    
    for (int i = 0; i < MAX_SLIDERS_NUMBER; ++i) {
        
        if (bipolar)
            [self setValue:((((double)arc4random() / 0x100000000)) * 2.0) - 1.0  forIndex:i];
        else
            [self setValue:(((double)arc4random() / 0x100000000)) forIndex:i];
        
    }
  
    [self updateAllSliders];
}

-(void)flat {
    
    memset(value, 0, sizeof(float) * MAX_SLIDERS_NUMBER);
    [self updateAllSliders];
}

-(void)scale:(float)value_ {

// Import Accelerate Framework

    rescale = value_;
    float max_value;
    
    // store the maximum value from array A in variable max_value
    vDSP_maxv(&value_, 1, &max_value, MAX_SLIDERS_NUMBER);
    
    max_value /= rescale;
    // divide each value in array A by max_value, store the results
    vDSP_vsdiv(&value_, 1, &max_value, &value_, 1, MAX_SLIDERS_NUMBER);
    
    [self updateAllSliders];
}

-(int)getIndex:(CGFloat)touchLocationX; {
    CGFloat w = self.frame.size.width / _slidersNumber;
    CGFloat _x = (touchLocationX/(self.frame.size.width-w));
    _x = _x > 1.0 ? 1.0 : _x;
    _x = _x < 0.0 ? 0.0 : _x;
    return (_x * (_slidersNumber-1));
}

-(float)getValue:(CGFloat)touchLocationY; {
    
    CGFloat gain_ = 1.0 - (touchLocationY/self.frame.size.height);    
    gain_ = gain_ > 1.0 ? 1.0 : gain_;
    gain_ = gain_ < 0.0 ? 0.0 : gain_;
    
    if (bipolar)
        gain_ = (gain_ * 2.0) - 1.0;

    return gain_ /* * rescale*/;
}

-(void)updateSlidersAtIndex:(int)ndx {

    if (bipolar) {
        CGFloat hhalf = self.frame.size.height * 0.5;
        bandRect[ndx].origin.y = hhalf;
        if(!_styleLine)
            bandRect[ndx].size.height = ((1.0 - value[ndx]) * hhalf) - hhalf;
        else {
            bandRect[ndx].origin.y = ((1.0 - value[ndx]) * hhalf);
            bandRect[ndx].size.height = STYLE_LINE_H;
        }
    }
    else {
        bandRect[ndx].origin.y = self.frame.size.height;
        if(!_styleLine)
            bandRect[ndx].size.height = ((1.0 - value[ndx]) * self.frame.size.height) - self.frame.size.height;
        else {
            bandRect[ndx].origin.y = ((1.0 - value[ndx]) * self.frame.size.height) - (STYLE_LINE_H/2);
            bandRect[ndx].size.height = STYLE_LINE_H;
        }
    }

    [_delegate didValueChanged:value currentSlider:ndx maxSlidersNumber:_slidersNumber Tag:self.tag];
    [self performSelectorOnMainThread:@selector(setNeedsDisplay) withObject:nil waitUntilDone:NO];
}

-(void)updateAllSliders {

    for (int i = 0; i < _slidersNumber; ++i) {
    
        [self updateSlidersAtIndex:i];
    }
}

-(void)scaleContents:(int)length {

    //Decrease
    if (length > _slidersNumber) {

//        int ratio = (float)length/(float)_slidersNumber;
//    
//        for (int i = 0; i < _slidersNumber; ++i) {
//            value[i] = value[i*ratio];
//        }
        [self interpArray:value SizeInput:length Output:value SizeOutput:_slidersNumber];
    }
    //Increase
    else if (length < _slidersNumber) {

        float tmp[_slidersNumber];
        memcpy(tmp, value, sizeof(float) * _slidersNumber);


#define INTERP

#ifdef INTERP
        [self interpArray:tmp SizeInput:length Output:value SizeOutput:_slidersNumber];
#else
        int w = 0;
        int ratio = (float)(_slidersNumber)/(float)length;
        for (int i = 0; i < _slidersNumber; ++i) {
            for (int x = 0; x < ratio; ++x)
            {
                if (w < _slidersNumber)
                    value[w] = tmp[i];
                ++w;
            }            
        }
#endif
    }
    
    [self updateAllSliders];
}

// linear interpolate array a[] -> array b[]
-(void) interpArray:(float*)input SizeInput:(int)inSize Output:(float*)output SizeOutput:(int)outSize
{
    float step = float( inSize - 1 ) / (outSize - 1);
    float val, x;
    for( int j = 0; j < outSize; j ++ ){

        x = j*step;
        if( x <= 0 ) val = input[0];
        if( x >= inSize - 1 )  val = input[inSize-1];
        int k = int(x);
        output[j] = input[k] + (x - k) * (input[k+1] - input[k]);
    }
}

#pragma mark - Touches
#ifdef MULTISLIDER_USE_HIT_TESTING
- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    CGRect frame = CGRectMake(-_hitTouch, -_hitTouch,
                              self.frame.size.width + (_hitTouch*2.0),
                              self.frame.size.height + (_hitTouch*2.0));
    
    if (CGRectContainsPoint(frame, point)) {
        return self;
    }
    return nil;
}
#endif

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchLocation = [touch locationInView:touch.view];
    
    lastNdx = [self getIndex:touchLocation.x];
    lastTouchY = touchLocation.y;
    
    [self touchesMoved:touches withEvent:event];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchLocation = [touch locationInView:touch.view];
    
    CGFloat x = touchLocation.x;
    CGFloat y = touchLocation.y;
    
    int index = [self getIndex:x];
    
    if (index > lastNdx) {
        
        if (index - lastNdx >= 2) {
            
            int jumpSteps = index - lastNdx;
            
            CGFloat startValue = lastTouchY;
            CGFloat endValue = y;
            CGFloat incr = endValue - startValue;
            incr /= (CGFloat)jumpSteps;
            CGFloat y_ = startValue;
            
            for (int i = 0; i < jumpSteps; ++i) {
                y_ += incr;
                
                int ndx_ = lastNdx + 1 + i;
                value[ndx_] = [self getValue:y_];
                [self updateSlidersAtIndex:ndx_];
            }
        }
    }
    else {
        
        if (lastNdx - index >= 2) {
            int jumpSteps = lastNdx - index;
            
            CGFloat startValue = lastTouchY;
            CGFloat endValue = y;
            CGFloat incr = startValue - endValue;
            incr /= (CGFloat)jumpSteps;
            CGFloat y_ = startValue;
            
            for (int i = 0; i < jumpSteps; ++i) {
                y_ -= incr;
                
                int ndx_ = lastNdx - 1 - i;
                value[ndx_] = [self getValue:y_];
                [self updateSlidersAtIndex:ndx_];
            }
        }
    }
    
    value[index] = [self getValue:y];
    
    lastNdx = index;
    lastTouchY = y;
    
//    [self updateAllSliders];
    [self updateSlidersAtIndex:index];
    
//    NSLog(@"Freq = %f", (float(index)/_slidersNumber)*22050.0 );
}


#pragma mark - Rendering

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    
//    [self setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0.2]];
    [self setBackgroundColor:[UIColor clearColor]];
    self.contentScaleFactor = 1.0;
    
    // Drawing code
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextSetAllowsAntialiasing(ctx, YES);
    
    //    if (_drawGrid)
//    [self drawGrid:ctx];

#ifndef MULTISLIDERS_SKIP_DRAW_BORDER
    CGContextSetLineWidth(ctx, 0.5);
    CGContextSetStrokeColorWithColor(ctx, [UIColor grayColor].CGColor);
    CGContextStrokeRect(ctx, rect);
#endif

    CGContextSaveGState(ctx);
    
    CGFloat x = 0;
    for (int i = 0; i < _slidersNumber; ++i)
    {
        x = (CGFloat)i/(CGFloat)_slidersNumber;
        
        CGContextSetLineWidth(ctx, 1);
        //        CGContextSetStrokeColorWithColor(ctx, [UIColor redColor].CGColor);
        // Draw them with a 1.0 stroke width so they are a bit more visible.
        //        CGFloat dash1[] = {1.0, 1.0};
        //        CGContextSetLineDash(ctx, 0.0, dash1, 2);
        
        CGFloat r,g,b;
        
        r = x;
        g = 1.0 - x;
        b = x;
        
        UIColor* color = [UIColor colorWithRed:r green:g blue:b alpha:slidersAlpha];
        CGContextSetFillColorWithColor(ctx, color.CGColor);
        //        CGContextSetFillColorWithColor(ctx, [SCOPE_COLOR colorWithAlphaComponent:1].CGColor);
        //        CGContextSetShadow(ctx, CGSizeMake(2, 3), 5);
        
        CGContextAddRect(ctx, bandRect[i]);
        CGContextFillPath(ctx);
    }
    
    CGContextRestoreGState(ctx);
    
}

-(void)drawGrid:(CGContextRef)ctx {
    
    /* Draw grid */
    CGContextSetAllowsAntialiasing(ctx, YES);
    
    CGContextSaveGState(ctx);
    
    //    // Draw them with a 1.0 stroke width so they are a bit more visible.
    CGContextSetLineWidth(ctx, 1);
    //    CGFloat dash1[] = {2.0, 6.0};
    //    CGContextSetLineDash(ctx, 0.0, dash1, 2);
    
    UIColor* color = [UIColor grayColor];
    CGContextSetStrokeColorWithColor(ctx, color.CGColor);
    
    
    NSMutableParagraphStyle* p = [NSMutableParagraphStyle new];
    p.alignment = NSTextAlignmentLeft;
    NSAttributedString* txt =
    txt = [[NSAttributedString alloc] initWithString:@"Spectral Bins X = freq; Y = value " attributes:@{
                                                                                                                        NSFontAttributeName:[UIFont systemFontOfSize:12],
                                                                                                                        NSForegroundColorAttributeName:color,
                                                                                                                        NSParagraphStyleAttributeName:p
                                                                                                                        }];
    
    [txt drawInRect:CGRectMake(5, 0, self.frame.size.width-5, 20)];
    
    
    
    CGContextMoveToPoint(ctx, 1, 0);
    CGContextAddLineToPoint(ctx, 1, self.frame.size.height);
    
    if (bipolar) {
        CGContextMoveToPoint(ctx, 0, (self.frame.size.height/2)-1);
        CGContextAddLineToPoint(ctx, self.frame.size.width, (self.frame.size.height/2)-1);
    }
    else {

        CGContextMoveToPoint(ctx, 0, self.frame.size.height-1);
        CGContextAddLineToPoint(ctx, self.frame.size.width, self.frame.size.height-1);

    }

    CGContextStrokePath(ctx);
    
    CGContextRestoreGState(ctx);
}


-(void)setNeedsDisplay {
    
    [super setNeedsDisplay];
}


#pragma mark - Observed Methods

-(void)saveAsDefault:(NSNotification *) notification {
    
    if (channel) {
        
        NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
        [preferences setObject:[self getState] forKey:[NSString stringWithFormat:@"%@_STATE_", channel]];
    }
}

-(void)loadAsDefault:(NSNotification *) notification {
    
    if (channel) {
        
        NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
        NSDictionary *retrieved = [preferences dictionaryForKey:[NSString stringWithFormat:@"%@_STATE_", channel]];
        
        if (retrieved) {
            NSMutableDictionary* array = [NSMutableDictionary dictionaryWithDictionary:retrieved];
            
            if(array)
                [self restoreState:array];
            
        }
    }
}

#pragma mark save and restore keyboard state and various

-(NSMutableDictionary*)getState {
    
    NSMutableDictionary* connectionState = [[NSMutableDictionary alloc] init];
    
    NSMutableData * data = [NSMutableData dataWithCapacity:MAX_SLIDERS_NUMBER];
    [data appendBytes:&value length:sizeof(float) * MAX_SLIDERS_NUMBER];
    
    /* STATE */
    [connectionState setObject:data forKey:[NSString stringWithFormat:@"%@_VALUES_", channel]];
    [connectionState setObject:[NSNumber numberWithInt:_slidersNumber] forKey:[NSString stringWithFormat:@"%@_NUM_OF_SLIDERS_", channel]];
    return connectionState;
}

-(void)restoreState:(NSMutableDictionary*)keyState {
    
    NSData *data;
    NSNumber* val;
    
    /* STATE */
    if((data = [keyState objectForKey:[NSString stringWithFormat:@"%@_VALUES_", channel]]))
    {
        [data getBytes:&value length:sizeof(float) * MAX_SLIDERS_NUMBER];
    }
    if((val = [keyState objectForKey:[NSString stringWithFormat:@"%@_NUM_OF_SLIDERS_", channel]]))
    {
        int presetSliderNums = (int) [val integerValue];
        
        if(presetSliderNums != _slidersNumber)
           [self scaleContents:presetSliderNums];
        else
            [self updateAllSliders];    
    }
}

-(void)genRandomic:(NSNotification *) notification {

    [self randomize];
}

-(void)resetsDefault:(NSNotification *) notification {
    
    [self flat];
}

+(void)genShapeFor:(int)currentItem inObj:(APE_MULTISLIDER*)mlsd {
    
    
    int _size = mlsd.slidersNumber;
    float val = 0.0;
    
    switch (currentItem) {
            
        case 0: //Triangoular
        {
            float val = 0.0;
            for(int i = 0; i < _size/2; ++i) {
                
                float incr = 1.0 / (_size/2);
                val += incr;
                [mlsd setValue:val forIndex:i];
            }
            
            for(int i = _size/2; i < _size; ++i) {
                
                float incr = 1.0 / (_size/2);
                val -= incr;
                [mlsd setValue:val forIndex:i];
            }
            break;
        }
        case 1: //Blackmann
        {
            float A0 = .42;
            float A1 = .5;
            float A2 = .08;
            
            int _size = mlsd.slidersNumber;
            float val = 0.0;
            
            /* Blackmann */
            for (int i = 0; i < _size; ++i)
            {
                val = (float) A0 - A1 * cos(2.0 * M_PI* i / _size) + A2
                * cos(4 * M_PI * (double) i / _size);
                [mlsd setValue:val forIndex:i];
            }
            
            break;
        }
        case 2: //Trapezoid
        {
            float val = 0.0;
            float rise = 4.0;
            float peak = 1.0;
            
            for(int i = 0; i < _size; ++i) {
                [mlsd setValue:peak forIndex:i];
            }
            
            for(int i = 0; i < _size/rise; ++i) {
                
                float incr = peak / (_size/rise);
                val += incr;
                [mlsd setValue:val forIndex:i];
            }
            
            
            for(int i = _size- (_size/rise); i < _size; ++i) {
                
                float incr = peak / (_size/rise);
                val -= incr;
                [mlsd setValue:val forIndex:i];
            }
            break;
        }
        case 3: //Sigmoid
        {
            for (int i = 0; i < _size; i++)
            {
                double x = (double) (_size - i) / _size;
                val = (float) cos(M_PI * x);
                if (!mlsd.bipolar) {
                    val *= 0.5;
                    val += 0.5;
                }
                [mlsd setValue:val forIndex:i];
            }
            
            break;
        }
        case 4: //Sine
        {
            for(int i = 0; i < _size; ++i) {
                val = (float) sin(2.0 * M_PI * (double) i / _size);
                if (!mlsd.bipolar) {
                    val *= 0.5;
                    val += 0.5;
                }
                [mlsd setValue:val forIndex:i];
            }
            
            break;
        }
        case 5: //Sync 1
        {
            for(int i = 0; i < _size; ++i) {
                float x =  ((float)i / (float)_size) * (M_PI*2.0);
                val = [APE_MULTISLIDER sinc:x];
                if (!mlsd.bipolar) {
                    val *= 0.5;
                    val += 0.5;
                }
                [mlsd setValue:val forIndex:i];
            }
            break;
        }
        case 6: //Sync 2
        {
            for(int i = 0; i < _size; ++i) {
                float x =  ((float)i / (float)_size) * (M_PI*6.0);
                val = [APE_MULTISLIDER sinc:x];
                if (!mlsd.bipolar) {
                    val *= 0.5;
                    val += 0.5;
                }
                [mlsd setValue:val forIndex:i];
            }
            break;
        }
        case 7: //Sync 3
        {
            for(int i = 0; i < _size; ++i) {
                float x =  ((float)i / (float)_size) * (M_PI*12.0);
                val = [APE_MULTISLIDER sinc:x];
                if (!mlsd.bipolar) {
                    val *= 0.5;
                    val += 0.5;
                }
                [mlsd setValue:val forIndex:i];
            }
            break;
        }
        case 8: //Sinc Mirror
        {
            for(int i = 0; i < _size/2; ++i) {
                float x =  ((float)i / (float)(_size/2)) * (M_PI*4.0);
                val = [APE_MULTISLIDER sinc:x];
                if (!mlsd.bipolar) {
                    val *= 0.5;
                    val += 0.5;
                }
                [mlsd setValue:val forIndex:((_size/2)-1)-i];
                [mlsd setValue:val forIndex:i+(_size/2)];
            }
            
            break;
        }
        case 9: //Random
        {
            [mlsd randomize];
            break;
        }
            
            
            /* TRATMENTS */
        case 20: //FLAT
        {
            [mlsd flat];
            break;
        }
        case 21: //Flip Vertical
        {
            for (int i = 0; i < _size; ++i)
            {
                float val = [mlsd getValueAtIndex:i];
                if (!mlsd.bipolar)
                    [mlsd setValue:1.0-val forIndex:i];
                else
                    [mlsd setValue:val*-1.0 forIndex:i];
            }
            
            break;
        }
        case 22: //Flip Horizontal
        {
            float* tmp;
            tmp = new float[_size];
            for (int i = 0; i < _size; ++i) {
                
                tmp[i] = [mlsd getValueAtIndex:i];
            }
            
            int x = _size-1;
            for (int i = 0; i < _size; ++i)
            {
                [mlsd setValue:tmp[x--] forIndex:i];
            }
            
            delete [] tmp;
            
            break;
        }
        case 23: //Shift <<
        {
            float* tmp;
            tmp = new float[_size];
            for (int i = 0; i < _size; ++i) {
                
                tmp[i] = [mlsd getValueAtIndex:i];
            }
            
            int x = _size/6;
            for (int i = 0; i < _size; ++i)
            {
                if (x > _size-1)
                    x = 0;
                
                [mlsd setValue:tmp[x++] forIndex:i];
            }
            
            delete [] tmp;
            
            break;
        }
        case 24: //reduce Size
        {
            float* input;
            input = new float[_size];
            for (int i = 0; i < _size; ++i) {
                
                input[i] = [mlsd getValueAtIndex:i];
            }
            
            int inSize = _size/2;
            int outSize = _size;
            
            float step = float( inSize - 1 ) / (outSize - 1);
            float val, x;
            for( int j = 0; j < outSize; j ++ ){
                
                x = j*step;
                if( x <= 0 ) val = input[0];
                if( x >= inSize - 1 )  val = input[inSize-1];
                int k = int(x);
                [mlsd setValue:(input[k] + (x - k) * (input[k+1] - input[k])) forIndex:j];
            }
            
            delete [] input;
            
            break;
        }
        case 25: //repeate Size
        {
            
            float* tmp;
            tmp = new float[_size];
            for (int i = 0; i < _size; ++i) {
                
                tmp[i] = [mlsd getValueAtIndex:i];
            }
            
            int ratio = 2;
            
            for (int i = 0; i < _size/ratio; ++i) {
                [mlsd setValue:tmp[i*ratio] forIndex:i];
                [mlsd setValue:[mlsd getValueAtIndex:i] forIndex:i+(_size/ratio)];
            }
            
            delete [] tmp;
            
            break;
        }
    }
    
    [mlsd updateAllSliders];
}

+(double) sinc:(double) x
{
    if (x==0)
        return 1;
    return sin(x)/x;
}
@end
