/*
 
 Waveview.m:
 
 Copyright (C) 2015 Steven Yi, Ed Costello, Aurelius Prochazka
 Extended for cs4dev tutorial by Alessandro Petrolati (apeSoft)
 
 This file is part of Csound iOS Examples.
 
 The Csound for iOS Library is free software; you can redistribute it
 and/or modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 Csound is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with Csound; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
 02111-1307 USA
 
 */

#import "Waveview.h"

@implementation Waveview
{
    BOOL tableLoaded;
    CGFloat lastY;
    MYFLT *table;
    int tableLength;
    MYFLT *displayData;
    int fTableNumber;
}

@synthesize drawGrid = _drawGrid;
@synthesize waveColor = _waveColor;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self commonInit];
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        [self commonInit];
    }
    return self;
}

-(void)commonInit {
    
    displayData = nil;
    _drawGrid = NO;
    _waveColor = [UIColor blackColor];
}

-(void)setWaveColor:(UIColor *)waveColor {
    
    _waveColor = waveColor;
    [self performSelectorOnMainThread:@selector(setNeedsDisplay) withObject:nil waitUntilDone:NO];
}

- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (tableLoaded)
    {
        int width = self.frame.size.width;
        
        CGContextSetStrokeColorWithColor(context, _waveColor.CGColor);
        CGMutablePathRef fill_path = CGPathCreateMutable();
        CGFloat x = 0;
        CGFloat y = displayData[0];
        
        CGPathMoveToPoint(fill_path, &CGAffineTransformIdentity, x, y);
        
        for(int i = 1; i < width; i++) {
            CGPathAddLineToPoint(fill_path, &CGAffineTransformIdentity, i, displayData[i]);
        }
        CGContextAddPath(context, fill_path);
        CGContextSetAllowsAntialiasing(context, YES);
        CGContextDrawPath(context, kCGPathStroke);
        CGPathRelease(fill_path);
    }
    
    if (_drawGrid) {
        
        /* Draw grid */
        CGContextRef ctx = UIGraphicsGetCurrentContext();
        CGContextSetAllowsAntialiasing(ctx, YES);
        
        [[UIColor grayColor] set];
        
        CGFloat toolbarHeight = 0;
        
        float x1 = self.frame.size.width / 4.f;
        float x2 = self.frame.size.width / 2.f;
        float x3 = self.frame.size.width - x1;;
        
        float y1 = self.frame.size.height / 4.f;
        float y2 = self.frame.size.height / 2.f;
        float y3 = self.frame.size.height - y1;;
        
        // Draw them with a 3.0 stroke width so they are a bit more visible.
        CGContextSetLineWidth(ctx, 1);
        
        CGContextMoveToPoint(ctx, x2, 0);
        CGContextAddLineToPoint(ctx, x2, self.frame.size.height-toolbarHeight);
        
        CGContextMoveToPoint(ctx, 0, y2);
        CGContextAddLineToPoint(ctx, self.frame.size.width, y2);
        
        CGContextStrokePath(ctx);
        
        // Draw them with a 1.0 stroke width so they are a bit more visible.
        CGContextSetLineWidth(ctx, 1);
        CGFloat dash1[] = {1.0, 1.0};
        CGContextSetLineDash(ctx, 0.0, dash1, 2);
        
        CGContextMoveToPoint(ctx, x1, 0);
        CGContextAddLineToPoint(ctx, x1, self.frame.size.height-toolbarHeight);
        
        CGContextMoveToPoint(ctx, x3, 0);
        CGContextAddLineToPoint(ctx, x3, self.frame.size.height-toolbarHeight);
        
        CGContextMoveToPoint(ctx, 0, y1);
        CGContextAddLineToPoint(ctx, self.frame.size.width, y1);
        
        CGContextMoveToPoint(ctx, 0, y3);
        CGContextAddLineToPoint(ctx, self.frame.size.width, y3);
        
        CGContextStrokePath(ctx);
    }
}

- (void)displayFTable:(CSOUND*)cs numOfTable:(int)fTableNum
{
    fTableNumber = fTableNum;
    tableLoaded = NO;
    
    if (cs) {
        
        if ((tableLength = csoundTableLength(cs, fTableNumber)) > 0) {
            table = malloc(tableLength * sizeof(MYFLT));
            csoundGetTable(cs, &table, fTableNumber);
            tableLoaded = YES;
            //                [self performSelectorInBackground:@selector(updataDisplayData) withObject:nil];
            [self performSelectorOnMainThread:@selector(updataDisplayData) withObject:nil waitUntilDone:NO];
        }
    }
    else {
        fTableNumber = -1;
        tableLoaded = NO;
    }
}

- (void)updataDisplayData
{
    
    if (!tableLoaded || (fTableNumber == -1))
        return;
    
    float scalingFactor = 0.9;
    int width = self.frame.size.width;
    int height = self.frame.size.height;
    int middle = (height / 2);
    
    if (!displayData)
        displayData = malloc(sizeof(MYFLT) * width);
    
    for(int i = 0; i < width; i++) {
        float percent = i / (float)(width);
        int index = (int)(percent * tableLength);
        displayData[i] = (-(table[index] * middle * scalingFactor) + middle);
    }
    
    [self performSelectorOnMainThread:@selector(setNeedsDisplay)
                           withObject:nil
                        waitUntilDone:NO];
}
@end
