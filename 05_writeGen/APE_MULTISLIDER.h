//
//  APE_MULTISLIDER.h
//  iOs Common SRC
//
//  Created by Alessandro Petrolati on 15/05/14.
//  Copyright (c) 2014 Alessandro Petrolati. All rights reserved.
//

#import <UIKit/UIKit.h>

#define MAX_SLIDERS_NUMBER 2048
#define STYLE_LINE_H 4

@interface APE_MULTISLIDER : UIView {

    CGRect bandRect[MAX_SLIDERS_NUMBER];
    float value[MAX_SLIDERS_NUMBER];
    int lastNdx;
    CGFloat lastTouchY;
    float rescale;
    BOOL isRandomizable;
    BOOL isDefaultResettable;
}
@property(nonatomic, readonly)Boolean bipolar;
@property(nonatomic, readonly)NSString* channel;
@property(nonatomic, strong) IBOutlet id delegate;
@property(nonatomic, readwrite) int slidersNumber;
@property(nonatomic, readwrite) BOOL styleLine;
@property(nonatomic, readwrite) CGFloat hitTouch;
@property(nonatomic, readwrite) CGFloat slidersAlpha;

-(void)setSlidersNumber:(int)slidersNumber;
-(int)getSlidersNumber;

-(void)setValue:(float)val forIndex:(int)ndx;
-(float)getValueAtIndex:(int)ndx;
-(void)randomize;
-(void)flat;
-(NSMutableDictionary*)getState;
-(void)restoreState:(NSMutableDictionary*)keyState;
-(void)updateAllSliders;
-(void)scaleContents:(int)length;
-(void)didRotate;
+(void)genShapeFor:(int)currentItem inObj:(APE_MULTISLIDER*)mlsd;
@end

@protocol APE_MULTISLIDER_DELEGATE <NSObject>

-(void)didValueChanged:(float*)value currentSlider:(int)ndx  maxSlidersNumber:(int)maxnum Tag:(NSInteger)tag;

@end